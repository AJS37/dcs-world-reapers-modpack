livery = {
	-- Livery painted by T.Jung "Neilus" (simfog.com) Version: 1.0 - 2019-11-02
	{"AV8B1", 0 ,"AV8_T1_SF",false};
	{"AV8B2", 0 ,"AV8_T2_SF",false};
	{"AV8B3", 0 ,"AV8_T3_SF",false};
	{"AV8B4", 0 ,"AV8_T4_SF",false};
	{"AV8B7", 0 ,"AV8_T7_SF",false};
	{"AV8B8", 0 ,"AV8_T8_SF",false};
	{"Pilot_AV8B_gear", 0 ,"pilot_AV8B_gear_SF",false};
	{"pilot_AV8B_body", 0 ,"pilot_AV8B",false};
			
	{"AV8B_T1_FUSE1", 0 ,"AV8_T1_SF",false};
	{"AV8B_T1_FUSE1", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T1_FUSE10", 0 ,"AV8_T1_SF",false};
	{"AV8B_T1_FUSE10", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T2_FUSE1", 0 ,"AV8_T2_SF",false};
	{"AV8B_T2_FUSE1", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T2_FUSE10", 0 ,"AV8_T2_SF",false};
	{"AV8B_T2_FUSE10", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T3_FUSE1", 0 ,"AV8_T3_SF",false};
	{"AV8B_T3_FUSE1", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T3_FUSE10", 0 ,"AV8_T3_SF",false};
	{"AV8B_T3_FUSE10", DECAL ,"AV8_TNumbers_SF",false};
	
	{"AV8B_T3_TAIL1", 0 ,"AV8_T3_SF",false};
	{"AV8B_T3_TAIL1", DECAL ,"empty",true};
	
	{"AV8B_T3_TAIL10", 0 ,"AV8_T3_SF",false};
	{"AV8B_T3_TAIL10", DECAL ,"empty",true};
	
	{"AV8B_T3_TAIL100", 0 ,"AV8_T3_SF",false};
	{"AV8B_T3_TAIL100", DECAL ,"empty",true};
		
	{"pilot_AV8B_patch", 0 ,"pilot_AV8B_patch",false};
	{"AV8B1", ROUGHNESS_METALLIC ,"AV8_T1_RoughMet",true};
	{"AV8B2", ROUGHNESS_METALLIC ,"AV8_T2_RoughMet",true};
	{"AV8B3", ROUGHNESS_METALLIC ,"AV8_T3_RoughMet",true};
	{"AV8B4", ROUGHNESS_METALLIC ,"AV8_T4_RoughMet",true};
	{"AV8B7", ROUGHNESS_METALLIC ,"AV8_T7_RoughMet",true};
	{"AV8B8", ROUGHNESS_METALLIC ,"AV8_T8_RoughMet",true};

}
name = "TM-Reapers"
