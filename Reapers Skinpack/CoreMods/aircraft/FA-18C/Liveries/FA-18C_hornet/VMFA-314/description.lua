livery = {

	{"f18c1", 0 ,"F18C_1_DIFF",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};

	
	{"f18c2", 0 ,"F18C_2_DIFF",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};

	{"pilot_F18_helmet", 0 ,"pilot_F18_helmet_green",false};
	{"pilot_F18_helmet", 1 ,"pilot_F18_helmet_nm",false};
	{"pilot_F18_helmet", ROUGHNESS_METALLIC ,"pilot_F18_helmet_RoughMet",false};
	{"pilot_F18", 0, "F-18_pilot_VMFA-314",false};
	{"pilot_F18_patch", 0 ,"empty",true};
	{"pilot_F18_patch", 1 ,"empty",true};
	
	
	
	{"F18C_BORT_NUMBER_NOSE_L_100", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_L_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_L_100", DECAL ,"VMFA-314_bort_number",false};
	
	{"F18C_BORT_NUMBER_NOSE_L_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_L_10", DECAL ,"VMFA-314_bort_number",false};	

	{"F18C_BORT_NUMBER_NOSE_L_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_L_01", DECAL ,"VMFA-314_bort_number",false};
	

	{"F18C_BORT_NUMBER_NOSE_R_100", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_R_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_R_100", DECAL ,"VMFA-314_bort_number",false};
	
	{"F18C_BORT_NUMBER_NOSE_R_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_R_10", DECAL ,"VMFA-314_bort_number",false};

	{"F18C_BORT_NUMBER_NOSE_R_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_R_01", DECAL ,"VMFA-314_bort_number",false};	
	
	
	

	
	{"F18C_BORT_NUMBER_ZAK_L_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_L_100", DECAL ,"VMFA-314_bort_number",false};
	
	{"F18C_BORT_NUMBER_ZAK_L_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_L_10", DECAL ,"VMFA-314_bort_number",false};

	{"F18C_BORT_NUMBER_ZAK_L_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_L_01", DECAL ,"VMFA-314_bort_number",false};
	
	{"F18C_BORT_NUMBER_ZAK_R_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_R_100", DECAL ,"VMFA-314_bort_number",false};
	
	{"F18C_BORT_NUMBER_ZAK_R_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_R_10", DECAL ,"VMFA-314_bort_number",false};

	{"F18C_BORT_NUMBER_ZAK_R_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_ZAK_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_ZAK_R_01", DECAL ,"VMFA-314_bort_number",false};

	
	
	{"F18C_BORT_NUMBER_KIL_L_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_L_100", DECAL ,"VMFA-314_bort_number_left",false};
	
	{"F18C_BORT_NUMBER_KIL_L_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_L_10", DECAL ,"VMFA-314_bort_number_left",false};

	{"F18C_BORT_NUMBER_KIL_L_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_L_01", DECAL ,"VMFA-314_bort_number_left",false};
	
	{"F18C_BORT_NUMBER_KIL_R_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_R_100", DECAL ,"VMFA-314_bort_number_right",false};
	
	{"F18C_BORT_NUMBER_KIL_R_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_R_10", DECAL ,"VMFA-314_bort_number_right",false};

	{"F18C_BORT_NUMBER_KIL_R_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_R_01", DECAL ,"VMFA-314_bort_number_right",false};

	
	

	
	
	
	
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", DECAL ,"empty",true};
	

	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", DECAL ,"empty",true};		
	


	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", DECAL ,"empty",true};	
	
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", DECAL ,"empty",true};		



	
	

	
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_fin_L_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_fin_L_01", DECAL ,"empty",true};
	


	
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_fin_R_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_NOSE_fin_R_01", DECAL ,"empty",true};			
	
	

	
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Switz_R_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_R_01", DECAL ,"empty",true};	
	
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Switz_L_01", 0 ,"F18C_2_DIFF",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_KIL_Switz_L_01", DECAL ,"empty",true};	

	
	
	
	{"F18C_BORT_NUMBER_STV_aus_R_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_STV_aus_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_STV_aus_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_STV_aus_R_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_STV_aus_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_STV_aus_R_01", DECAL ,"empty",true};	
	
	
	{"F18C_BORT_NUMBER_STV_aus_L_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_STV_aus_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_STV_aus_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_STV_aus_L_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_STV_aus_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_STV_aus_L_01", DECAL ,"empty",true};		
		

	{"F18C_BORT_NUMBER_MTW_aus_R_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_MTW_aus_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_MTW_aus_R_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_MTW_aus_R_01", DECAL ,"empty",true};	


	{"F18C_BORT_NUMBER_MTW_aus_L_10", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_MTW_aus_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_MTW_aus_L_01", 0 ,"F18C_1_DIFF",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",true};
	{"F18C_BORT_NUMBER_MTW_aus_L_01", DECAL ,"empty",true};	
	
	


	
}
name = "VMFA-314"
-- countries = {"USA",}