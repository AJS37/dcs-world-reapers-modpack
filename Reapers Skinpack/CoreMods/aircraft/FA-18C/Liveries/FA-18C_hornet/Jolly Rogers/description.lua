livery = {

	{"f18c1", 0 ,"F18C_1_DIFF_gray84",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};

	
	{"f18c2", 0 ,"F18C_2_DIFF_gray84",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};

	
	
	
	{"F18C_BORT_NUMBER_NOSE_L_100", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_L_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_L_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_NOSE_L_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_L_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_NOSE_L_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_L_01", DECAL ,"F18C_bort_number2",true};
	

	{"F18C_BORT_NUMBER_NOSE_R_100", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_R_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_R_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_NOSE_R_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_R_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_NOSE_R_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_R_01", DECAL ,"F18C_bort_number2",true};	
	
	
	

	
	{"F18C_BORT_NUMBER_ZAK_L_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_L_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_ZAK_L_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_L_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_ZAK_L_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_L_01", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_ZAK_R_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_R_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_ZAK_R_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_R_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_ZAK_R_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_ZAK_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_ZAK_R_01", DECAL ,"F18C_bort_number2",true};

	
	
	{"F18C_BORT_NUMBER_KIL_L_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_L_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_KIL_L_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_L_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_KIL_L_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_L_01", DECAL ,"F18C_bort_number2",true};	
	
	{"F18C_BORT_NUMBER_KIL_R_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_R_100", DECAL ,"F18C_bort_number2",true};
	
	{"F18C_BORT_NUMBER_KIL_R_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_R_10", DECAL ,"F18C_bort_number2",true};	

	{"F18C_BORT_NUMBER_KIL_R_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_R_01", DECAL ,"F18C_bort_number2",true};		

	
	
	
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_L_01", DECAL ,"empty",true};
	

	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_kuw_R_01", DECAL ,"empty",true};		
	


	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_R_01", DECAL ,"empty",true};	
	
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Kuw_L_01", DECAL ,"empty",true};		



	
	

	
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_fin_L_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_fin_L_01", DECAL ,"empty",true};
	


	
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_NOSE_fin_R_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_NOSE_fin_R_01", DECAL ,"empty",true};			
	
	

	
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Switz_R_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_R_01", DECAL ,"empty",true};	
	
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_100", DECAL ,"empty",true};
	
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_KIL_Switz_L_01", 0 ,"F18C_2_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_01", ROUGHNESS_METALLIC ,"F18C_2_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_KIL_Switz_L_01", DECAL ,"empty",true};


	
	
	
	{"F18C_BORT_NUMBER_STV_aus_R_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_STV_aus_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_STV_aus_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_STV_aus_R_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_STV_aus_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_STV_aus_R_01", DECAL ,"empty",true};	
	
	
	{"F18C_BORT_NUMBER_STV_aus_L_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_STV_aus_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_STV_aus_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_STV_aus_L_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_STV_aus_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_STV_aus_L_01", DECAL ,"empty",true};		
		

	{"F18C_BORT_NUMBER_MTW_aus_R_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_MTW_aus_R_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_MTW_aus_R_01", DECAL ,"empty",true};	


	{"F18C_BORT_NUMBER_MTW_aus_L_10", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_10", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_10", DECAL ,"empty",true};	

	{"F18C_BORT_NUMBER_MTW_aus_L_01", 0 ,"F18C_1_DIFF_gray84",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_01", ROUGHNESS_METALLIC ,"F18C_1_DIF_Blue_Angels_gray84_RoughMet",false};
	{"F18C_BORT_NUMBER_MTW_aus_L_01", DECAL ,"empty",true};

	{"FPU_8A", 0 ,"FPU_8A_gray84",false};
	{"FPU_8A", 2 ,"FPU_8A_Diff_gray84_RoughMet",false};

	{"pilot_F18", 0 ,"pilot_F18_gray84",false};
	{"pilot_F18", 2 ,"pilot_F18_roughmet",true};
	{"pilot_F18_patch", 0 ,"empty",true};



	
}
name = "Jolly Rogers"

countries = {"RUS","UKR","USA","TUR","UK","FRA","GER","AUSAF","CAN","SPN","NETH","BEL","NOR","DEN","ISR","GRG","INS","ABH",
"RSO","ITA","AUS","SUI","AUT","BLR","BGR","CZE","CHN","HRV","EGY","FIN","GRC","HUN","IND","IRN","IRQ","JPN","KAZ","PRK",
"PAK","POL","ROU","SAU","SRB","SVK","KOR","SWE","SYR","YEM","VNM","VEN","TUN","THA","SDN","PHL","MAR","MEX","MYS",
"LBY","JOR","IDN","HND","ETH","CHL","BRA","BHR","NZG","YUG","SUN","RSI","DZA","KWT","QAT","OMN","ARE","CUB","RSA"}
order     = 999