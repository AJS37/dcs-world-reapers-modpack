-- livery made by PorcoRosso86

livery = {
	{"F16_bl50_Kil", 0, "f16_bl50_kil", false};
	{"F16_bl50_Kil", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_Main_1", 0, "f16_bl50_main_1", false};
	{"F16_bl50_Main_1", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_Main_1", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_Main_2", 0, "f16_bl50_main_2", false};
	{"F16_bl50_Main_3", 0, "f16_bl50_main_3", false};
	{"F16_bl50_Main_3", NORMAL_MAP, "f16_bl50_main_3_Normal", false};
	{"F16_bl50_Main_3", ROUGHNESS_METALLIC, "f16_bl50_main_3_RoughMet", false};
	{"F16_bl50_Main_2", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_wing_L", 0, "f16_bl50_wing_l", false};
	{"F16_bl50_wing_L", ROUGHNESS_METALLIC, "F16_bl50_wing_L_RoughMet", false};
	{"F16_bl50_wing_R", 0, "f16_bl50_wing_r", false};
	{"F16_bl50_wing_R", ROUGHNESS_METALLIC, "F16_bl50_wing_R_RoughMet", false};		
--	{"F16_bl50_Wing_Pylon_2", 0, "f16_bl50_wing_pylon_2", false};
--  {"F16_bl50_Wing_Pylon_2", NORMAL_MAP, "f16_bl50_wing_pylon_2_normal", false};
--	{"F16_bl50_Wing_Pylon_2", ROUGHNESS_METALLIC, "f16_bl50_wing_pylon_2_roughmet", false};
--	{"F16_bl50_Wing_Pylon_1", 0, "f16_bl50_wing_pylon_1", false};
	--{"F16_bl50_Wing_Pylon_1", NORMAL_MAP, "f16_bl50_wing_pylon_1_normal", false};
--	{"F16_bl50_Wing_Pylon_1", ROUGHNESS_METALLIC, "f16_bl50_wing_pylon_1_roughmet", false};
	{"F16_bl50_GLASS", 0, "F16_bl50_Glass_new", false};
	{"F16_bl50_GLASS", SPECULAR, "F16_bl50_Glass_RoughMet", false};
	{"F16_bl50_GLASS", 14, "F16_bl50_Glass", false};
	{"F16_bl50_Engine", 0, "F16_bl50_Engine", false};
	{"LAU_129", 0 , "lau_129_diff", false};
--	{"LAU_129", NORMAL_MAP, "lau_129_nm", false};
--	{"LAU_129", ROUGHNESS_METALLIC, "lau_129_diff_roughmet", false};
	{"Tank_370", DIFFUSE, "fuel_tank_370gal_diff", false};
	{"Tank_370", ROUGHNESS_METALLIC, "fuel_tank_370gal_diff_RoughMet", false};
	{"PTB_300Gal", 0, "fuel_tank_300gal", false};
	{"PTB_300Gal", ROUGHNESS_METALLIC, "fuel_tank_300gal_roughmet", false};
	{"pilot_F16_patch", 0, "pilot_f16_patch", false};
	{"pilot_F16_patch", NORMAL_MAP, "pilot_f16_patch_nm", false};
	{"pilot_F16_patch", SPECULAR , "empty", true};
	
	{"F16_bl50_FIN_BORT_NUMBER_100", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_100", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_FIN_BORT_NUMBER_001", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_001", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_001",	DECAL				,	"empty", true};
	{"F16_bl50_FIN_BORT_NUMBER_010", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_010", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_010",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_1_DECAL", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_DECAL", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_DECAL", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_DECAL",	DECAL				,	"empty", true};
	
	{"F16_bl50_FIN_DECAL", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_DECAL", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_DECAL",	DECAL				,	"empty", true};
}

name = "USAF 64th Aggresso 57th ATG By PorcoRosso86"
countries = {"USA"}