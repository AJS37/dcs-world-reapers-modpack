livery = {

	{"F16_bl50__GLASS_��Lear", 0, "f16_bl50_glass", false};
	{"F16_bl50__GLASS_��Lear", ROUGHNESS_METALLIC, "f16_bl50_glass_roughmet", false};
	{"F16_bl50__GLASS_��Lear", 0, "f16_bl50_glass_grey", false};


	{"F16_bl50_GLASS", 0, "f16_bl50_glass", false};
	{"F16_bl50_GLASS", ROUGHNESS_METALLIC, "f16_bl50_glass_roughmet", false};
	{"F16_bl50_GLASS", 0, "f16_bl50_glass_yellow", false};
	
	{"F16C_afterburn", 0, "f16c_bl50_afterburn", false};
	{"F16C_afterburn_bok", 0, "f16c_bl50_afterburn", false};
	{"F16C_afterburn_bok", 0, "f16c_bl50_afterburn", false};
	{"F16C_afterburn", 0, "f16c_bl50_afterburn", false};

	{"AAQ_28", DIFFUSE, "aaq_28_diff", false};
	{"AAQ_28", ROUGHNESS_METALLIC, "aaq_28_diff_roughmet", false};
	{"AAQ_28", NORMAL_MAP, "aaq_28_nm", false};	
	{"AAQ_28_Glass", 0, "aaq_28_glass", false};
	{"AAQ_28_Glass", ROUGHNESS_METALLIC, "aaq_28_glass_roughmet", false};


	{"F16_bl50_glass", 0 ,"F16_bl50_Glass_new",false};	

	{"F16_bl50_Kil", 0 ,"F16_bl50_Kil",false};
	{"F16_bl50_Kil", 13 ,"F16_bl50_Kil_RoughMet",false};
	{"F16_bl50_Kil", 1,"F16_bl50_Kil_Normal",false};

  

	{"F16_bl50_Main_1", 0 ,"F16_bl50_Main_1",false};
	{"F16_bl50_Main_1", ROUGHNESS_METALLIC, "f16_bl50_main_1_roughmet", false};
	{"F16_bl50_Main_1", 1,"F16_bl50_Main_1_Normal",false};



	
	{"F16_bl50_Main_2", 0 ,"F16_bl50_Main_2",false};
	{"F16_bl50_Main_2", 13 ,"F16_bl50_Main_2_RoughMet",false};






            {"F16_bl50_Main_3", 0 ,"F16_bl50_Main_3",false};
	{"F16_bl50_Main_3", 13 ,"F16_bl50_Main_3_RoughMet",false};




	{"F16_bl50_Engine", 0, "f16_bl50_engine", false};

	
	{"F16_bl50_wing_L", 0 ,"F16_bl50_wing_L",false};
	{"F16_bl50_wing_L", 13 ,"F16_bl50_wing_L_RoughMet",false};

	{"F16_bl50_wing_L", 1,"F16_bl50_wing_L_Normal",false};

	
	{"F16_bl50_wing_R", 0 ,"F16_bl50_wing_R",false};
	{"F16_bl50_wing_R", 13 ,"F16_bl50_wing_R_RoughMet",false};		

	{"F16_bl50_wing_R", 1,"F16_bl50_wing_R_Normal",false};
	
	{"F16_bl50_GLASS", 0 ,"F16_bl50_Glass_new",false};

	{"F16_bl50_Tyres",0,"F16_bl50_Tyres",false};
	{"F16_bl50_Tyres", 13 ,"F16_bl50_Tyres_RoughMet",false};

	
	{"Tank_370", DIFFUSE, "fuel_tank_370gal_diff", false};
	{"PTB_300Gal", 0, "fuel_tank_300gal", false};
	
}

countries = {"USA"}