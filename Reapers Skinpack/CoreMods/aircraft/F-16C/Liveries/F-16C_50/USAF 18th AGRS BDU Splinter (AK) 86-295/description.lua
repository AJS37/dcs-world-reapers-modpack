livery = {
--F16_bl50_Kil--
	{"F16_bl50_Kil", DIFFUSE ,"F16_bl50_Kil",false};
	{"F16_bl50_Kil", ROUGHNESS_METALLIC, "f16_bl50_kil_roughmet", false};

--F16_bl50_Main_1--
	{"F16_bl50_Main_1", 0, "f16_bl50_main_1", false};
	{"F16_bl50_Main_1", ROUGHNESS_METALLIC, "f16_bl50_main_1_roughmet", false};


--Other parts--
	{"F16_bl50_Engine", 0, "F16_bl50_Engine",false};

--F16_bl50_Main_2--
	{"F16_bl50_Main_2", DIFFUSE, "F16_bl50_Main_2",false};
	{"F16_bl50_Main_2", ROUGHNESS_METALLIC, "f16_bl50_main_2_roughmet", false};

--F16_bl50_Main_3--	
	{"F16_bl50_Main_3", DIFFUSE, "F16_bl50_Main_3",false};
	{"F16_bl50_Main_3", ROUGHNESS_METALLIC, "f16_bl50_main_3_roughmet", false};

--F16_bl50_wing_L--	
	{"F16_bl50_wing_L", DIFFUSE,"F16_bl50_wing_L",false};
	{"F16_bl50_wing_L", ROUGHNESS_METALLIC, "f16_bl50_wing_l_roughmet", false};

--F16_bl50_wing_R--	
	{"F16_bl50_wing_R", DIFFUSE, "F16_bl50_wing_R",false};
	{"F16_bl50_wing_R", ROUGHNESS_METALLIC, "f16_bl50_wing_r_roughmet", false};

--Pylon--
	{"F16_bl50_Wing_Pylon_1", DIFFUSE, "F16_bl50_Wing_Pylon_1_usaf",false};
	{"F16_bl50_Wing_Pylon_2", DIFFUSE, "F16_bl50_Wing_Pylon_2_usaf",false};
	{"LAU_129", DIFFUSE, "lau_129_diff_usaf", false};

--Pilot--
	{"pilot_F16_patch", DIFFUSE, "pilot_F16_patch",false};
	{"pilot_F16_patch", ROUGHNESS_METALLIC,"pilot_F16_patch_roughmet",false};
	{"pilot_F16_patch", 1, "pilot_F16_patch_nm",false};
	
--Bort number--
--F16_bl50_NOUSE_1_DECAL
    {"F16_bl50_NOUSE_1_DECAL",				0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_NOUSE_1_DECAL",				1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_DECAL",				ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_DECAL",				DECAL,	            "empty",                    true};

--F16_bl50_NOUSE_1_BORT_NUMBER_001
    {"F16_bl50_NOUSE_1_BORT_NUMBER_001",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	DECAL,	            "empty",                    true};

--F16_bl50_NOUSE_1_BORT_NUMBER_010
    {"F16_bl50_NOUSE_1_BORT_NUMBER_010",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	DECAL,	            "empty",                    true};

--F16_bl50_NOUSE_1_BORT_NUMBER_100
    {"F16_bl50_NOUSE_1_BORT_NUMBER_100",	0,	                "f16_bl50_main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	DECAL,	            "empty",				    true};
	{"F16_bl50_Main_2", 					0, 					"f16_bl50_main_2",          false};
	{"F16_bl50_Main_2", 					ROUGHNESS_METALLIC, "f16_bl50_main_2_roughmet", false};
	
--F16_bl50_NOUSE_2_BORT_NUMBER_001
    {"F16_bl50_NOUSE_2_BORT_NUMBER_001",	0,	                "f16_bl50_main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	ROUGHNESS_METALLIC,	"f16_bl50_main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	DECAL,	            "empty", 					true};
--F16_bl50_NOUSE_2_BORT_NUMBER_010
    {"F16_bl50_NOUSE_2_BORT_NUMBER_010",	0,	                "f16_bl50_main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	ROUGHNESS_METALLIC,	"f16_bl50_main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	DECAL,	            "empty", 					true};
	
--F16_bl50_NOUSE_2_BORT_NUMBER_100
    {"F16_bl50_NOUSE_2_BORT_NUMBER_100",	0,	                "f16_bl50_main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	ROUGHNESS_METALLIC,	"f16_bl50_main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	DECAL,	            "empty", 					true};
	{"F16_bl50_Main_3", 					0,				    "f16_bl50_main_3", 			false};
	{"F16_bl50_Main_3", 					ROUGHNESS_METALLIC, "f16_bl50_main_3_roughmet", false};
	{"F16_bl50_Kil", 						0,				    "f16_bl50_kil", 		    false};
	{"F16_bl50_Kil", 						ROUGHNESS_METALLIC, "f16_bl50_kil_roughmet",    false};
	
--F16_bl50_FIN_DECAL	
    {"F16_bl50_FIN_DECAL",					0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_DECAL",					1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_DECAL",					ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_DECAL",					DECAL,	            "empty",					true};

--F16_bl50_FIN_BORT_NUMBER_001
    {"F16_bl50_FIN_BORT_NUMBER_001",		0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_001",		1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_001",		ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_001",		DECAL,	            "empty",					true};

--F16_bl50_FIN_BORT_NUMBER_010
    {"F16_bl50_FIN_BORT_NUMBER_010",		0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_010",		1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_010",		ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_010",		DECAL,	            "empty",					true};
	
--F16_bl50_FIN_BORT_NUMBER_100
    {"F16_bl50_FIN_BORT_NUMBER_100",		0,	                "f16_bl50_kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_100",		1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_100",		ROUGHNESS_METALLIC,	"f16_bl50_kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_100",		DECAL,              "empty",					true};	

	
}
name = "USAF 18th AGRS Splinter (AK) 86-295"
--countries = {"USA",}
--Livery by 59th_Jack (2020)