-- livery made by PorcoRosso86

livery = {
	{"F16_bl50_Kil", 0, "f16_bl50_kil", false};
	{"F16_bl50_Kil", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_Main_1", 0, "f16_bl50_main_1", false};
	{"F16_bl50_Main_1", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_Main_1", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_Main_2", 0, "f16_bl50_main_2", false};
	{"F16_bl50_Main_3", 0, "f16_bl50_main_3", false};
	{"F16_bl50_Main_3", NORMAL_MAP, "f16_bl50_main_3_Normal", false};
	{"F16_bl50_Main_3", ROUGHNESS_METALLIC, "f16_bl50_main_3_RoughMet", false};
	{"F16_bl50_Main_2", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_wing_L", 0, "f16_bl50_wing_l", false};
	{"F16_bl50_wing_L", ROUGHNESS_METALLIC, "F16_bl50_wing_L_RoughMet", false};
	{"F16_bl50_wing_R", 0, "f16_bl50_wing_r", false};
	{"F16_bl50_wing_R", ROUGHNESS_METALLIC, "F16_bl50_wing_R_RoughMet", false};		
	{"F16_bl50_glass", 0, "F16_bl50_Glass", false};
	{"F16_bl50_GLASS", 14, "F16_bl50_Glass_new", false};
	{"F16_bl50_GLASS", SPECULAR, "F16_bl50_Glass_RoughMet", false};
	{"F16_bl50_Engine", 0, "F16_bl50_Engine", false};
	{"Tank_370", DIFFUSE, "fuel_tank_370gal_diff", false};
	{"Tank_370", ROUGHNESS_METALLIC, "fuel_tank_370gal_diff_RoughMet", false};
	{"PTB_300Gal", 0, "fuel_tank_300gal", false};
	{"PTB_300Gal", ROUGHNESS_METALLIC, "fuel_tank_300gal_roughmet", false};
	{"pilot_F16_patch", 0, "pilot_f16_patch", false};
	{"pilot_F16_patch", NORMAL_MAP, "pilot_f16_patch_nm", false};
	{"pilot_F16_patch",	 ROUGHNESS_METALLIC,	"pilot_f16_patch_roughmet",		false};
	{"LAU_129", 0 , "lau_129_diff", false};
	
	{"AAQ_28_Glass", 0, "aaq_28_glass", false};
    {"AAQ_28_Glass", ROUGHNESS_METALLIC, "aaq_28_glass_roughmet", false};
	
	{"F16_bl50_Tyres",	DIFFUSE			,	"f16_bl50_tyres", false};
	{"F16_bl50_Tyres",	NORMAL_MAP			,	"f16_bl50_tyres_normal", false};
	{"F16_bl50_Tyres",	SPECULAR			,	"f16_bl50_tyres_roughmet", false};
	
	{"F16_bl50_Gear",	DIFFUSE			,	"f16_bl50_gear", false};
	{"pilot_F16_helmet_glass",	DIFFUSE			,	"pilot_f16_visor", false};
	{"pilot_F16_helmet_glass",	SPECULAR			,	"pilot_f16_visor_roughmet", false};
	{"pilot_F16_helmet_glass",	14,	"pilot_f16_visor_filtr", false};
	{"pilot_F16",	DIFFUSE			,	"pilot_f16", false};
	{"f16c_cockpit_1",	DIFFUSE			,	"f16c_cockpit_1", false};
	{"pilot_F16_helmet",	DIFFUSE			,	"pilot_f16_helmet", false};
	{"pilot_F16_helmet",	NORMAL_MAP			,	"pilot_f16_helmet_nm", false};

	{"F16_bl50_FIN_BORT_NUMBER_100", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_100", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_FIN_BORT_NUMBER_001", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_001", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_001",	DECAL				,	"empty", true};
	{"F16_bl50_FIN_BORT_NUMBER_010", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_BORT_NUMBER_010", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_BORT_NUMBER_010",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001", 0, "f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001", ROUGHNESS_METALLIC, "f16_bl50_main_2_RoughMet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_1_BORT_NUMBER_X100",	DIFFUSE			,	"f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_X100",	NORMAL_MAP			,	"f16_bl50_main_1_normal", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_X100",	SPECULAR			,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_X100",	DECAL				,	"empty", true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_X001",	DIFFUSE			,	"f16_bl50_main_2", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_X001",	SPECULAR			,	"f16_bl50_main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_X001",	DECAL				,	"empty", true};
	
	{"F16_bl50_NOUSE_1_DECAL", 0, "f16_bl50_main_1", false};
	{"F16_bl50_NOUSE_1_DECAL", NORMAL_MAP, "F16_bl50_Main_1_Normal", false};
	{"F16_bl50_NOUSE_1_DECAL", ROUGHNESS_METALLIC, "F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_DECAL",	DECAL				,	"empty", true};
	
	{"F16_bl50_FIN_DECAL", 0, "f16_bl50_kil", false};
	{"F16_bl50_FIN_DECAL", ROUGHNESS_METALLIC, "F16_bl50_Kil_RoughMet", false};
	{"F16_bl50_FIN_DECAL",	DECAL				,	"empty", true};
}

name = "USAF 93 Fighter Squadron 'Makos' by PorcoRosso86"
countries = {"USA"}