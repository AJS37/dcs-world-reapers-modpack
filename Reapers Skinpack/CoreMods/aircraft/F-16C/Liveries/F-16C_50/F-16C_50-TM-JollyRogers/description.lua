livery = {
	-- Livery painted by T.Jung "Neilus" (simfog.com) Version: 0.9 - 2019-12-08
	{"F16_bl50_Kil", 0 ,"F16_bl50_Kil.dds",false};
	{"F16_bl50_Kil",ROUGHNESS_METALLIC,"F16_bl50_Kil_RoughMet",false};
	{"F16_bl50_Main_1", 0 ,"F16_bl50_Main_1",false};
	{"F16_bl50_Main_1",ROUGHNESS_METALLIC,"F16_bl50_Main_1_RoughMet",false};
	{"F16_bl50_Main_2", 0 ,"F16_bl50_Main_2",false};
	{"F16_bl50_Main_3", 0 ,"F16_bl50_Main_3",false};
	{"F16_bl50_wing_L", 0 ,"F16_bl50_wing_L.dds",false};
	{"F16_bl50_wing_R", 0 ,"F16_bl50_wing_R.dds",false};
	
}
name = "TM JollyRogers"
--countries = {"USA"}
