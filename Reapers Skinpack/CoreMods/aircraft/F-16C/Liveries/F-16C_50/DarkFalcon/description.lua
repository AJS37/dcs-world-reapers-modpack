livery = {

	--Extra files for the TB
	{"LAU_129", DIFFUSE, "lau_129_diff", false}; -- Pylon 1 and 9
	{"F16_bl50_Wing_Pylon_2", 0, "f16_bl50_wing_pylon_2", false}; --Wing Pylons
	{"F16_bl50_Wing_Pylon_1", 0, "f16_bl50_wing_pylon_1", false}; --Wing Pylons
	{"F16_bl50_Gear", 0, "f16_bl50_gear", false};
	{"F16_bl50_Engine", 0, "f16_bl50_engine", false};
	{"F16_bl50_Engine", NORMAL_MAP, "f16_bl50_engine_normal", false};
	{"F16_bl50_Engine", ROUGHNESS_METALLIC, "f16_bl50_engine_roughmet", false};
	{"PTB_300Gal", 0,	                        "Fuel_Tank_300Gal",		            false};
	{"PTB_300Gal", 1,	                        "Fuel_Tank_300Gal_Normal",		    true};
	{"PTB_300Gal", ROUGHNESS_METALLIC,	    	"Fuel_Tank_300Gal_RoughMet",	    false};
	{"Smokewinder",    0 , "Smokewinder_Diff", false};
	--Extra files for the TB
	
	--Normal Texture Files
	{"F16_bl50_Kil", 0 ,"F16_bl50_Kil",false};
	{"F16_bl50_Kil", 13 ,"F16_bl50_Kil_RoughMet",false};
	
	{"F16_bl50_Main_1", 0 ,"F16_bl50_Main_1",false};
	{"F16_bl50_Main_1", 13 ,"F16_bl50_Main_1_RoughMet",false};
	
	{"F16_bl50_Main_2", 0 ,"F16_bl50_Main_2",false};
	{"F16_bl50_Main_2", 13 ,"F16_bl50_Main_2_RoughMet",false};
		
    {"F16_bl50_Main_3", 0 ,"F16_bl50_Main_3",false};
	{"F16_bl50_Main_3", 13 ,"F16_bl50_Main_3_RoughMet",false};
	
	{"F16_bl50_wing_L", 0 ,"F16_bl50_wing_L",false};
	{"F16_bl50_wing_L", 13 ,"F16_bl50_wing_L_RoughMet",false};
	
	{"F16_bl50_wing_R", 0 ,"F16_bl50_wing_R",false};
	{"F16_bl50_wing_R", 13 ,"F16_bl50_wing_R_RoughMet",false};	
	
	{"pilot_F16_helmet", 0, "pilot_F16_helmet",false};
	
	{"pilot_F16_patch", 0, "pilot_F16_patch",false};
	
	{"pilot_F16", 0, "pilot_F16", false};
	--Normal Texture Files
	
------BORT NUMBERS------
    {"F16_bl50_NOUSE_1_DECAL",	0,	                "F16_bl50_Main_1",			false};
	{"F16_bl50_NOUSE_1_DECAL",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_DECAL",	ROUGHNESS_METALLIC,	"F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_DECAL",	DECAL,	            "F16_AF_91_Decal", false};
    {"F16_bl50_NOUSE_1_BORT_NUMBER_001",	0,	                "F16_bl50_Main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	ROUGHNESS_METALLIC,	"F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_001",	DECAL,	            "F16_bort_number", false};
    {"F16_bl50_NOUSE_1_BORT_NUMBER_010",	0,	                "F16_bl50_Main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	ROUGHNESS_METALLIC,	"F16_bl50_Main_1_RoughMet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_010",	DECAL,	            "F16_bort_number", false};
    {"F16_bl50_NOUSE_1_BORT_NUMBER_100",	0,	                "F16_bl50_Main_1",			false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	1,	                "f16_bl50_main_1_normal",	true};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	ROUGHNESS_METALLIC,	"f16_bl50_main_1_roughmet", false};
	{"F16_bl50_NOUSE_1_BORT_NUMBER_100",	DECAL,	            "F16_bort_number", false};

    {"F16_bl50_NOUSE_2_BORT_NUMBER_001",	0,	                "f16_bl50_Main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	ROUGHNESS_METALLIC,	"f16_bl50_Main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_001",	DECAL,	            "F16_bort_number", false};
    {"F16_bl50_NOUSE_2_BORT_NUMBER_010",	0,	                "f16_bl50_Main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	ROUGHNESS_METALLIC,	"f16_bl50_Main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_010",	DECAL,	            "F16_bort_number", false};
    {"F16_bl50_NOUSE_2_BORT_NUMBER_100",	0,	                "f16_bl50_Main_2",			false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	1,	                "f16_bl50_main_2_normal",	true};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	ROUGHNESS_METALLIC,	"f16_bl50_Main_2_roughmet", false};
	{"F16_bl50_NOUSE_2_BORT_NUMBER_100",	DECAL,	            "F16_bort_number", false};
	
    {"F16_bl50_FIN_DECAL",	0,	                "F16_bl50_Kil",				false};
	{"F16_bl50_FIN_DECAL",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_DECAL",	ROUGHNESS_METALLIC,	"F16_bl50_Kil_roughmet",	false};
	{"F16_bl50_FIN_DECAL",	DECAL,	            "F16_AF_91_Decal",	false};
    {"F16_bl50_FIN_BORT_NUMBER_001",	0,	                "F16_bl50_Kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_001",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_001",	ROUGHNESS_METALLIC,	"F16_bl50_Kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_001",	DECAL,	                  "F16_bort_number",	false};
    {"F16_bl50_FIN_BORT_NUMBER_010",	0,	                "F16_bl50_Kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_010",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_010",	ROUGHNESS_METALLIC,	"F16_bl50_Kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_010",	DECAL,	                  "F16_bort_number",	false};
    {"F16_bl50_FIN_BORT_NUMBER_100",	0,	                "F16_bl50_Kil",				false};
	{"F16_bl50_FIN_BORT_NUMBER_100",	1,	                "f16_bl50_kil_normal",		true};
	{"F16_bl50_FIN_BORT_NUMBER_100",	ROUGHNESS_METALLIC,	"F16_bl50_Kil_roughmet",	false};
	{"F16_bl50_FIN_BORT_NUMBER_100",	DECAL,	                   "F16_bort_number",	false};
------BORT NUMBERS------

	--[[
	{"Fuel_Tank_300Gal", 0,	                        "Fuel_Tank_300Gal",		            false};
	{"Fuel_Tank_300Gal", 1,	                        "Fuel_Tank_300Gal_Normal",		    true};
	{"Fuel_Tank_300Gal", ROUGHNESS_METALLIC,	    "Fuel_Tank_300Gal_RoughMet",	    false};
	{"F_16_Tank_370", 0,	                        "F-16_Tank_370_diff",		        false};
	{"F_16_Tank_370", 1,	                        "F-16_Tank_370_NM",		            true};
	{"F_16_Tank_370", ROUGHNESS_METALLIC,	        "F-16_Tank_370_diff_RoughMet",	    false};
	]]--
	--{"Tank_370", 0, 						"fuel_tank_370gal_diff", 		false};
	--{"Tank_370", 1,	                        "fuel_tank_370gal_NM",				true};
	--{"Tank_370", ROUGHNESS_METALLIC, 		"fuel_tank_370gal_diff_RoughMet", 	false};
}
name = "DarkFalcon"
countries = {"USA","BEL","FRA"}
--- created by TOTOTOM ---