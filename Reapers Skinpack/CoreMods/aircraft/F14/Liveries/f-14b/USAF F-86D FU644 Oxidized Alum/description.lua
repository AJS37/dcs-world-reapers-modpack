livery = {


--Diffuse  -----------------------------------

{"HB_F14_EXT_01", 0 ,"HB_F14_EXT_01",false};
{"HB_F14_EXT_01", 13 ,"HB_F14_EXT_01_RoughMet",false};
{"HB_F14_EXT_02", 0 ,"HB_F14_EXT_02",false};
{"HB_F14_EXT_02", 13 ,"HB_F14_EXT_02_RoughMet",false};
{"HB_F14_EXT_03", 0 ,"HB_F14_EXT_03",false};
{"HB_F14_EXT_03", 13 ,"HB_F14_EXT_03_RoughMet",false};
{"HB_F14_EXT_04", 0 ,"HB_F14_EXT_04",false};
{"HB_F14_EXT_04", 13 ,"HB_F14_EXT_04_RoughMet",false};
{"HB_F14_EXT_TAIL", 0 ,"HB_F14_EXT_TAIL",false};
{"HB_F14_EXT_TAIL", 13 ,"HB_F14_EXT_TAIL_RoughMet",false};
{"HB_F14_EXT_DROPTANKS", 0 ,"HB_F14_EXT_DROPTANK",false};
{"HB_F14_EXT_DROPTANKS", 13 ,"HB_F14_EXT_DROPTANK_RoughMet",false};
{"HB_F14_WING_LEFT_01", 0 ,"HB_F14_WING_LEFT",false};
{"HB_F14_WING_LEFT_01", 13 ,"HB_F14_WING_LEFT_RoughMet",false};
{"HB_F14_WING_RIGHT", 0 ,"HB_F14_WING_RIGHT",false};
{"HB_F14_WING_RIGHT", 13 ,"HB_F14_WING_RIGHT_RoughMet",false};
{"HB_F14_TCS", 0 ,"HB_F14_EXT_TCS",false};
{"HB_F14_TCS", 13 ,"HB_F14_TCS_RoughMet",true};
{"HB_F14_EXT_PYLONS", 0 ,"HB_F14_EXT_PYLONS_01",false};
{"HB_F14_EXT_PHOENIXRAILS", 0 ,"HB_F14_EXT_PHOENIXPYLONS",false};
{"HB_F14_AIM-54", 0 ,"HB_F14_AIM-54",false};
{"HB_F14_AIM-54", 13 ,"HB_F14_AIM-54_RoughMet",false};
{"HB_F14_EXT_PILOT", 0 ,"HB_F14_EXT_PILOT",false};
{"HB_F14_EXT_PILOT_HELMET", 0 ,"HB_F14_EXT_PILOT_HELMET",false};
{"HB_F14_EXT_PILOT_HELMET", 1 ,"HB_F14_EXT_PILOT_HELMET_NORMAL",false};

{"HB_F14_EXT_RIO_HELMET", 0 ,"HB_F14_EXT_RIO_HELMET",false};
{"HB_F14_LOD1_3in1", 0 ,"HB_F14_LOD1_3in1",false};
{"HB_F14_LOD1_3in1", 13 ,"HB_F14_LOD1_3in1_RoughMet",false};
{"HB_F14_EXT_PILOT_SUIT", 0 ,"HB_F14_EXT_PILOT_SUIT",false};
{"HB_F14_EXT_RIO_SUIT", 0 ,"HB_F14_EXT_RIO_SUIT",false};
}
name = "USAF F-86D FU644 Oxidized AL by 000rick000"

--countries = {"USA", "RUS", "FRA", "UKR", "SPN", "NETH", "TUR", "BEL", "GER", "NOR", "CAN", "DEN", "UK", "GRG", "ISR", "ABH", "RSO"}