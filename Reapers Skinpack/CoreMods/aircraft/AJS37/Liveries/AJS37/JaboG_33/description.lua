livery = {


{"LNS_VIG_EXT_FUS_T", 0 ,"LNS_VIG_FUSELAGE_T",false};
{"LNS_VIG_EXT_FUS_L", 0 ,"LNS_VIG_FUSELAGE_L",false};
{"LNS_VIG_EXT_FUS_R", 0 ,"LNS_VIG_FUSELAGE_R",false};

{"LNS_VIG_EXT_FUS_L", 2 ,"LNS_VIG_FUSELAGE_L_S",false};
{"LNS_VIG_EXT_FUS_R", 2 ,"LNS_VIG_FUSELAGE_L_S",false};

{"LNS_VIG_EXT_WING_L", 0 ,"LNS_VIG_WING_L",false};
{"LNS_VIG_EXT_WING_R", 0 ,"LNS_VIG_WING_R",false};

{"LNS_VIG_PYLONS_02", 0 ,"Pilon_01",false};
{"LNS_VIG_Pylons_01", 0 ,"Pilon_02",false};

{"LNS_VIG_WING_PYLONS", 0 ,"Pilon_03",false};
{"LNS_VIG_OUTER_PYLONS", 0 ,"Pilon_04",false};

--Roughmet
{"LNS_VIG_EXT_FUS_T", ROUGHNESS_METALLIC ,"LNS_VIG_FUSELAGE_T_Roughmet",true};
{"LNS_VIG_EXT_FUS_L", ROUGHNESS_METALLIC ,"LNS_VIG_FUSELAGE_L_Roughmet",true};
{"LNS_VIG_EXT_FUS_R", ROUGHNESS_METALLIC ,"LNS_VIG_FUSELAGE_R_Roughmet",true};
{"LNS_VIG_EXT_WING_L", ROUGHNESS_METALLIC ,"LNS_VIG_WING_L_Roughmet",true};
{"LNS_VIG_EXT_WING_R", ROUGHNESS_METALLIC ,"LNS_VIG_WING_R_Roughmet",true};
{"LNS_VIG_PYLONS_02", ROUGHNESS_METALLIC ,"Pilon_01_Roughmet",true};
{"LNS_VIG_Pylons_01", ROUGHNESS_METALLIC ,"Pilon_02_Roughmet",true};
{"LNS_VIG_WING_PYLONS", ROUGHNESS_METALLIC ,"Pilon_03_Roughmet",true};
{"LNS_VIG_OUTER_PYLONS", ROUGHNESS_METALLIC ,"Pilon_04_Roughmet",true};
{"LNS_VIG_XTANK", ROUGHNESS_METALLIC , "ln_vig_xtankr_Roughmet", true};
}

name = "Jagdbombergeschwader 33"

--countries = {"GER"}