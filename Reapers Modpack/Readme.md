# Wichtig: Hier finden Sie informationen dazu, wie die jeweiligen Mods in DCS World abzulegen sind. 


Die im Ordner Mods (Rootfolder) enthaltenen Dateien werden mittels des im Modpack enthaltenen Tools "OVGME" im DCS-Hauptverzeichnis hinterlegt. 
Fragen hierzu bitte an [Reaper-02]Berkut. Bei Bedarf wird gerne bei der Einrichtung geholfen!
Achtung: Die Nutzung von OVGME wird dringend empfohlen, da die Mods idR zur Durchführung eines DCS Updates entladen werden sollten! OVGME ermöglicht das Laden und Entladen der Mods mit wenigen Mausklicks.

Die im Ordner Mods (Savegame) hinterlegten Modifikationen werden direkt im Savegame Ordner abgelegt und müssen bei einem DCS Update nicht entladen werden!