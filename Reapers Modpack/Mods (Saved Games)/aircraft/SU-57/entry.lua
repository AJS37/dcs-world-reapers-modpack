local self_ID  = "SU-57 PAK FA by CUBANACE SIMULATIONS" 

----------------------------A project being made in order to learn 3D,2D,animation,Lua,C++ and to better understand DCS ------------------------------------
-----------------------------------------------Project Started on December 16 2016------------------------------------------------------------------------------
   
declare_plugin(self_ID,
{

installed 	 = true,
developer    = CubanAce_Simulations,
dirName	  	 = current_mod_path,
version		 = "2.5.5 Alpha",           
state		 = "installed",                               
info		 = _("The Sukhoi SU-57 is a fifth-generation Multirole Stealh fighter Being Developed by the Russian Air Force"),
--binaries	= { 'SU-57', },  -- The DLL of the external flight model 



InputProfiles =
	{
        ["Su-57"]     = current_mod_path .. '/Input/Su-57',
    },

{

},

Skins	=
	{
		{
			name	= _("SU-57"),
			dir		= "Theme",    
		},
	},
Missions =
	{
		{
			name		= _("Su-57"),
			dir			= "Missions",
		},
	},
LogBook =
	{
		{
			name		= _("Su-57"), 
			type		= "Su-57",    
		},
	},
 encyclopedia_path = current_mod_path..'/Encyclopedia'
})
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------CALLING TEXTURES/SOUNDS/LOADING WINDOW AND SHAPES--------------------------------------------------------------------------------------------
mount_vfs_model_path    (current_mod_path .."/Cockpit/Shape") 
mount_vfs_texture_path  (current_mod_path.."/Theme/ME") 
mount_vfs_sound_path    (current_mod_path.."/Sounds/Pak-Fa/")
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_liveries_path (current_mod_path.."/Liveries")
mount_vfs_texture_path  (current_mod_path.."/Textures/Missile_Textures")
mount_vfs_texture_path  (current_mod_path.."/Textures/OtherTextures")
mount_vfs_texture_path  (current_mod_path.."/Textures/Su-57Skins")
mount_vfs_texture_path  (current_mod_path.."/Cockpit/Textures")
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------CALLING AIRCRAFT/FM/AVIONICS TYPE-------------------------------------------------------------------------------------------------------------
make_flyable('Su-57', 'Mods/aircrafts/ModernAirCombat/Cockpit/KneeboardLeft/',  {nil, old = 4}, current_mod_path..'/Scripts/comm.lua')
--make_flyable('Su-57'	, current_mod_path..'/Cockpit/Scripts/', FM, current_mod_path..'Scripts/comm.lua')
--local support_cockpit = current_mod_path..'/Cockpit/'



----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------CALLING SCRIPTS FILES--------------------------------------------------------------------------------------------------------------------------
dofile(current_mod_path.."/Scripts/SU-57 AeroDynamics.lua") --SU-57 Flight Model  and Engine Table.
dofile(current_mod_path.."/Scripts/Views.lua") --View Settings.
dofile(current_mod_path.."/Scripts/Su-57.lua")--Main Lua
dofile(current_mod_path.."/Scripts/Weapons.lua")--Calling Weapon Types
dofile(current_mod_path.."/Scripts/SU-57OverwingVapor.lua")-- WIP Over wingvapor

-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

--local FM = 
--{
	--[1] = self_ID,
	--[2] = "SU-57",                                    -- DLL binarires for FM
	--center_of_mass		=	{ 0.0 , 0.0 , 0.0},		-- center of mass position relative to object 3d model center for empty aircraft
	--moment_of_inertia  	= 	{0.0, 0.0, 0.0},   	    -- moment of inertia of empty aircraft
	--suspension   		= suspension,                   -- gear posts initialization
--}








----------------------------------------------CALLING VIEWS---------------------------------------------------------------------------------------------------------
make_view_settings('Su-57', ViewSettings, SnapViews) 


plugin_done()--The End 
