

       
dofile(current_mod_path.."/Views.lua")





Su_57 =  {
	Name 				=   'Su-57',
	DisplayName			= _('SU-57 PAK FA Project'),

	Cannon = "yes",
	HumanCockpit 		= true,
	HumanCockpitPath    = current_mod_path..'/Cockpit/Scripts/',

	Picture 			= "Su-57.png",
	Rate 				= 40, 
	Shape 				= "Su-57",
	WorldID             = Su_57; -- Su-57

	shape_table_data 	=
	{
		{
			file  	 = 'Su-57';
			life  	 = 18; 
			vis   	 = 3; 
			desrt    = 'Su-57-destr';
			fire  	 = { 300, 2}; 
			username = 'Su-57';
			index    =  WSTYPE_PLACEHOLDER;
			
		},
		{
			name  = "Su-57-destr";
			file  = "Su-57-destr";
			fire  = { 240, 2};
		},

	},
	
	    LandRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of LandRWCategories
        TakeOffRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of TakeOffRWCategories
	
	
	------------------------- overwing vapor call
	effects_presets = {
        {effect = "OVERWING_VAPOR", file = current_mod_path.."/Scripts/SU-57OverwingVapor.lua"},
    },
	
	
	

	-------------------------
	
	mapclasskey	= "P0091000024",
	attribute	= {wsType_Air, wsType_Airplane, wsType_Fighter,WSTYPE_PLACEHOLDER, Su_57, "Multirole fighters", "Refuelable" },
	Categories 	= {"{78EFB7A2-FD52-4b57-A6A6-3BF0E1D6555F}", "Interceptor",},
	-------------------------
		M_empty	=	18000,
		M_nominal	= 20000,
		M_max	=	35000,
		M_fuel_max	=	10300,
		H_max	=	20000,
		average_fuel_consumption	=	0.4895,
		CAS_min	=	1.58,
		V_opt	=	170,
		V_take_off	= 75,
		V_land	=	65,
		has_afteburner	=	true,
		has_speedbrake	=	true,
		radar_can_see_ground =true,
	
   		AOA_take_off	=	0.17,
	    stores_number	=	12,
		bank_angle_max	=	60,
	    Ny_min		=	-3,
		Ny_max		=	9.5,
		Ny_max_e 	= 	9.5,  
		tand_gear_max = 0.577,
		V_max_sea_level	= 603,
		V_max_h		=	694.44,   
		Vy_max		=	325,  
		Mach_max	=	2.00,  
		tanker_type	=	1,
		wing_area	=	78.8,


		detection_range_max	=400,
		
		is_tanker	=	false,
		wing_span	=	13.95,
		thrust_sum_max	=	24300,
		thrust_sum_ab	=	37000,
		length	=	19.008,
		height	=	4.074,
		flaps_maneuver	=	2,
		range	=	3415,
		RCS	=   0.001,
		IR_emission_coeff	=	1,
		IR_emission_coeff_ab	=	3,
		
		---------------------Landing Gear -------------------------------------------------
		nose_gear_pos 			    = { 7.611, -2.70,    0.000},
		main_gear_pos 			    = {-1.0,   -2.70,    2.000},
		
		nose_gear_wheel_diameter	=	0.5,
		main_gear_wheel_diameter	=	0.8,
		nose_gear_amortizer_direct_stroke    	 =  0,  -- down from nose_gear_pos !!!
	    nose_gear_amortizer_reversal_stroke  	 =  2.25 - 2.587,  -- up
	    main_gear_amortizer_direct_stroke	 	 =  0, --  down from main_gear_pos !!!
	    main_gear_amortizer_reversal_stroke  	 = 	2.417 - 2.819, --  up
	    nose_gear_amortizer_normal_weight_stroke = -0.1,-- down from nose_gear_pos
	    main_gear_amortizer_normal_weight_stroke = -0.1,-- down from main_gear_pos
		------------------------------------------------------------------------------------
		
		
		
		
		
		
		
		
		
		
		
		brakeshute_name	            =	3,
		
	    wing_tip_pos 				= {-3.8,   0.85,    7.100},  
		air_refuel_receptacle_pos   = { 9.220,     0.075,   -1.290}, 
		engines_count	=	2,
		engines_nozzles =
		
		{
			[1] = 
			{
			pos 		=  {-7.50, 0.20, 1.35}, 
			elevation   =  0,  
			diameter	 = 0.85,  
			exhaust_length_ab   = 7.0, 
			exhaust_length_ab_K = 0.629, 
			smokiness_level = 0.1,
			afterburner_effect_texture = "Afterburner_SU57",
			}, 

            [2] = 
			{
			pos 		=  {-7.50, 0.20, -1.35}, 
			elevation   =  0,  
			diameter	 = 0.85,  
			exhaust_length_ab   = 7.0, 
			exhaust_length_ab_K = 0.629, 
			smokiness_level = 0.1,
			afterburner_effect_texture = "Afterburner_SU57",
			}, 
		},

	crew_size	 = 1,
	crew_members =
	{
		[1] =
		{
			ejection_seat_name	= 9,
			drop_canopy_name	= "Su-57-fragment-canopy-glass",
			pos					= {3.035, 0.169, 0.005},
			canopy_pos			= {2.677, 2.677, 0},
			g_suit 			    =  4
		}, -- end of [1]
	}, -- end of crew_members

	fires_pos =
		{
			[1] = 	{-3.484, -0.004, -0.149}, 
			[2] = 	{-2.518,  0.055,  1.216}, 
			[3] = 	{-2.518,  0.055, -1.216}, 
			[4] = 	{-6.250,  0.525,  0.000}, 
			[5] = 	{-6.750,  0.525,  0.000}, 
			[6] = 	{-2.346, -0.448,  0.000}, 
			[7] = 	{ 2.346, -0.448,  0.000}, 
		}, -- end of fires_pos


	-- Countermeasures
	Countermeasures = {
		ECM = "AN/ALQ-135"
	},

	passivCounterm = {
		CMDS_Edit = true,
		SingleChargeTotal = 200,
		chaff = {default = 100, increment = 3, chargeSz = 1},
		flare = {default = 96,  increment = 3, chargeSz = 1},
	},

	chaff_flare_dispenser 	= {

		{ dir =  {-1, 0, -1.0}, pos =   {-3.827,  0.435, -1.108}, }, -- Chaff L
		{ dir =  {-1, 0, 1.0}, pos =   {-3.827,  0.435, 1.108}, },  -- Chaff R
		{ dir =  {-1, 0, -1.0}, pos =  {-5.032,  0.873, -1.2}, }, -- Flares L
		{ dir =  {-1, 0,  1.0}, pos =  {-5.032,  0.873,  1.2}, }, -- Flares R
	},

	--sensors
	detection_range_max		 = 400,               
	radar_can_see_ground 	 = true,  
	sound_name	=	"aircraft\Su-57\Sounds",
	CanopyGeometry = {
	azimuth   = {-120.0, 120.0}, 
	elevation = {-60.0, 90.0}  
	},

     Sensors = {
            RADAR = "N-011M",               
            IRST = "OLS-27",
            RWR = "Abstract RWR"
			
		
			
        },
		
	    Failures = {
			{ id = 'asc', 		label = _('ASC'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'autopilot', label = _('AUTOPILOT'), enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hydro',  	label = _('HYDRO'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'l_engine',  label = _('L-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'r_engine',  label = _('R-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'radar',  	label = _('RADAR'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
		    { id = 'eos',  		label = _('EOS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'helmet',  	label = _('HELMET'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mlws',  	label = _('MLWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'rws',  		label = _('RWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'ecm',   	label = _('ECM'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hud',  		label = _('HUD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mfd',  		label = _('MFD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },		
	}, 
	Guns = {
        gun_mount("GSh_30_1", { count = 150 },{muzzle_pos_connector = "Gun_point", muzzle_pos = {6.0, 0.00, 1.1}}),
	
	},
  
		Pylons = { ---Research has been made in order to make the 12 pylon sections,6 internally and 6 externally.
	

              --Bck/Frwrd    Up/Down  -Left/Right
		pylon(1, 0, -2.500,0.977, -4.899, --1, 0, -1.943000, 0.173000, -7.280000,
            {
			arg = 308 ,arg_value = 0,
			use_full_connector_position=true,
            },
            {
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				--{ CLSID = "{0519A264-0AB6-11d6-9193-00A0249B6F00}" }, --radiation pod
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange
        
            }
        ),
        pylon  (2, 0, -1.285,0.800, -3.500,--2, 0, -2.535000, -0.165000, -6.168000,
		    {
			use_full_connector_position=true,
		      FiZ = -2,                      
            
            },
            {
                { CLSID = "{9B25D316-0434-4954-868F-D51DB1A38DF0}" },
                { CLSID = "{E8069896-8435-4B90-95C0-01A03AE6E400}" },
                { CLSID = "{88DAC840-9F75-4531-8689-B46E64E42E53}" },
                { CLSID = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}" },
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
                { CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}" }, -- ?31?
                { CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE504}" }, -- ?31?
                { CLSID = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}" }, -- ?29?
                { CLSID = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}" }, -- ?29?
                { CLSID = "{40AB87E8-BEFB-4D85-90D9-B2753ACF9514}" }, -- ?59?
				{ CLSID = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}" }, -- ��?100 ��?
                { CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}" },
                { CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}" },
                { CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}" },
                { CLSID = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74884}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74881}" },
                { CLSID = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}" },
                { CLSID = "{4203753F-8198-4E85-9924-6F8FF679F9FF}" },
                { CLSID = "{37DCC01E-9E02-432F-B61D-10C166CA2798}" },
                { CLSID = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}" },
                { CLSID = "{BA565F89-2373-4A84-9502-A0E017D3A44A}" }, -- ��?500?
                { CLSID = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}" }, -- ��?500��
                { CLSID = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}" },
                { CLSID = "{39821727-F6E2-45B3-B1F0-490CC8921D1E}" }, -- ��?1500?
                { CLSID = "{53BE25A4-C86C-4571-9BC0-47D668349595}" },
				
				{ CLSID = "{RVV-BD}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{RVV-L}" },
				{ CLSID = "{E8D4652F-FD48-45B7-BA5B-2AE05BB5A9CF}"},-- Fuel tank 800L Wing
				{ CLSID = "{RN-28}"},
				{ CLSID = "{SU_57Tank}"},
		-----------------------------------------------------------------------------------------------------
		              --AntiShip Cruise missile
				{ CLSID = "{KH_59MK2}"},--antiship missile
		------------------------------------------------------------------------------------------------------		
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange
				
					
            }
        ),
        pylon(3, 1,  0.8500000, 1.200000, -0.00000, --
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange

            }
        ),
		pylon(4,  0, -0.000,.200, 1.300, 
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{9B25D316-0434-4954-868F-D51DB1A38DF0}" },
                { CLSID = "{E8069896-8435-4B90-95C0-01A03AE6E400}" },
                { CLSID = "{88DAC840-9F75-4531-8689-B46E64E42E53}" },
                { CLSID = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}" },
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
                { CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}" }, -- ?31?
                { CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE504}" }, -- ?31?
                { CLSID = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}" }, -- ?29?
                { CLSID = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}" }, -- ?29?
                { CLSID = "{40AB87E8-BEFB-4D85-90D9-B2753ACF9514}" }, -- ?59?
				{ CLSID = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}" }, -- ��?100 ��?
                { CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}" },
                { CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}" },
                { CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}" },
                { CLSID = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74884}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74881}" },
                { CLSID = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}" },
                { CLSID = "{4203753F-8198-4E85-9924-6F8FF679F9FF}" },
                { CLSID = "{37DCC01E-9E02-432F-B61D-10C166CA2798}" },
                { CLSID = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}" },
                { CLSID = "{BA565F89-2373-4A84-9502-A0E017D3A44A}" }, -- ��?500?
                { CLSID = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}" }, -- ��?500��
                { CLSID = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}" },
                { CLSID = "{39821727-F6E2-45B3-B1F0-490CC8921D1E}" }, -- ��?1500?
                { CLSID = "{53BE25A4-C86C-4571-9BC0-47D668349595}" },
				
				{ CLSID = "{RVV-BD}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{RVV-L}" },
				{ CLSID = "{RN-28}"},
				{ CLSID = "{SU_57Tank}"},
				{ CLSID = "{KH_59MK2}"},
            }
        ),
        pylon(5, 1,  0.8500000, 1.200000, 0.00000,
            {
			    use_full_connector_position = true,
				--connector = "Pylon5",
				--arg				= 26,
				--arg_value		= 0,
				--arg_value		= 1,
			
			
			
			
            },
            {
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{KH_59MK2}"},
            }
        ),
        pylon(6, 1,  0.8500000, 1.200000, 0.00000,
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{KH_59MK2}"},
            }
        ),
        pylon(7, 1, 0.8500000, 1.200000, 0.00000,
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{KH_59MK2}" },
            }
        ),
        pylon(8, 1,  0.8500000, 1.200000, 0.00000,
            {
			use_full_connector_position=true,
                FiZ = -2,
            },
            {
               { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
			   { CLSID = "{RVV-AE}" },
			   { CLSID = "{RVV-M}" },
			   { CLSID = "{KH_59MK2}" },
            }
        ),
        pylon(9,  0, -0.000,.200, -1.300, 
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{9B25D316-0434-4954-868F-D51DB1A38DF0}" },
                { CLSID = "{E8069896-8435-4B90-95C0-01A03AE6E400}" },
                { CLSID = "{88DAC840-9F75-4531-8689-B46E64E42E53}" },
                { CLSID = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}" },
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
                { CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}" }, 
                { CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE504}" }, 
                { CLSID = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}" }, 
                { CLSID = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}" }, 
                { CLSID = "{40AB87E8-BEFB-4D85-90D9-B2753ACF9514}" }, 
				{ CLSID = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}" }, 
                { CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}" },
                { CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}" },
                { CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}" },
                { CLSID = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74884}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74881}" },
                { CLSID = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}" },
                { CLSID = "{4203753F-8198-4E85-9924-6F8FF679F9FF}" },
                { CLSID = "{37DCC01E-9E02-432F-B61D-10C166CA2798}" },
                { CLSID = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}" },
                { CLSID = "{BA565F89-2373-4A84-9502-A0E017D3A44A}" }, 
                { CLSID = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}" }, 
                { CLSID = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}" },
                { CLSID = "{39821727-F6E2-45B3-B1F0-490CC8921D1E}" }, 
                { CLSID = "{53BE25A4-C86C-4571-9BC0-47D668349595}" },
				
				{ CLSID = "{RVV-BD}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{RVV-L}" },
				{ CLSID = "{RN-28}"},
				{ CLSID = "{SU_57Tank}"},
				{ CLSID = "{KH_59MK2}"},
            }
        ),
		 pylon(10, 1, 0.8500000 , 1.200000, 0.00000,
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
				
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange
            }
        ),
        pylon(11, 0, -1.285,0.800, 3.500, 
		    {
			use_full_connector_position=true,
		      FiZ = -2,    
            
            },
            {
               { CLSID = "{9B25D316-0434-4954-868F-D51DB1A38DF0}" },
                { CLSID = "{E8069896-8435-4B90-95C0-01A03AE6E400}" },
                { CLSID = "{88DAC840-9F75-4531-8689-B46E64E42E53}" },
                { CLSID = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}" },
                { CLSID = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}" },
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
                { CLSID = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}" }, 
                { CLSID = "{4D13E282-DF46-4B23-864A-A9423DFDE504}" }, 
                { CLSID = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}" }, 
                { CLSID = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}" },
                { CLSID = "{40AB87E8-BEFB-4D85-90D9-B2753ACF9514}" }, 
				{ CLSID = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}" }, 
                { CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}" },
                { CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}" },
                { CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}" },
                { CLSID = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74884}" },
                { CLSID = "{96A7F676-F956-404A-AD04-F33FB2C74881}" },
                { CLSID = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}" },
                { CLSID = "{4203753F-8198-4E85-9924-6F8FF679F9FF}" },
                { CLSID = "{37DCC01E-9E02-432F-B61D-10C166CA2798}" },
                { CLSID = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}" },
                { CLSID = "{BA565F89-2373-4A84-9502-A0E017D3A44A}" }, 
                { CLSID = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}" }, 
                { CLSID = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}" },
                { CLSID = "{39821727-F6E2-45B3-B1F0-490CC8921D1E}" }, -- ��?1500?
                { CLSID = "{53BE25A4-C86C-4571-9BC0-47D668349595}" },
				{ CLSID = "{R-33}" },
				{ CLSID = "{RVV-BD}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{RVV-L}" },
				
				{ CLSID = "{E8D4652F-FD48-45B7-BA5B-2AE05BB5A9CF}"},-- Fuel tank 800L Wing
				{ CLSID = "{SU_57Tank}"},--Fuel tank
				{ CLSID = "{RN-28}"},				--Nuke
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange
		-------------------AntiShip weapons-----------------------------------------------------------
				
			    { CLSID = "{KH_59MK2}" },--antiship Cruise missile
				
				
				
				
				
				
		----------------------------------------------------------------------------------------------			
            }
        ),
        pylon(12, 0, -2.500,0.977, 4.899,
            {
			use_full_connector_position=true,
            },
            {
                { CLSID = "{FBC29BFE-3D24-4C64-B81D-941239D12249}" },
				{ CLSID = "{RVV-AE}" },
				{ CLSID = "{RVV-M}" },
				{ CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B1}" }, --Smoke Generator - red
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B2}" }, --Smoke Generator - green
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B3}" }, --Smoke Generator - blue
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B4}" }, --Smoke Generator - white
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B5}" }, --Smoke Generator - yellow
                { CLSID = "{D3F65166-1AB8-490f-AF2F-2FB6E22568B6}" }, --Smoke Generator - orange
				
    
                
            }
        ),
        
	    --pylon(13, 1, -5.95, 0.45, 0,
          --  {
              --  FiZ = -3,
			--	FiX = -90,
           -- },
           -- {
		     --   { CLSID = "{white}" },
	          --  { CLSID = "{red}" }, 
				--{ CLSID = "{orange}" }, 
				--{ CLSID = "{yellow}" },
				--{ CLSID = "{green}"}, 
				--{ CLSID = "{blue}" }, 
			--	{ CLSID = "{purple}" }, 
			--	{ CLSID = "{light green}"}, 
			--	{ CLSID = "{pink}" }, 
			--	{ CLSID = "{lime green}"}, 	

           -- }
        --),


        LandRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of LandRWCategories
        TakeOffRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of TakeOffRWCategories



	

},
	

	Tasks = {
        aircraft_task(CAP),
        aircraft_task(Intercept),
        aircraft_task(Escort),
        aircraft_task(FighterSweep),
        aircraft_task(AFAC),
        aircraft_task(GroundAttack),
        aircraft_task(RunwayAttack),
		aircraft_task(AntishipStrike),
		aircraft_task(CAS),
		
    },

	DefaultTask = aircraft_task(CAP),

	SFM_Data = { --Aerodynamics-----------SU-57 Airfoil main wing profile NACA64a204,SU-57 Airfoil horizontal wing profile NACA64a202,SU-57 Airfoil fin profile NACA64a003.
	aerodynamics =                                                       ---Development Improvements---
		{
			Cy0	=	0,
			Mzalfa	=	3.355,
			Mzalfadt	=	0.20,
			kjx = 2.75,
			kjz = 0.00125,
			Czbe = -0.016,
			cx_gear = 0.0268,
			cx_flap = 0.05,
			cy_flap = 0.35,
			cx_brk = 0.18,
			table_data =
			{
			--    M	     Cx0		 Cya		 B		      B4	    Omxmax	Aldop	Cymax
				{0.0,	0.0150,		0.100,		0.1,		0.032,		1.65,	35.0,	1.6,	},
				{0.1,	0.0155,		0.087,		0.1,		0.032,		1.65,	35.0,	1.6,	},
				{0.2,	0.0160,		0.057,		0.1,		0.032,		1.95,	35.0,	1.6,	},
				{0.3,	0.0160,		0.055,		0.1,		0.032,		1.95,	35.0,	1.6,	},
				{0.4,	0.0160,		0.053,		0.1,	   	0.032,		3.25,	35.0,	1.6,	},
				{0.5,	0.0160,		0.052,		0.1,	   	0.032,		3.25,	35.0,	1.6,	},
				{0.6,	0.0165,		0.050,		0.094,		0.043,		4.55,	24.0,	1.6,	},
				{0.7,	0.0165,		0.049,		0.094,		0.045,		4.55,	23.0,	1.45,	},
				{0.8,	0.0178,		0.048,		0.094,		0.048,		4.55,	30.0,	1.4,    },
				{0.9,	0.0215,		0.047,		0.11,		0.050,		4.55,	30.0,	1.3,    },
				{1.0,	0.0310,		0.046,		0.15,		0.065,		4.55,	38.0,	1.2,    },
				{1.1,	0.0422,		0.045,	   	0.15,		0.070,		4.10,	36.0,	1.1,	},
				{1.2,	0.0440,		0.044,	   	0.14,		0.080,		3.19,	37.0,	1.05,	},		
				{1.3,	0.0432,		0.043,	   	0.17,		0.096,		2.28,	35.0,	1.0,	},
				{1.4,	0.0432,		0.043,	   	0.17,		0.096,		2.28,	35.0,	1.0,	},
				{1.5,	0.0423,		0.042,	   	0.23,		0.09,		1.95,	33.0,	0.9,	},
				{1.6,	0.0423,		0.041,	   	0.23,		0.09,		1.95,	33.0,	0.9,	},
				{1.7,	0.0423,		0.040,	   	0.23,		0.09,		1.95,	33.0,	0.9,	},
				{1.8,	0.0416,		0.049,	   	0.23,		0.38,		1.17,	23.0,	0.7,	},
				{1.9,	0.0416,		0.049,	   	0.23,		0.38,		1.17,	23.0,	0.7,	},
				{2.0,	0.0416,		0.069,	   	0.08,		2.5,		1.04,	30.5,	0.55,	},
				{2.1,	0.0416,		0.069,	   	0.08,		2.5,		1.04,	30.5,	0.55,	},
				{2.2,	0.0416,		0.070,	   	0.16,		3.2,		0.91,	39.0,	0.4,	},
                {2.3,	0.0416,		0.070,	   	0.16,		3.2,		0.91,	39.0,	0.4,	},
				{2.4,	0.0416,		0.070,	   	0.16,		3.2,		0.91,	39.0,	0.4,	},
				{2.5,	0.0410,		0.070,		0.25,		4.5,		0.91,	39.0,	0.4,	},		
				--{3.9,	0.0395,		0.070,		0.35,		6.0,		0.8,	39.0,	0.4		},			
			}, -- end of table_data

		
		}, -- end of aerodynamics
		engine = ----------- (WIP),Second Stage Engine.
		{
			Nmg	=	67.5,
			MinRUD	=	0,
			MaxRUD	=	1,
			MaksRUD	=	0.85,
			ForsRUD	=	0.91,
			typeng	=	1,
			hMaxEng	=	60000,
			dcx_eng	=	0.0000,
			cemax	=	1.24,
			cefor	=	2.56,
			dpdh_m	=	8000,
			dpdh_f	=	17000.0,
			table_data = {
			--   M		Pmax		 Pfor
					{0,		150000,	200000},
				 	{0.2,	205000,	200040},
				 	{0.4,	250000,	200500},
				 	{0.6,	300000,	200620},
				 	{0.7,	300500,	200700},
				 	{0.8,	300900,	200870},
				 	{0.9,	400500,	200900},
				 	{1,		406000,	201200},
				 	{1.1,	480000,	202000},
				 	{1.2,	500000,	203000},
				 	{1.3,	510000,	205000},
				 	{1.5,	511000,	220000},
				 	{1.8,	520000,	390000},
				 	{2,0,   530020,	600000},
				 	{2.2,	530000, 630000},
				 	
				},
		}, -- end of engine
	},
    --extended = {




	Damage = {
		-- NOSE, COCKPIT & AVIONICS
		[0]	 = {critical_damage =  3, args = {82}},		
		[1]	 = {critical_damage =  8, args = {150}},	
		[2]	 = {critical_damage =  8, args = {149}},	
		[3]	 = {critical_damage =  2, args = {65}},		
		[4]	 = {critical_damage =  8, args = {298}},	
		[5]	 = {critical_damage =  8, args = {299}},	
		[90] = {critical_damage =  5},					
		[86] = {critical_damage =  8, args = {300}},	
		[87] = {critical_damage =  8, args = {301}},	
		[88] = {critical_damage =  8, args = {302}},	

		-- CONTROL SURFACES
		[53] = {critical_damage =  5, args = {248}},	
		[25] = {critical_damage =  5, args = {226}},	
		[51] = {critical_damage =  5, args = {240}},	
		[52] = {critical_damage =  5, args = {238}},	
		[26] = {critical_damage =  5, args = {216}},	
		[21] = {critical_damage =  5, args = {232}},	
		[33] = {critical_damage =  5, args = {230}},	
		[22] = {critical_damage =  5, args = {222}},	
		[34] = {critical_damage =  5, args = {220}},	
		[19] = {critical_damage =  5, args = {183}},	
		[20] = {critical_damage =  5, args = {185}},	

		-- ENGINE & FUEL TANKS
		[11] = {critical_damage = 10, args = {271}},	
		[61] = {critical_damage = 10, args = {224}},	
		[62] = {critical_damage = 10, args = {214}},	
		[65] = {critical_damage = 10, args = {155}},	

		-- FUSELAGE & WINGS
		[39] = {critical_damage = 10, args = {244}},								
		[41] = {critical_damage = 10, args = {245}, deps_cells = {39,53}},			
		[43] = {critical_damage = 10, args = {246}, deps_cells = {41,88}},			
		[23] = {critical_damage = 8,  args = {223}, deps_cells = {21}},				
		[29] = {critical_damage = 9,  args = {224}, deps_cells = {19,23,84}},	    
		[35] = {critical_damage = 10, args = {225}, deps_cells = {23,21,29,33,61,84}},	
		[36] = {critical_damage = 10, args = {215}, deps_cells = {24,22,30,34,62,85}},	
		[30] = {critical_damage = 9,  args = {214}, deps_cells = {20,24,85}},	    
		[24] = {critical_damage = 8,  args = {213}, deps_cells = {22}},				
		[9]	 = {critical_damage = 10, args = {154}},								
		[82] = {critical_damage = 10, args = {152}},								
		[10] = {critical_damage = 10, args = {153}},								
		[55] = {critical_damage = 10, args = {159}},								
		[56] = {critical_damage = 10, args = {158}},								
		[57] = {critical_damage = 10, args = {157}},								
		[58] = {critical_damage = 10, args = {156}},								

		-- LANDING GEAR
		[8]  = {critical_damage = 8, args = {265}, deps_cells = {83}},	
		[15] = {critical_damage = 8, args = {267}, deps_cells = {84}},	
		[16] = {critical_damage = 8, args = {266}, deps_cells = {85}},	
		[83] = {critical_damage = 3, args = {134}},						
		[84] = {critical_damage = 3, args = {135}},						
		[85] = {critical_damage = 3, args = {136}},						

		-- WEAPONS
		[7]  = {critical_damage = 5, args = {296}},						
			
        
	 

	},


	DamageParts =
	{
	
	-------------UNDER CONSTRUCTION---------------------
	
   		[1] = "Su-57-part-wing-R", -- wing R 
   	    [2] = "Su-57-part-wing-L", -- wing L
   		[3] = "Su-57-part-nose", -- nose
   		[4] = "Su-57-part-tail", -- tail
	},

	---------------------------------------------------------
	lights_data = {
		typename = "collection",
		lights = {
			[2] = { typename = "collection", -- form lights aft
				lights = {
					--{typename = "spotlight",argument = 208,dir_correction = {elevation = math.rad(3)}}, -- form front
					{typename = "spotlight",argument = 209,connector = "NOSE_TAXI_SPOT", dir_correction = {elevation = math.rad(0)}}, -- form aft
					--{typename = "natostrobelight",connector = "TOP_BEACON",argument_1 = 83,period = 2.0,color = {1.0, 0.0, 0.0},phase_shift = 0.0},
					--{typename = "natostrobelight",connector = "BOTTOM_BEACON",argument_1 = 802,period = 2.0,color = {1.0, 0.0, 0.0},phase_shift = 0.0},
				},
			},
			[3] = { typename = "collection", -- left nav light
				lights = {
					{typename = "omnilight",argument = 190}, -- left nav light red
					{typename = "omnilight",argument = 191}, -- tail nav light white
					{typename = "omnilight",argument = 192}, -- right nav light green
				},
			},
			[4] = { typename = "collection", -- tail nav light
				lights = {
					{typename = "argumentlight",argument = 200}, -- FORMATION LIGHTS
					{typename = "argumentlight",argument = 201}, -- FORMATION LIGHTS
				},
			},			    
		}
    }, -- end lights_data

}

add_aircraft(Su_57)


