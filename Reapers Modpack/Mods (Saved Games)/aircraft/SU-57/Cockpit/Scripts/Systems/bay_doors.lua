-- Check with other files
--dofile(LockOn_Options.script_path.."command_defs.lua")
--dofile(LockOn_Options.script_path.."devices.lua")
-- dofile(LockOn_Options.script_path.."mainpanel_init.lua")

-- Declare self variables
local controls_system = GetSelf()
local dev = GetSelf()

-- 0.01 rep. 1000th of second, by 10 ms.
local update_time_step = 0.01

make_default_activity(update_time_step)

local sensor_data = get_base_data()

-- Defines what happens when the script loads in
function post_initialize()
  print_message_to_user("BAY DOORS SYSTEM ONLINE")
  
  
  
  
end

-- TODO FOR BAY DOORS:
--[[
  implement mainpanel_init.lua
  implement in which speed they  animate
  implement weapon selection linked to bay doors
  
]]--

-- Defines how bay doors animations  work
function calculate_bay_doors()
  -- Sets up constants/dependencies
  -- Speed
  local mach = sensor_data:getMachNumber()

  -- BAY DOORS
  local bay_doors = 0
  

  -- While we are at faster speed than Mach 0.4
  if (mach > 0.5) and (mach < 0.6) then
    bay_doors = 1
  -- Otherwise, Do not open Bay doors.
  else
    bay_doors = 0
  end

  --BAY  DOORS ANIMATION ARG#
  set_aircraft_draw_argument_value(26, bay_doors)
  
end

update = function()
  -- Runs the bay doors calculations
  calculate_bay_doors()
  
  
  
  
  
  
  
end

need_to_be_closed = false
