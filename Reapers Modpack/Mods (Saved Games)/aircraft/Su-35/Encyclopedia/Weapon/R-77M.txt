R-77M
Weapon/R-77M.png
r-77m


Name: R-77M (AA-12C 'Adder')
Type: Medium-range, radar-guided, air-to-air missile.
Developed: Spetztekhnika Vympel NPO, Russia.
Weight, kg: 190
Length, m: 3.60
Diameter, м: 0.200
Warhead: Laser fuse and an expanding rod warhead.
Warhead weight, kg: 22.5
Guidance: AESA seeker, inertial, command, conventional fins, two-pulse motor.
G limit: 40
Maximum Mach number: 4
Range, km: 80