Su-35
Plane/su-35.png
su-35


Name: Su-35 "Flanker-E"
Type: Multirole fighters
Developed: Sukhoi OKB, Russia
Crew: 1
Length: 21.9 m
Height: 5.9 m
Wing span: 14.7 m
Wing area: 62 m²
Maximum Mach at S/L: 1.1
Maximum Mach at height: 2.3
Weight empty: 19000 kg
Normal weight: 25300 kg
Maximum weight: 34500 kg
G limit: 9
Maximum range: 3600 km
Maximum fuel: 11500 kg
Service ceiling: 20000 m
Take-off speed: 280 km/h
Range with nominal load: 3000 km
Landing speed: 280-300 km/h

Armament:
- GSh-301 cannon