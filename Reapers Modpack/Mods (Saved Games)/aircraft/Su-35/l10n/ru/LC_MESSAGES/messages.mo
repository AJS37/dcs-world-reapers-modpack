��          �      ,      �     �  
   �     �  	   �     �     �     �     �       
             6     <     L  @  S  
   �  �   �  d   �  0     2   4     g     |     �     �     �     �     �  #   �     �     �     �    �          	                           
                                              ASC Disengage / Emergency Mode HUD Filter MFD HUD Repeater Mode Pitch SDU R-73L R-73L (AA-11 Archer-L) R-77M R-77M (AA-12C Adder) Roll SDU Rudder SDU Stick Limiter Override Su-35 Su-35 Flanker-E Su-35S The Sukhoi Su-35 (NATO reporting name: Flanker-E) is the designation for two separate, heavily-upgraded derivatives of the Su-27 air-defence fighter. They are single-seat, twin-engine, highly-maneuverable aircraft, designed by the Sukhoi Design Bureau and built by the Komsomolsk-on-Amur Aircraft Production Association. Thrust SDU Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: Morkva_55 <morkva55@gmail.com>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
X-Generator: Poedit 1.8.11
 СДУ, отключение каналов стабилизации / Аварийный режим ИЛС, светофильтр вкл./выкл. МФД, режим дублирования ИЛС СДУ, тангаж Р-73Л Р-73Л Р-77М Р-77М СДУ, крен СДУ, педали СДУ, отключение ОПР Cу-35 Су-35 Cу-35C Су-35 (код НАТО: Flanker-Е+) — российский многоцелевой сверхманевренный истребитель поколения 4++ без ПГО с двигателями управляемого вектора тяги (УВТ). Разработан в ОКБ Сухого. Серийный истребитель для ВКС России носит название Су-35С, производится на заводе КнААЗ в Комсомольске-на-Амуре. СДУ, тяга 