--27.11.2017г Су-35
--18.12.2017г
--24.11.2018г
dofile('Scripts/Database/Weapons/warheads.lua')

function copyTable(target, src)
    for i, v in pairs(src) do
        if type(v) == 'table' then
            if not target[i] then
                target[i] = { }
            end
            copyTable(target[i], v)
        else
            target[i] = v
        end
    end
end
  
-- TWP 2ой держатель для бомб
local twpl_rot = 0
local twpl_points = {
	[1] = {Position = {0.412001, -0.039800, -0.326014}, Rotation = { twpl_rot,0,0} },-- left
	[2] = {Position = {0.412001, -0.039800,  0.326014}, Rotation = {-twpl_rot,0,0} },-- right
}

--some predefinition helpers as adapter
local twpl_adapter =  {  ShapeName = WSTYPE_PLACEHOLDER, IsAdapter = true }

-- and weapon itself , attached to point at number i
local function twpl_element(shape,i)
 return  {
    Position = twpl_points[i].Position, -- position relative to adapter
    Rotation = twpl_points[i].Rotation, -- rotations in degrees , mostly used only X axis
    ShapeName = shape,
   }
end

--*************************************************************************************************

declare_loadout({
	category	= CAT_BOMBS,
	CLSID		= "{TWP_FAB_250}",
	attribute	= {wsType_Weapon, wsType_Bomb, wsType_Container, WSTYPE_PLACEHOLDER},
	wsTypeOfWeapon	=	{4,	5,	9,	6},
	Count		= 2,	--count of wepon
	Cx_pil		= 0.001, -- drag index of whole rack
	Picture		=	"FAB250.png",
	displayName	= "2x".._("FAB-250"),
	Weight		= 75 + 2 * 250,-- weight adapter + weapon
	Elements	=
	{
		twpl_adapter,
		twpl_element("FAB-250", 2, false),
		twpl_element("FAB-250", 1, true),
	}
})

declare_loadout({
	category	= CAT_BOMBS,
	CLSID		= "{TWPL_FAB_250}",
	attribute	= {wsType_Weapon, wsType_Bomb, wsType_Container, WSTYPE_PLACEHOLDER},
	wsTypeOfWeapon	=	{4,	5,	9,	6},
	Count		= 2,	--count of wepon
	Cx_pil		= 0.001, -- drag index of whole rack
	Picture		=	"FAB250.png",
	displayName	= "2x".._("FAB-250"),
	Weight		= 75 + 2 * 250,-- weight adapter + weapon
	Elements	=
	{
		twpl_adapter,
		twpl_element("FAB-250", 1, false),
		twpl_element("FAB-250", 2, true),
	}
})

--*************************************************************************************************

declare_loadout({
	category	= CAT_BOMBS,
	CLSID		= "{TWP_FAB_500}",
	attribute	= {wsType_Weapon, wsType_Bomb, wsType_Container, WSTYPE_PLACEHOLDER},
	wsTypeOfWeapon	=	{4,	5,	9,	7},
	Count		= 2,	--count of wepon
	Cx_pil		= 0.001, -- drag index of whole rack
	Picture		=	"FAB500.png",
	displayName	= "2x".._("FAB-500 M62"),
	Weight		= 75 + 2 * 506,-- weight adapter + weapon
	Elements	=
	{
		twpl_adapter,
		twpl_element("FAB-500", 2, false),
		twpl_element("FAB-500", 1, true),
	}
})

declare_loadout({
	category	= CAT_BOMBS,
	CLSID		= "{TWPL_FAB_500}",
	attribute	= {wsType_Weapon, wsType_Bomb, wsType_Container, WSTYPE_PLACEHOLDER},
	wsTypeOfWeapon	=	{4,	5,	9,	7},
	Count		= 2,	--count of wepon
	Cx_pil		= 0.001, -- drag index of whole rack
	Picture		=	"FAB500.png",
	displayName	= "2x".._("FAB-500 M62"),
	Weight		= 75 + 2 * 506,-- weight adapter + weapon
	Elements	=
	{
		twpl_adapter,
		twpl_element("FAB-500", 1, false),
		twpl_element("FAB-500", 2, true),
	}
})

--**********************************************NURS********************************************

B8M1_20_S8OFP2_DUAL_L = {
    category = CAT_ROCKETS,
    CLSID	= "{TWPL_B-8M1 - 20 S-8OFP2}",
    Picture	= "b8v20a.png",
    Cx_pil	= 0.00146484375 * 2,
    displayName	= "2x".._("B-8M1 - 20 S-8OFP2"),
    Count	= 2,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            ShapeName = "TWP_35",
            IsAdapter = true,
        },
        {
            payload_CLSID = "B-8M1 - 20 S-8OFP2",
            connector_name = "Pylon_out",
        },
        {
            payload_CLSID = "B-8M1 - 20 S-8OFP2",
            connector_name = "Pylon_in",
        },
    },
    Weight = (137.5 + 20 * 11.3) * 2,
    wsTypeOfWeapon = {4, 7, 33, 155},
    attribute      = {4, 7, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(B8M1_20_S8OFP2_DUAL_L)

B8M1_20_S8OFP2_DUAL_R = {}
copyTable(B8M1_20_S8OFP2_DUAL_R, B8M1_20_S8OFP2_DUAL_L)
B8M1_20_S8OFP2_DUAL_R.CLSID = "{TWP_B-8M1 - 20 S-8OFP2}"
B8M1_20_S8OFP2_DUAL_R.Elements[1].ShapeName = "TWP_35"
B8M1_20_S8OFP2_DUAL_R.Elements[2].connector_name = "Pylon_out"
B8M1_20_S8OFP2_DUAL_R.Elements[3].connector_name = "Pylon_in"
B8M1_20_S8OFP2_DUAL_R.attribute = B8M1_20_S8OFP2_DUAL_L.attribute
declare_loadout(B8M1_20_S8OFP2_DUAL_R)

--*************************************************************************************************

B8M1_20_S8KOM_DUAL_L = {
    category = CAT_ROCKETS,
    CLSID	= "{TWPL_B8M1_20_S8KOM}",
    Picture	= "b8v20a.png",
    Cx_pil	= 0.00146484375 * 2,
    displayName	= "2x".._("B-8M1 - 20 S-8KOM"),
    Count	= 2,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            ShapeName = "TWP_35",
            IsAdapter = true,
        },
        {
            payload_CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
            connector_name = "Pylon_out",
        },
        {
            payload_CLSID = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
            connector_name = "Pylon_in",
        },
    },
    Weight = (137.5 + 20 * 11.3) * 2,
    wsTypeOfWeapon = {4, 7, 33, 32},
    attribute      = {4, 7, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(B8M1_20_S8KOM_DUAL_L)

B8M1_20_S8KOM_DUAL_R = {}
copyTable(B8M1_20_S8KOM_DUAL_R, B8M1_20_S8KOM_DUAL_L)
B8M1_20_S8KOM_DUAL_R.CLSID = "{TWP_B8M1_20_S8KOM}"
B8M1_20_S8KOM_DUAL_R.Elements[1].ShapeName = "TWP_35"
B8M1_20_S8KOM_DUAL_R.Elements[2].connector_name = "Pylon_out"
B8M1_20_S8KOM_DUAL_R.Elements[3].connector_name = "Pylon_in"
B8M1_20_S8KOM_DUAL_R.attribute = B8M1_20_S8KOM_DUAL_L.attribute
declare_loadout(B8M1_20_S8KOM_DUAL_R)

--*************************************************************************************************

B13_5_S13OF_DUAL_L = {
    category = CAT_ROCKETS,
    CLSID	= "{TWPL_B13_5_S13OF}",
    Picture	= "B13.png",
    Cx_pil	= 0.00159912109375 * 2,
    displayName	= "2x".._("B-13L - 5 S-13 OF"),
    Count	= 10,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            ShapeName = "TWP_35",
            IsAdapter = true
        },
        {
            payload_CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
            DrawArgs = {{3,0.5}},
            connector_name = "Pylon_out"
        },
        {
            payload_CLSID = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
            DrawArgs = {{3,0.5}},
            connector_name = "Pylon_in"
        },
    },
    Weight = 510 * 2,
    wsTypeOfWeapon = {4, 7, 33, 33},
    attribute      = {4, 7, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(B13_5_S13OF_DUAL_L)

B13_5_S13OF_DUAL_R = {}
copyTable(B13_5_S13OF_DUAL_R, B13_5_S13OF_DUAL_L)
B13_5_S13OF_DUAL_R.CLSID = "{TWP_B13_5_S13OF}"
B13_5_S13OF_DUAL_R.Elements[1].ShapeName = "TWP_35"
B13_5_S13OF_DUAL_R.Elements[2].connector_name = "Pylon_out"
B13_5_S13OF_DUAL_R.Elements[3].connector_name = "Pylon_in"
B13_5_S13OF_DUAL_R.attribute = B13_5_S13OF_DUAL_L.attribute
declare_loadout(B13_5_S13OF_DUAL_R)

--*************************************************************************************************

S25_DUAL_L = {
    category = CAT_ROCKETS,
    CLSID	= "{TWPL_S25}",
    Picture	= "s25.png",
    Cx_pil	= 0.0004,
    displayName	= "2x".._("S-25 OFM"),
    Count	= 2,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            ShapeName = "TWP_35",
            IsAdapter = true
        },
        {
            payload_CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}",
            DrawArgs = {{3,0.5}},
            connector_name = "Pylon_out"
        },
        {
            payload_CLSID = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}",
            DrawArgs = {{3,0.5}},
            connector_name = "Pylon_in"
        },
    },
    Weight = 500,
    wsTypeOfWeapon = {4, 7, 33, 35},
    attribute      = {4, 7, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(S25_DUAL_L)

S25_DUAL_R = {}
copyTable(S25_DUAL_R, S25_DUAL_L)
S25_DUAL_R.CLSID = "{TWP_S25}"
S25_DUAL_R.Elements[1].ShapeName = "TWP_35"
S25_DUAL_R.Elements[2].connector_name = "Pylon_out"
S25_DUAL_R.Elements[3].connector_name = "Pylon_in"
S25_DUAL_R.attribute = S25_DUAL_L.attribute
declare_loadout(S25_DUAL_R)

--*********************************************RBK-500*******************************************

RBK_500_PTAB_1M_DUAL_L = {
    category = CAT_BOMBS,
    CLSID	= "{TWPL_RBK-500 PTAB-1M}",
    Picture	= "RBK_500_SPBE_D_cassette.png",
    Cx_pil	= 0.0004,
    displayName	= "2x".._("RBK-500 PTAB-1M"),
    Count	= 2,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            DrawArgs =	
            {
                [1] = {1, 1},
                [2] = {2, 1},
                [3] = {3, 1},
            }, -- end of DrawArgs
            ShapeName = "RBK_500_PTAB_1M_cassette",
            Position  = {0.645000*3/4,-0.56/2,0.324936},
        },
        {
            DrawArgs =	
            {
                [1] = {1, 1},
                [2] = {2, 1},
                [3] = {3, -1},
            }, -- end of DrawArgs
            ShapeName = "RBK_500_PTAB_1M_cassette",
            Position  = {0.645000*3/4,-0.56/2,-0.324936},
        },
    },
    Weight = 427*2,
    wsTypeOfWeapon = {4, 5, 38, 91},
    attribute      = {4, 5, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(RBK_500_PTAB_1M_DUAL_L)

RBK_500_PTAB_1M_DUAL_R = {}
copyTable(RBK_500_PTAB_1M_DUAL_R, RBK_500_PTAB_1M_DUAL_L)
RBK_500_PTAB_1M_DUAL_R.CLSID = "{TWP_RBK-500 PTAB-1M}"
RBK_500_PTAB_1M_DUAL_R.Elements[1].DrawArgs[3] = {3, -1}
RBK_500_PTAB_1M_DUAL_R.Elements[2].DrawArgs[3] = {3, 1}
RBK_500_PTAB_1M_DUAL_R.Elements[2].Position = {0.645000*3/4,-0.56/2,0.324936}
RBK_500_PTAB_1M_DUAL_R.Elements[1].Position = {0.645000*3/4,-0.56/2,-0.324936}
RBK_500_PTAB_1M_DUAL_R.attribute = RBK_500_PTAB_1M_DUAL_L.attribute
declare_loadout(RBK_500_PTAB_1M_DUAL_R)

--*************************************************************************************************

RBK_500_PTAB105_DUAL_L = {
    category = CAT_BOMBS,
    CLSID	= "{TWPL_RBK_500_PTAB105}",
    Picture	= "RBK_500_255_PTAB_10_5_cassette.png",
    Cx_pil	= 0.0004,
    displayName	= "2x".._("RBK-500-255 PTAB-10-5"),
    Count	= 2,
    JettisonSubmunitionOnly = true,
    
    Elements =
    {
        {
            DrawArgs =	
            {
                [1] = {1, 1},
                [2] = {2, 1},
                [3] = {3, 1},
            }, -- end of DrawArgs
            ShapeName = "RBK_500_255_PTAB_10_5_cassette",
            Position  = {0.645000*3/4,-0.56/2,0.324936},
        },
        {
            DrawArgs =	
            {
                [1] = {1, 1},
                [2] = {2, 1},
                [3] = {3, -1},
            }, -- end of DrawArgs
            ShapeName = "RBK_500_255_PTAB_10_5_cassette",
            Position  = {0.645000*3/4,-0.56/2,-0.324936},
        },
    },
    Weight = 427*2,
    wsTypeOfWeapon = {4, 5, 38, 20},
    attribute      = {4, 5, 32, WSTYPE_PLACEHOLDER},
}
declare_loadout(RBK_500_PTAB105_DUAL_L)

RBK_500_PTAB105_DUAL_R = {}
copyTable(RBK_500_PTAB105_DUAL_R, RBK_500_PTAB105_DUAL_L)
RBK_500_PTAB105_DUAL_R.CLSID = "{TWP_RBK_500_PTAB105}"
RBK_500_PTAB105_DUAL_R.Elements[1].DrawArgs[3] = {3, -1}
RBK_500_PTAB105_DUAL_R.Elements[2].DrawArgs[3] = {3, 1}
RBK_500_PTAB105_DUAL_R.Elements[2].Position = {0.645000*3/4,-0.56/2,0.324936}
RBK_500_PTAB105_DUAL_R.Elements[1].Position = {0.645000*3/4,-0.56/2,-0.324936}
RBK_500_PTAB105_DUAL_R.attribute = RBK_500_PTAB105_DUAL_L.attribute
declare_loadout(RBK_500_PTAB105_DUAL_R)

--*******************************************R-77M*************************************************

tail_liquid = {0.9, 0.9, 0.9, 0.05 };

local P_77M = { 

	category		= CAT_AIR_TO_AIR,
	name			= _("R-77M"),
	user_name		= _("R-77M (AA-12C Adder)"),
	wsTypeOfWeapon	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile, WSTYPE_PLACEHOLDER},
	NatoName		= "(AA-12C)",

	shape_table_data =
	{
		{
			name = "r-77m";
			file = "r-77m";
			life = 1;
			fire = { 0, 1};
			username = "R-77M";	--	имя ракеты
			index = WSTYPE_PLACEHOLDER,
		},
	},

		Escort = 0,				--	сопровождение: 0 - нет,
		Head_Type = 2,			--	тип головки самонаведения
		sigma = {5, 5, 5},		--	 sigma = {x, y, z}, максимальная ошибка прицеливания в метрах, в координатах цели. x - продольная ось цели, y - вертиальная ось цели, z - поперечная ось цели
		M = 190.0,				--	полная масса в кг/
		H_max = 25000.0,		--	максимальная высота полета
		H_min = -1,				--	минимальная высота полета
		Diam = 200.0,			--	диаметр корпуса в мм 
		Cx_pil = 2.5,			--	Cx как подвески
		D_max = 17000.0,		--	максимальная дальность пуска на малой высоте, для AI
		D_min = 300.0,			--	минимальная дальность пуска, для AI
		Head_Form = 1,			--	0 - полусферическая форма головы, 1 - оживальная (~коническая)
		Life_Time = 110.0,		--	время жизни (таймер самоликвидации), сек
		Nr_max = 40,			--	максимальная перегрузка при разворотах
		v_min = 140.0,			--	минимальная скорость
		v_mid = 700.0,			--	средняя скорость
		Mach_max = 4.0,			--	максимальное число Маха
		t_b = 0.0,				--	время включения двигателя
		t_acc = 6.0,			--	время работы ускорителя
		t_marsh = 5.0,			--	время работы в маршевом режиме
		Range_max = 80000.0,	--	максимальная дальность пуска на максимальной высоте
		H_min_t = 3.0,			--	минимальная высота цели над рельефом, м.
		Fi_start = 0.5,		--	угол сопровождения и визирования при пуске
		Fi_rak = 3.14152,		--	допустимый угол ракурса цели (rad)
		Fi_excort = 1.05,	--	угол сопровождения/визирования цели ракетой
		Fi_search = 1.05,	--	предельный угол свободного поиска
		OmViz_max = 0.52,	--	предельная скорость линии визирования
								--	повреждение, наносимое при прямом попадании
		warhead = enhanced_a2a_warhead(12.9),	--	боевая часть // WAU-17/B, стержневая
		exhaust = tail_liquid,	--	tail smoke color and opacity
		X_back = -1.170,		--	
		Y_back = -0.098,		--	
		Z_back = 0,				--	
		Reflection = 0.035,		--	эффективная поверхность радиоотражения, квадратные метры
		KillDistance = 15.0,	--	радиус поражения зарядом в метрах
		loft = 1,
		hoj = 1,				--	наведение на источник помех
		ccm_k0 = 0.6,			-- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
		rad_correction = 1,

		PN_coeffs = {3,					-- Number of Entries	
					5000.0 ,1.0,		-- Less 5 km to target Pn = 1
					10000.0, 0.5,		-- Between 10 and 5 km	to target, Pn smoothly changes from 0.5 to 1.0. 
					15000.0, 0.2};		-- Between 15 and 10 km	 to target, Pn smoothly changes from 0.2 to 0.5. Longer then 15 km Pn = 0.2.

		ModelData = {	58 ,  -- model params count
						0.6 ,	-- characteristic square (характеристическая площадь)

						-- параметры зависимости Сx
						0.0125 , -- планка Сx0 на дозвуке ( M << 1)
						0.052 , -- высота пика волнового кризиса
						0.010 , -- крутизна фронта на подходе к волновому кризису
						0.002 , -- планка Cx0 на сверхзвуке ( M >> 1)
						0.5	 , -- крутизна спада за волновым кризисом 
						1.2	 , -- коэффициент отвала поляры

						-- параметры зависимости Cy
						2.20, -- планка Cya на дозвуке ( M << 1)
						1.05, -- планка Cya на сверхзвуке ( M >> 1)
						1.20, -- крутизна спада(фронта) за волновым кризисом  
						
						0.29 , -- 7 Alfa_max  максимальный балансировачный угол, радианы
						0.0, --угловая скорость создаваймая моментом газовых рулей

					-- Engine data. Time, fuel flow, thrust.	
					--	t_start		t_b		t_accel		t_march		t_inertial		t_break		t_end		-- Stage
						-1.0,		-1.0,	6.0,		5.0,		0.0,			0.0,		1.0e9,		-- time of stage, sec
						 0.0,		0.0,	8.3,		3.7,		0.0,			0.0,		0.0,		-- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	22500.0,	12500.0,	0.0,			0.0,		0.0,		-- thrust, newtons

						 1.0e9, -- таймер самоликвидации, сек
						 110.0, -- время работы энергосистемы, сек
						 0, -- абсолютная высота самоликвидации, м
						 1.0, -- время задержки включения управления (маневр отлета, безопасности), сек
						 30000, -- дальность до цели в момент пуска, при превышении которой ракета выполняется маневр "горка", м
						 25000, -- дальность до цели, при которой маневр "горка" завершается и ракета переходит на чистую пропорциональную навигацию (должен быть меньше или равен предыдущему параметру), м 
						 0.17,	-- синус угла возвышения траектории набора горки
						 50.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,	 коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,	коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,	полоса пропускания контура управления
						 30000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 3.9, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 3.2,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 90.0, -- прогноз времени полета ракеты 
						  -- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
						 80000.0, -- дальность ракурс 180(навстречу) град,	Н=10000м, V=900км/ч, м
						 35000.0, -- дальность ракурс 0(в догон) град,	Н=10000м, V=900км/ч
						 30000.0, -- дальность ракурс 180(навстречу) град, Н=1000м, V=900км/ч
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 0.6, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						 0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
}

declare_weapon(P_77M)

declare_loadout(
	{		
		category		= CAT_AIR_TO_AIR,
		CLSID			= "{R_77M}",
		attribute		= {wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
		wsTypeOfWeapon	= P_77M.wsTypeOfWeapon,
		Count			= 1,
		Picture			= "r27r.png",
		displayName		= P_77M.user_name, -- weapon name in the loadout menu
		Weight			= 190+75, -- missile + launcher weight. So this weight should be higher than a missile itself!
		Cx_pil			= 0.001,
		Elements		={
			[1] =
			{
				DrawArgs	=	
				{
					[1]	=	{1,	1},
					[2]	=	{2,	1},
				}, -- end of DrawArgs
				Position	=	{0,	0,	0},
				ShapeName	=	"r-77m",
			},
		},
	}
)

-------------------------R-73L------------------------

local P_73L = { 

	category		= CAT_AIR_TO_AIR,
	name			= _("R-73L"),
	user_name		= _("R-73L (AA-11 Archer-L)"),
	wsTypeOfWeapon	= {wsType_Weapon,wsType_Missile,wsType_AA_Missile, WSTYPE_PLACEHOLDER},
	NatoName		= "(AA-11L)",

	shape_table_data =
	{
		{
			name = "r-73l";
			file = "r-73l";
			life = 1;
			fire = { 0, 1};
			username = "R-73L";	--	имя ракеты
			index = WSTYPE_PLACEHOLDER,
		},
	},

		Escort = 0,				--	сопровождение: 0 - нет,
		Head_Type = 1,			--	тип головки самонаведения
		sigma = {3, 3, 3},		--	 sigma = {x, y, z}, максимальная ошибка прицеливания в метрах, в координатах цели. x - продольная ось цели, y - вертиальная ось цели, z - поперечная ось цели
		M = 105.0,				--	полная масса в кг/
		H_max = 20000.0,		--	максимальная высота полета
		H_min = -1,				--	минимальная высота полета
		Diam = 170.0,			--	диаметр корпуса в мм 
		Cx_pil = 1.9,			--	Cx как подвески
		D_max = 4000.0,		--	максимальная дальность пуска на малой высоте, для AI
		D_min = 200.0,			--	минимальная дальность пуска, для AI
		Head_Form = 0,			--	0 - полусферическая форма головы, 1 - оживальная (~коническая)
		Life_Time = 30.0,		--	время жизни (таймер самоликвидации), сек
		Nr_max = 45,			--	максимальная перегрузка при разворотах
		v_min = 140.0,			--	минимальная скорость
		v_mid = 400.0,			--	средняя скорость
		Mach_max = 2.8,			--	максимальное число Маха
		t_b = 0.0,				--	время включения двигателя
		t_acc = 2.0,			--	время работы ускорителя
		t_marsh = 3.0,			--	время работы в маршевом режиме
		Range_max = 12000.0,	--	максимальная дальность пуска на максимальной высоте
		H_min_t = 1.0,			--	минимальная высота цели над рельефом, м.
		Fi_start = 2,0944,		--	угол сопровождения и визирования при пуске
		Fi_rak = 3.14152,		--	допустимый угол ракурса цели (rad)
		Fi_excort = 2,61799,	--	угол сопровождения/визирования цели ракетой
		Fi_search = 0.15708,	--	предельный угол свободного поиска
		OmViz_max = 1.05,	--	предельная скорость линии визирования
								--	повреждение, наносимое при прямом попадании
		warhead = enhanced_a2a_warhead(8.0),	--	боевая часть // стержневая, ПИМ расположен внутри БЧ
		exhaust = tail_liquid,	--	tail smoke color and opacity
		X_back = -1.1,		--
		Y_back = -0.09,		--
		Z_back = 0,				--
		Reflection = 0.0262,		--	эффективная поверхность радиоотражения, квадратные метры
		KillDistance = 5.0,	--	радиус поражения зарядом в метрах
		loft = 0,
		hoj = 0,				--	наведение на источник помех
		SeekerSensivityDistance = 20000, -- The range of target with IR value = 1. In meters.
		ccm_k0 = 0.3,  -- Counter Countermeasures Probability Factor. Value = 0 - missile has absolutely resistance to countermeasures. Default = 1 (medium probability)
		SeekerCooled = true, -- True is cooled seeker and false is not cooled seeker.
		rad_correction = 0,

		ModelData = {   58 ,  -- model params count
						0.4 ,   -- characteristic square (характеристическая площадь)
						
						-- параметры зависимости Сx
						0.05 , -- Cx_k0 планка Сx0 на дозвуке ( M << 1)
						0.12 , -- Cx_k1 высота пика волнового кризиса
						0.02 , -- Cx_k2 крутизна фронта на подходе к волновому кризису
						0.062, -- Cx_k3 планка Cx0 на сверхзвуке ( M >> 1)
						1.2 , -- Cx_k4 крутизна спада за волновым кризисом 
						1.0 , -- коэффициент отвала поляры (пропорционально sqrt (M^2-1))
						
						-- параметры зависимости Cy
						0.9 , -- Cy_k0 планка Сy0 на дозвуке ( M << 1)
						0.8	 , -- Cy_k1 планка Cy0 на сверхзвуке ( M >> 1)
						1.2  , -- Cy_k2 крутизна спада(фронта) за волновым кризисом  
						
						0.78 , -- 7 Alfa_max  максимальный балансировачный угол, радианы
						5.0, -- Extra G by trust vector
						
					-- Engine data. Time, fuel flow, thrust.	
					--	t_statr		t_b		t_accel		t_march		t_inertial		t_break		t_end			-- Stage
						-1.0,		-1.0,	5.5,  		0.0,		0.0,			0.0,		1.0e9,         -- time of stage, sec
						 0.0,		0.0,	6.18,		0.0,		0.0,			0.0,		0.0,           -- fuel flow rate in second, kg/sec(секундный расход массы топлива кг/сек)
						 0.0,		0.0,	13847.0,	0.0,	0.0,			0.0,		0.0,           -- thrust, newtons
					
						 1.0e9, -- таймер самоликвидации, сек
						 23.0, -- время работы энергосистемы, сек
						 0, -- абсолютная высота самоликвидации, м
						 0.35, -- время задержки включения управления (маневр отлета, безопасности), сек
						 1.0e9, -- дальность до цели в момент пуска, при превышении которой ракета выполняется маневр "горка", м
						 1.0e9, -- дальность до цели, при которой маневр "горка" завершается и ракета переходит на чистую пропорциональную навигацию (должен быть больше или равен предыдущему параметру), м 
						 0.0,  -- синус угла возвышения траектории набора горки
						 30.0, -- продольное ускорения взведения взрывателя
						 0.0, -- модуль скорости сообщаймый катапультным устройством, вышибным зарядом и тд
						 1.19, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K0
						 1.0, -- характристика системы САУ-РАКЕТА,  коэф фильтра второго порядка K1
						 2.0, -- характристика системы САУ-РАКЕТА,  полоса пропускания контура управления
						 8000.0, -- дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 на высоте H=2000
						 1.077, -- крутизна зависимости  дальность полета в горизонт с располагаемой перегрузкой Navail >= 1.0 от высоты H
						 0.9,
						 0.75, -- безразмерный коэф. эффективности САУ ракеты
						 28.0, -- расчет времени полета
						  -- DLZ. Данные для рассчета дальностей пуска (индикация на прицеле)
						 19000.0, -- дальность ракурс   180(навстречу) град,  Н=10000м, V=900км/ч, м
						 6000.0, -- дальность ракурс 0(в догон) град,  Н=10000м, V=900км/ч, м
						 10000.0, -- дальность ракурс 	180(навстречу) град, Н=1000м, V=900км/ч, м
						 0.2, -- Уменьшение разрешенной дальности пуска при отклонении вектора скорости носителя от линии визирования цели.
						 1.0, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в нижнюю полусферу. Уменьшение дальности при стрельбе вниз.
						 1.4, -- Вертикальная плоскость. Наклон кривой разрешенной дальности пуска в верхнюю полусферу. Увеличение дальности при стрельбе вверх.
						-3.0, -- Вертикальная плоскость. Угол перегиба кривой разрешенной дальности, верхняя - нижняя полусфера.
						0.5, -- Изменение коэффициентов наклона кривой в верхнюю и нижнюю полусферы от высоты носителя.
					},
}

declare_weapon(P_73L)

declare_loadout(
	{		
		category		= CAT_AIR_TO_AIR,
		CLSID			= "{R_73L}",
		attribute		= {wsType_Weapon, wsType_Missile, wsType_Container, WSTYPE_PLACEHOLDER},
		wsTypeOfWeapon	= P_73L.wsTypeOfWeapon,
		Count			= 1,
		Picture			= "r73.png",
		displayName		= P_73L.user_name, -- weapon name in the loadout menu
		Weight			= 105, -- missile + launcher weight. So this weight should be higher than a missile itself!
		Cx_pil			= 0.001,
		Elements		={
			[1] =
			{
				DrawArgs	=	
				{
					[1]	=	{1,	1},
					[2]	=	{2,	1},
				}, -- end of DrawArgs
				Position	=	{0,	0,	0},
				ShapeName	=	"r-73l",
			},
		},
	}
)