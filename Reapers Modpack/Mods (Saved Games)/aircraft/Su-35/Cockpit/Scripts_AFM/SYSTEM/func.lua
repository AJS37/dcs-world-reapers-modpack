--------------------- DEBUG MESSAGE OUTPUT ---------------------
dbg_msg = false
--------------------- LIMIT TO 1 FUNCTION ----------------------
function limit_to_1(param)
	if (param > 1) then param = 1 end
	if (param < -1) then param = -1 end
	return param
end
------------------------- ROUND TO IDP -------------------------
-- rounds the number 'num' to the number of decimal places in 'idp'
-- print(round(107.75, -1))     : 110.0
-- print(round(107.75, 0))      : 108.0
-- print(round(107.75, 1))      : 107.8
function round(num, idp)
	local mult = 10^(idp or 0)
	return math.floor(num * mult + 0.5) / mult
end
