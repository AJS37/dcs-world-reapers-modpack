local unitPayloads = {
	["name"] = "Su-35",
	["payloads"] = {
		[1] = {
			["name"] = "UB-13*4,FAB-250*4,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[2] = {
			["name"] = "FAB-100*28,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FB3CE165-BF07-4979-887C-92B87F13276B}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{FB3CE165-BF07-4979-887C-92B87F13276B}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{FB3CE165-BF07-4979-887C-92B87F13276B}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{FB3CE165-BF07-4979-887C-92B87F13276B}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[3] = {
			["name"] = "KAB-500*4,R-73*2,R-27ET*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 6,
				},
				[6] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 7,
				},
				[7] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 10,
				},
				[9] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[10] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 33,
			},
		},
		[4] = {
			["name"] = "KAB-500*6,R-73*2,R-27ET*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 4,
				},
				[6] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 9,
				},
				[7] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 10,
				},
				[8] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[9] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
				[10] = {
					["CLSID"] = "{BA565F89-2373-4A84-9502-A0E017D3A44A}",
					["num"] = 5,
				},
				[11] = {
					["CLSID"] = "{BA565F89-2373-4A84-9502-A0E017D3A44A}",
					["num"] = 8,
				},
				[12] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 7,
				},
				[13] = {
					["CLSID"] = "{E2C426E3-8B10-4E09-B733-9CDC26520F48}",
					["num"] = 6,
				},
			},
			["tasks"] = {
				[1] = 33,
			},
		},
		[5] = {
			["name"] = "Kh-29T*4,R-73*2,R-27T*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{88DAC840-9F75-4531-8689-B46E64E42E53}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 5,
				},
				[7] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 8,
				},
				[8] = {
					["CLSID"] = "{B4FC81C9-B861-4E87-BBDC-A1158E648EBF}",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{88DAC840-9F75-4531-8689-B46E64E42E53}",
					["num"] = 10,
				},
				[10] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[11] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 33,
			},
		},
		[6] = {
			["name"] = "B-8*6,R-73*2,R-77*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[7] = {
			["name"] = "Kh-25MPU*6,R-73*2,R-77,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{0519A264-0AB6-11d6-9193-00A0249B6F00}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[8] = {
			["name"] = "FAB-250*8,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[9] = {
			["name"] = "FAB-500*8,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[10] = {
			["name"] = "Kh-25ML*6,R-73*2,R-77*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{6DADF342-D4BA-4D8A-B081-BA928C4AF86D}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[11] = {
			["name"] = "ФАБ-250*10,Р-73*2,Р-77М*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{R_77M}",
					["num"] = 11,
				},
				[4] = {
					["CLSID"] = "{R_77M}",
					["num"] = 2,
				},
				[5] = {
					["CLSID"] = "{FAB_250_DUAL_L}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{FAB_250_DUAL_R}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{3C612111-C7AD-476E-8A8E-2485812F4E5C}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[12] = {
			["name"] = "РБК-250*10,Р-73*2,Р-77М*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{R_77M}",
					["num"] = 11,
				},
				[4] = {
					["CLSID"] = "{R_77M}",
					["num"] = 2,
				},
				[5] = {
					["CLSID"] = "{RBK_250_PTAB25M_DUAL_L}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{RBK_250_PTAB25M_DUAL_R}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[13] = {
			["name"] = "FAB-1500*3,R-73*2,R-27ET*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}",
					["num"] = 6,
				},
				[6] = {
					["CLSID"] = "{40AA4ABE-D6EB-4CD6-AEFE-A1A0477B24AB}",
					["num"] = 9,
				},
				[7] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 10,
				},
				[8] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[9] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[14] = {
			["name"] = "ФАБ-500*10,Р-73*2,Р-77М*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{R_77M}",
					["num"] = 11,
				},
				[4] = {
					["CLSID"] = "{R_77M}",
					["num"] = 2,
				},
				[5] = {
					["CLSID"] = "{FAB_500_DUAL_L}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{FAB_500_DUAL_R}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{37DCC01E-9E02-432F-B61D-10C166CA2798}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[15] = {
			["name"] = "ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 16,
			},
		},
		[16] = {
			["name"] = "Kh-31P*2,Kh-25MPU*4,R-73*2,R-77,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{0519A264-0AB6-11d6-9193-00A0249B6F00}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{E86C5AA5-6D49-4F00-AD2E-79A62D6DDE26}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 29,
			},
		},
		[17] = {
			["name"] = "Kh-29L*4,R-73*2,R-27ET*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}",
					["num"] = 5,
				},
				[7] = {
					["CLSID"] = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}",
					["num"] = 8,
				},
				[8] = {
					["CLSID"] = "{3468C652-E830-4E73-AFA9-B5F260AB7C3D}",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 10,
				},
				[10] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[11] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 33,
			},
		},
		[18] = {
			["name"] = "РБК-500 ПТАБ-1М*10,Р-73*2,Р-77М*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{R_77M}",
					["num"] = 11,
				},
				[4] = {
					["CLSID"] = "{R_77M}",
					["num"] = 2,
				},
				[5] = {
					["CLSID"] = "{RBK_500_PTAB1M_DUAL_L}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{RBK_500_PTAB1M_DUAL_R}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[19] = {
			["name"] = "Р-73*2,Р-27ЭТ*2,Р-27ЭР*2,Р-77М*6,РЭБ",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[4] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{R_77M}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{B79C379A-9E87-4E50-A1EE-7F7E29C2E87A}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{R_77M}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{R_77M}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{E8069896-8435-4B90-95C0-01A03AE6E400}",
					["num"] = 7,
				},
				[12] = {
					["CLSID"] = "{E8069896-8435-4B90-95C0-01A03AE6E400}",
					["num"] = 6,
				},
			},
			["tasks"] = {
				[1] = 19,
				[2] = 18,
				[3] = 11,
				[4] = 10,
			},
		},
		[20] = {
			["name"] = "УБ-13*6,РБК-500 ПТАБ-1М*4,Р-73*2,Р-77*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[4] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{TWIN_B13L_5OF}",
					["num"] = 10,
				},
				[6] = {
					["CLSID"] = "{TWIN_B13L_5OF}",
					["num"] = 3,
				},
				[7] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 4,
				},
				[8] = {
					["CLSID"] = "{FC56DF80-9B09-44C5-8976-DCFAFF219062}",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 8,
				},
				[10] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 5,
				},
				[11] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{7AEC222D-C523-425e-B714-719C0D1EB14D}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[21] = {
			["name"] = "Kh-31A*4,Kh-31P*2,R-73*2,R-77,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{4D13E282-DF46-4B23-864A-A9423DFDE504}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{4D13E282-DF46-4B23-864A-A9423DFDE504}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{0519A264-0AB6-11d6-9193-00A0249B6F00}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{D8F2C90B-887B-4B9E-9FE2-996BC9E9AF03}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{4D13E282-DF46-4B23-864A-A9423DFDE504}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{4D13E282-DF46-4B23-864A-A9423DFDE504}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 30,
			},
		},
		[22] = {
			["name"] = "Б-8 ВМ1 20 С-8 ОФП2*6,РБК-500 ПТАБ-1М*4,Р-73*2,Р-77*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[4] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{TWIN_B_8M1_S_8_OFP2}",
					["num"] = 10,
				},
				[6] = {
					["CLSID"] = "{TWIN_B_8M1_S_8_OFP2}",
					["num"] = 3,
				},
				[7] = {
					["CLSID"] = "B-8M1 - 20 S-8OFP2",
					["num"] = 4,
				},
				[8] = {
					["CLSID"] = "B-8M1 - 20 S-8OFP2",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 8,
				},
				[10] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 5,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[23] = {
			["name"] = "Б-8 ВМ1 20 С-8 КОМ*6,РБК-500 ПТАБ-1М*4,Р-73*2,Р-77*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[4] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{TWIN_B_8M1_S_8KOM}",
					["num"] = 10,
				},
				[6] = {
					["CLSID"] = "{TWIN_B_8M1_S_8KOM}",
					["num"] = 3,
				},
				[7] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 4,
				},
				[8] = {
					["CLSID"] = "{F72F47E5-C83A-4B85-96ED-D3E46671EE9A}",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 8,
				},
				[10] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 5,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[24] = {
			["name"] = "RBK-500 PTAB-10-5*8,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[25] = {
			["name"] = "BetAB-500*8,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{35B698AC-9FEF-4EC4-AD29-484A0085F62B}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 34,
			},
		},
		[26] = {
			["name"] = "С-25 ОФМ*6,РБК-500 ПТАБ-1М*4,Р-73*2,Р-77*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[4] = {
					["CLSID"] = "{B4C01D60-A8A3-4237-BD72-CA7655BC0FE9}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{TWIN_S25}",
					["num"] = 10,
				},
				[6] = {
					["CLSID"] = "{TWIN_S25}",
					["num"] = 3,
				},
				[7] = {
					["CLSID"] = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}",
					["num"] = 4,
				},
				[8] = {
					["CLSID"] = "{A0648264-4BC0-4EE8-A543-D119F6BA4257}",
					["num"] = 9,
				},
				[9] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 8,
				},
				[10] = {
					["CLSID"] = "{F99BEC1A-869D-4AC7-9730-FBA0E3B1F5FC}",
					["num"] = 5,
				},
			},
			["tasks"] = {
				[1] = 32,
			},
		},
		[27] = {
			["name"] = "РБК-500 ПТАБ-10-5*10,Р-73*2,Р-77М*2",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{R_77M}",
					["num"] = 11,
				},
				[4] = {
					["CLSID"] = "{R_77M}",
					["num"] = 2,
				},
				[5] = {
					["CLSID"] = "{RBK_500_PTAB105_DUAL_L}",
					["num"] = 3,
				},
				[6] = {
					["CLSID"] = "{RBK_500_PTAB105_DUAL_R}",
					["num"] = 10,
				},
				[7] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 6,
				},
				[12] = {
					["CLSID"] = "{D5435F26-F120-4FA3-9867-34ACE562EF1B}",
					["num"] = 7,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
		[28] = {
			["name"] = "Б-8 ВМ1 20 С-8ЦМ*6,Р-73*2,Р-77М*4,РЭБ",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
				[3] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[4] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[5] = {
					["CLSID"] = "{TWIN_B_8M1_S_8TsM}",
					["num"] = 10,
				},
				[6] = {
					["CLSID"] = "{TWIN_B_8M1_S_8TsM}",
					["num"] = 3,
				},
				[7] = {
					["CLSID"] = "{3DFB7320-AB0E-11d7-9897-000476191836}",
					["num"] = 9,
				},
				[8] = {
					["CLSID"] = "{3DFB7320-AB0E-11d7-9897-000476191836}",
					["num"] = 4,
				},
				[9] = {
					["CLSID"] = "{R_77M}",
					["num"] = 5,
				},
				[10] = {
					["CLSID"] = "{R_77M}",
					["num"] = 8,
				},
				[11] = {
					["CLSID"] = "{R_77M}",
					["num"] = 7,
				},
				[12] = {
					["CLSID"] = "{R_77M}",
					["num"] = 6,
				},
			},
			["tasks"] = {
				[1] = 16,
			},
		},
		[29] = {
			["name"] = "RBK-250 PTAB-2.5M*8,R-73*2,ECM",
			["pylons"] = {
				[1] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82F}",
					["num"] = 1,
				},
				[2] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 2,
				},
				[3] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 3,
				},
				[4] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 4,
				},
				[5] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 5,
				},
				[6] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 6,
				},
				[7] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 7,
				},
				[8] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 8,
				},
				[9] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 9,
				},
				[10] = {
					["CLSID"] = "{4203753F-8198-4E85-9924-6F8FF679F9FF}",
					["num"] = 10,
				},
				[11] = {
					["CLSID"] = "{FBC29BFE-3D24-4C64-B81D-941239D12249}",
					["num"] = 11,
				},
				[12] = {
					["CLSID"] = "{44EE8698-89F9-48EE-AF36-5FD31896A82A}",
					["num"] = 12,
				},
			},
			["tasks"] = {
				[1] = 31,
			},
		},
	},
	["tasks"] = {
	},
	["unitType"] = "Su-35",
}
return unitPayloads
