local self_ID = "Su-35"

declare_plugin(self_ID,
{
displayName		= _("Su-35"),
developerName	= "ISVA",

image		 = "FC.bmp",
installed	 = true, -- if false that will be place holder , or advertising
dirName		 = current_mod_path,
fileMenuName = _("Su-35"),

version		 = __DCS_VERSION__,
state		 = "installed",
info		 = _("The Sukhoi Su-35 (NATO reporting name: Flanker-E) is the designation for two separate, heavily-upgraded derivatives of the Su-27 air-defence fighter. They are single-seat, twin-engine, highly-maneuverable aircraft, designed by the Sukhoi Design Bureau and built by the Komsomolsk-on-Amur Aircraft Production Association."),

Skins	=
	{
		{
			name	= _("Su-35"),
			dir		= "Theme"
		},
	},
Missions =
	{
		{
			name	= _("Su-35"),
			dir		= "Missions",
		},
	},
LogBook =
	{
		{
			name	= _("Su-35"),
			type	= "Su-35",
		},
		{
			name	= _("Su-35S"),
			type	= "Su-35S",
		},
	},
	
InputProfiles =
	{
		["su-35"]	= current_mod_path .. '/Input/su-35',
		["su-35s"]	= current_mod_path .. '/Input/su-35s',
	},

binaries	 =
{
	'Su27'
},

encyclopedia_path = current_mod_path..'/Encyclopedia'
})
---------------------------------------------------------------------------------------------------------
mount_vfs_texture_path	(current_mod_path.."/Theme/ME")
mount_vfs_model_path	(current_mod_path.."/Cockpit/Shape")
--mount_vfs_texture_path	(current_mod_path.."/Cockpit/Textures/SU-35-CPT-TEXTURES") -- wip entries
mount_vfs_model_path	(current_mod_path.."/Shapes")
mount_vfs_liveries_path (current_mod_path.."/Liveries")
mount_vfs_texture_path	(current_mod_path.."/Textures/Su-35.zip")
mount_vfs_texture_path	(current_mod_path.."/Textures/Weapons.zip")
---------------------------------------------------------------------------------------------------------
make_flyable('Su-35S',current_mod_path..'/Cockpit/Scripts_AFM/', {self_ID,'Su27',old = 3}, current_mod_path..'/Entry/comm.lua') -- PFM Su-27
make_flyable('Su-35',current_mod_path..'/Cockpit/Scripts_AFM/', {self_ID,'Su27',old = 54}, current_mod_path..'/Entry/comm.lua') -- PFM Su-27
--make_flyable('Su-35S',current_mod_path..'/Cockpit/Scripts_SFM/', {nil, old = 3}, current_mod_path..'/Entry/comm.lua') -- SFM obsolete entries
--make_flyable('Su-35',current_mod_path..'/Cockpit/Scripts_SFM/', {nil, old = 54}, current_mod_path..'/Entry/comm.lua') -- SFM obsolete entries
---------------------------------------------------------------------------------------------------------
dofile(current_mod_path.."/Entry/views.lua")
make_view_settings('Su-35', ViewSettings, SnapViews)
make_view_settings('Su-35S', ViewSettings, SnapViews)
dofile(current_mod_path..'/Entry/su35.lua')
dofile(current_mod_path..'/Entry/su35s.lua')
dofile(current_mod_path..'/Entry/weapons.lua')
dofile(current_mod_path..'/Entry/sensors.lua')
---------------------------------------------------------------------------------------------------------
plugin_done()