
C2A_Greyhound =  {
      
		Name 			= 'C2A_Greyhound',--AG
		DisplayName		= _('C-2A Greyhound'),--AG
        Picture 		= "C2A_Greyhound.png",
        Rate 			= "100",
        Shape			= "C2A_Greyhound",--AG	
		WorldID			=  WSTYPE_PLACEHOLDER, 
		--WorldID 		= 31,
		singleInFlight 	= true,
        
	shape_table_data 	= 
	{
		{
			file  	 	= 'C2A_Greyhound';--AG
			life  	 	= 20; -- lifebar
			vis   	 	= 3; -- visibility gain.
			desrt    	= 'C2A_Greyhound-oblomok'; -- Name of destroyed object file name
			fire  	 	= { 300, 2}; 			-- Fire on the ground after destoyed: 300sec 2m
			username	= 'C2A_Greyhound';--AG
			index       =  WSTYPE_PLACEHOLDER;
			--index       =  C2A_Greyhound;
			classname   = "lLandPlane";
			positioning = "BYNORMAL";
		},
		{
			name  		= "C2A_Greyhound-oblomok";
			file  		= "C2A_Greyhound-oblomok";
			fire  		= { 240, 2};
		},
	},
	
	
	
	LandRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of LandRWCategories
        TakeOffRWCategories = 
        {
        [1] = 
        {
			Name = "AircraftCarrier",
        },
        [2] = 
        {
            Name = "AircraftCarrier With Catapult",
        }, 
        [3] = 
        {
            Name = "AircraftCarrier With Tramplin",
        }, 
    }, -- end of TakeOffRWCategories
	

	    mapclasskey 		= "P0091000029",
	    attribute  			= {wsType_Air, wsType_Airplane, wsType_Cruiser, WSTYPE_PLACEHOLDER ,
        "Transports",},
		
	    Categories = {
        },
		
		M_empty	=	17090,
		M_nominal	=	20500,
		M_max	=	24687,
		M_fuel_max	=	5624,
		H_max	=	11275,
		average_fuel_consumption	=	0.3,
		CAS_min	=	43,
		V_opt	=	133.3,
		V_take_off	=	53,
		V_land	=	53,
		has_afteburner	=	false,
		has_speedbrake	=	false,
		has_differential_stabilizer = false,
		radar_can_see_ground	=	true,
		
		nose_gear_pos 								= 	{6.902,	-1.918,	0},
	    nose_gear_amortizer_direct_stroke   		=  0,      -- down from nose_gear_pos !!!
	    nose_gear_amortizer_reversal_stroke  		= -0,      -- up 
	    nose_gear_amortizer_normal_weight_stroke 	= -0,      -- down from nose_gear_pos
	    nose_gear_wheel_diameter					=	0.528,
	
	    main_gear_pos 								= 	{0,	-2.047,	3.141},
	    main_gear_amortizer_direct_stroke	 	    =   0,     --  down from main_gear_pos !!!
	    main_gear_amortizer_reversal_stroke  	    =  -0,     --  up 
	    main_gear_amortizer_normal_weight_stroke    =  -0,     --  down from main_gear_pos
	    main_gear_wheel_diameter					=	0.972,

		AOA_take_off	=	0.14,
		stores_number	=	0,
		bank_angle_max	=	45,
		Ny_min	=	0,
		Ny_max	=	2.5,
		tand_gear_max	=	3.73,
		V_max_sea_level	=	178.2,
		V_max_h	=	173.8,
		wing_area	=	65.03,
		wing_span	=	24.60,
		wing_type = FOLDED_WING,
		thrust_sum_max	=	22000,
		thrust_sum_ab	=	22000,
		Vy_max	=	12,
		length	=	17.30,
		height	=	4.85,
		flaps_maneuver	=	1,
		Mach_max	=	0.53,
		range	=	2854,
		RCS	=	50,
		Ny_max_e	=	2,
		detection_range_max	=	400,
		IR_emission_coeff	=	0.5,
		IR_emission_coeff_ab	=	0,
		crew_size				=	4,
		engines_count	=	2,
		wing_tip_pos = 	{-0.614,	1.341,	12.279},
		
		--EPLRS 						= true,--?
		TACAN_AA					= true,--?
		launch_bar_connected_arg_value	= 0.745,
		
		mechanimations = {
            Door0 = {
                {Transition = {"Close", "Open"}, Sequence = {{C = {{"Sleep", "for", 0.0}}}}},
                {Transition = {"Open", "Close"}, Sequence = {{C = {{"Sleep", "for", 0.0}}}}},
                {Transition = {"Open", "Board"}, Sequence = {{C = {{"Sleep", "for", 50.0}}}, {C = {{"Arg", 38, "to", 1.0, "in", 3.0}}}}},
                {Transition = {"Board", "Open"}, Sequence = {{C = {{"Arg", 38, "to", 0.0, "in", 6.0}}}}},
            },
			Door1 = {DuplicateOf = "Door0"},
            Door2 = {DuplicateOf = "Door0"},
            Door3 = {DuplicateOf = "Door0"},
            FoldableWings = {
               {Transition = {"Retract", "Extend"}, Sequence = {{C = {{"Arg", 8, "to", 0.0, "in", 10.0},{"Arg", 555, "to", 0.0, "in", 6.0},}}}, Flags = {"Reversible"}},
               {Transition = {"Extend", "Retract"}, Sequence = {{C = {{"Arg", 8, "to", 1.0, "in", 15.0},{"Arg", 555, "to", 1.0, "in", 6.0}}}}, Flags = {"Reversible", "StepsBackwards"}},
            },
            LaunchBar = {
                {Transition = {"Retract", "Extend"}, Sequence = {{C = {{"ChangeDriveTo", "HydraulicGravityAssisted"}, {"VelType", 2}, {"Arg", 85, "to", 1.0, "in", 4.5}}}}},
                {Transition = {"Extend", "Retract"}, Sequence = {{C = {{"ChangeDriveTo", "Hydraulic"}, {"VelType", 2}, {"Arg", 85, "to", 0.0, "in", 4.5}}}}},
                {Transition = {"Retract", "Stage"},  Sequence = {{C = {{"ChangeDriveTo", "HydraulicGravityAssisted"}, {"VelType", 2}, {"Arg", 85, "to", 0.745, "in", 4.0}}}}},
                {Transition = {"Stage", "Retract"},  Sequence = {{C = {{"ChangeDriveTo", "Hydraulic"}, {"VelType", 2}, {"Arg", 85, "to", 0.0, "in", 4.0}}}}},
                {Transition = {"Extend", "Stage"},   Sequence = {{C = {{"ChangeDriveTo", "Mechanical"}, {"Arg", 85, "from", 1.0, "to", 0.745, "in", 1.0}}}}},
                {Transition = {"Stage", "Extend"},   Sequence = {{C = {{"ChangeDriveTo", "Mechanical"}, {"Arg", 85, "from", 0.745, "to", 1.0, "in", 0.2}}}}},
            },
        }, -- end of mechanimations
		
		engines_nozzles = 
		{
			[1] = 
			{
				pos = 	{-2.004,	0.438,	-3.293},
				elevation	=	0,
				diameter	=	0.756,
				exhaust_length_ab	=	8.629,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.1, 
			}, -- end of [1]
			[2] = 
			{
				pos = 	{-2.004,	0.438,	3.293},
				elevation	=	0,
				diameter	=	0.756,
				exhaust_length_ab	=	8.629,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.1, 
			}, -- end of [2]
		}, -- end of engines_nozzles
		crew_members = 
		{
			[1] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{7.916,	0.986,	0},
				ejection_order = 4,
			}, -- end of [1]
			[2] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
				ejection_order = 3,
			}, -- end of [2]
			[3] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{7.916,	0.986,	0},
				ejection_order = 2,
			}, -- end of [3]
			[4] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
				ejection_order = 1,
			}, -- end of [4]
		}, -- end of crew_members
		
		brakeshute_name	=	0,
		is_tanker	=	false,
		air_refuel_receptacle_pos = 	{17.3,	1.3,	0},
		fires_pos = 
		{
			[1] = 	{0.048,	1.008,	0},
			[2] = 	{0.048,	1.008,	2.322},
			[3] = 	{0.048,	1.008,	-2.322},
			[4] = 	{-0.82,	0.265,	2.774},
			[5] = 	{-0.82,	0.265,	-2.774},
			[6] = 	{-0.82,	0.255,	4.274},
			[7] = 	{-0.82,	0.255,	-4.274},
			[8] = 	{-0.267,	0.054,	3.293},
			[9] = 	{-0.267,	0.054,	-3.293},
			[10] = 	{-0.267,	0.054,	3.293},
			[11] = 	{-0.267,	0.054,	-3.293},
		}, -- end of fires_pos
		
		chaff_flare_dispenser = 
		{
			[1] = 
			{
				dir = 	{0,	-1,	0},
				pos = 	{-1.185,	-1.728,	-0.878},
			}, -- end of [1]
			[2] = 
			{
				dir = 	{0,	-1,	0},
				pos = 	{-1.185,	-1.728,	0.878},
			}, -- end of [2]
		}, -- end of chaff_flare_dispenser
		
        -- Countermeasures
passivCounterm 		= {
CMDS_Edit 			= true,
SingleChargeTotal 	= 240,
chaff 				= {default = 120, increment = 30, chargeSz = 1},
flare 				= {default = 60, increment = 15, chargeSz = 2}
 },		

        CanopyGeometry = {
            azimuth = {-110.0, 110.0},
            elevation = {-40.0, 70.0}
        },
		
Sensors = {
RADAR = "AN/APS-138",
RWR = "Abstract RWR"
},
Countermeasures = {
ECM = "AN/ALQ-135"--f15
},		
		

	Failures = {
			{ id = 'asc', 		label = _('ASC'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'autopilot', label = _('AUTOPILOT'), enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hydro',  	label = _('HYDRO'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'l_engine',  label = _('L-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'r_engine',  label = _('R-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'radar',  	label = _('RADAR'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
--			{ id = 'eos',  		label = _('EOS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
--			{ id = 'helmet',  	label = _('HELMET'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mlws',  	label = _('MLWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'rws',  		label = _('RWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'ecm',   	label = _('ECM'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hud',  		label = _('HUD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mfd',  		label = _('MFD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
	},
	HumanRadio = {
		frequency = 127.5,  -- Radio Freq
		editable = true,
		minFrequency = 100.000,
		maxFrequency = 156.000,
		modulation = MODULATION_AM
	},

	Pylons =     {

        pylon(1, 0, 0, 0, 0,
            {
            },
            {
            }
        ),
	},
	
	Tasks = {
        aircraft_task(Transport),
    },	
	DefaultTask = aircraft_task(Transport),

	SFM_Data = {     --E2C.
	aerodynamics =
		{
			Cy0	=	0,
			Mzalfa	=	4.355,
			Mzalfadt	=	0.8,
			kjx	=	2.75,
			kjz	=	0.00125,
			Czbe	=	-0.016,
			cx_gear	=	0.015,
			cx_flap	=	0.05,
			cy_flap	=	1,
			cx_brk	=	0.06,
			table_data = 
			{
				[1] = 	{0,	0.022,	0.117,	0.0397,	1e-006,	0.5,	30,	1.2},
				[2] = 	{0.1,	0.022,	0.117,	0.0397,	1e-006,	1,	30,	1.2},
				[3] = 	{0.2,	0.022,	0.117,	0.0397,	1e-006,	1.5,	30,	1.2},
				[4] = 	{0.3,	0.022,	0.117,	0.0397,	1e-006,	2,	30,	1.2},
				[5] = 	{0.4,	0.022,	0.117,	0.0397,	1e-006,	2.5,	30,	1.2},
				[6] = 	{0.5,	0.022,	0.117,	0.0397,	1e-006,	3,	30,	1.2},
				[7] = 	{0.6,	0.022,	0.117,	0,	0.32,	3.5,	30,	1.2},
				[8] = 	{0.7,	0.025,	0.117,	0.049,	0.9,	3.5,	28.666666666667,	1.18},
				[9] = 	{0.8,	0.034,	0.117,	0.117,	1,	3.5,	27.333333333333,	1.16},
				[10] = 	{1.05,	0.04775,	0.117,	0.186375,	0.48125,	3.5,	24,	1.11},
				[11] = 	{1.1,	0.0505,	0.117,	0.20025,	0.3775,	3.15,	18,	1.1},
				[12] = 	{1.2,	0.056,	0.117,	0.228,	0.17,	2.45,	17,	1.05},
				[13] = 	{1.3,	0.056,	0.117,	0.228,	0.17,	1.75,	16,	1},
				[14] = 	{1.5,	0.056,	0.117,	0.228,	0.17,	1.5,	13,	0.9},
				[15] = 	{1.7,	0.056,	0.117,	0.228,	0.17,	0.9,	12,	0.7},
				[16] = 	{2.2,	0.056,	0.117,	0.228,	0.17,	0.7,	9,	0.4},
				[17] = 	{3.9,	0.056,	0.117,	0.228,	0.17,	0.7,	9,	0.4},
			}, -- end of table_data
		}, -- end of aerodynamics
		engine = 
		{
			Nmg	=	67.5,
			MinRUD	=	0,
			MaxRUD	=	1,
			MaksRUD	=	1,
			ForsRUD	=	1,
			--type	=	"TurboJet", 
			type	=	"TurboProp", 	--MQ-9 Reaper
			hMaxEng	=	19,
			dcx_eng	=	0.0144,
			cemax	=	1.24,
			cefor	=	2.56,
			dpdh_m	=	3000,
			dpdh_f	=	3000,
			table_data = 
			{
				[1] = 	{0,		75395.9,	75395.9},--75395.9
				[2] = 	{0.1,	274143.8,	274143.8},--74143.8
				[3] = 	{0.2,	61765.6,	61765.6},
				[4] = 	{0.3,	51900.8,	51900.8},
				[5] = 	{0.4,	43773.3,	43773.3},
				[6] = 	{0.5,	35854.2,	35854.2},
				[7] = 	{0.6,	29229.2,	29229.2},
				[8] = 	{0.7,	24312.3,	24312.3},
				[9] = 	{0.8,	20719.3,	20719.3},
				[10] = 	{0.9,	16500,	16500},
			}, -- end of table_data
		}, -- end of engine
	},


	--damage , index meaning see in  Scripts\Aircrafts\_Common\Damage.lua
	Damage = {
	[0]  = {critical_damage = 5,  args = {146}},--NOSE_CENTER
	[1]  = {critical_damage = 3,  args = {296}},--NOSE_LEFT_SIDE
	[2]  = {critical_damage = 3,  args = {297}},--NOSE_RIGHT_SIDE
	[3]  = {critical_damage = 8, args = {65}},--CABINA / COCKPIT
	[4]  = {critical_damage = 2,  args = {298}},--CABIN_LEFT_SIDE
	[5]  = {critical_damage = 2,  args = {301}},--CABIN_RIGHT_SIDE
	[7]  = {critical_damage = 2,  args = {249}},--GUN
	[8]  = {critical_damage = 3,  args = {265}},--FRONT_GEAR_BOX
	[9]  = {critical_damage = 3,  args = {154}},--FUSELAGE_LEFT_SIDE
	[10] = {critical_damage = 3,  args = {153}},--MAIN / FUSELAGE_RIGHT_SIDE
	[11] = {critical_damage = 1,  args = {167}},--ENGINE_L
	[12] = {critical_damage = 1,  args = {161}},--ENGINE_R
	[13] = {critical_damage = 2,  args = {169}},--MTG_L_BOTTOM
	[14] = {critical_damage = 2,  args = {163}},--MTG_R_BOTTOM
	[15] = {critical_damage = 2,  args = {267}},--LEFT_GEAR_BOX
	[16] = {critical_damage = 2,  args = {266}},--RIGHT_GEAR_BOX
	[17] = {critical_damage = 2,  args = {168}},--ENGINE_L_OUT
	[18] = {critical_damage = 2,  args = {162}},--ENGINE_R_OUT
	[20] = {critical_damage = 2,  args = {183}},--AIR_BRAKE_R
	[23] = {critical_damage = 5, args = {223}},--WING_L_OUT
	[24] = {critical_damage = 5, args = {213}},--WING_R_OUT
	[25] = {critical_damage = 2,  args = {226}},--ELERON_L
	[26] = {critical_damage = 2,  args = {216}},--ELERON_R
	[29] = {critical_damage = 5, args = {224}, deps_cells = {23, 25}},--WING_L_CENTER
	[30] = {critical_damage = 5, args = {214}, deps_cells = {24, 26}},--WING_R_CENTER
	[35] = {critical_damage = 6, args = {225}, deps_cells = {23, 29, 25, 37}},--WING_L_IN
	[36] = {critical_damage = 6, args = {215}, deps_cells = {24, 30, 26, 38}},--WING_R_IN
	[37] = {critical_damage = 2,  args = {228}},--FLAP_L_IN
	[38] = {critical_damage = 2,  args = {218}},--FLAP_R_IN
	[39] = {critical_damage = 2,  args = {244}, deps_cells = {53}},--FIN_L_TOP
	[40] = {critical_damage = 2,  args = {241}, deps_cells = {54}},--FIN_R_TOP 
	[43] = {critical_damage = 2,  args = {243}, deps_cells = {39, 53}},--FIN_L_BOTTOM
	[44] = {critical_damage = 2,  args = {242}, deps_cells = {40, 54}},--FIN_R_BOTTOM 
	[51] = {critical_damage = 2,  args = {240}},--ELEVATOR_L_IN
	[52] = {critical_damage = 2,  args = {238}},--ELEVATOR_R_IN
	[53] = {critical_damage = 2,  args = {248}},--RUDDER_L
	[54] = {critical_damage = 2,  args = {247}},--RUDDER_R
	[56] = {critical_damage = 2,  args = {158}},--TAIL_LEFT_SIDE
	[57] = {critical_damage = 2,  args = {157}},--TAIL_RIGHT_SIDE
	[59] = {critical_damage = 3,  args = {148}},--NOSE_BOTTOM
	[61] = {critical_damage = 2,  args = {147}},--FUEL_TANK_F
	[82] = {critical_damage = 2,  args = {152}},--FUSELAGE_BOTTOM
	[105] = {critical_damage = 2,  args = {603}},--ENGINE_3
	[106] = {critical_damage = 2,  args = {604}},--ENGINE_4
	},
	
	DamageParts = 
	{  
		[1] = "c2a_greyhound-oblomok-wing-r", -- wing R
		[2] = "c2a_greyhound-oblomok-wing-l", -- wing L
	},
	
-- VSN DCS World\Scripts\Aircrafts\_Common\Lights.lua

	lights_data = { typename = "collection", lights = {
	
    [1] = { typename = "collection", -- WOLALIGHT_STROBES
					lights = {	
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV_BANO_1"},--R
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV1_BANO_1"},--L
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV2_BANO_1"},--H
						--{typename  = "natostrobelight",	argument_1  = 195, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON L"},--195
						--{typename  = "natostrobelight",	argument_1  = 196, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON R"},--196
						--{typename  = "natostrobelight",	argument_1  = 192, period = 1.2, color = {0.8,0,0}, connector = "BANO_0_BACK"},
						--{typename  = "natostrobelight",	argument_1  = 195, period = 1.2, color = {0.8,0,0}, connector = "RED_BEACON L"},
						--{typename  = "natostrobelight",	argument_1  = 196, period = 1.2, color = {0.8,0,0}, connector = "RED_BEACON R"},
							}
			},
	[2] = { typename = "collection",
					lights = {-- 1=Landing light -- 2=Landing/Taxi light
						{typename = "spotlight", connector = "MAIN_SPOT_PTR", argument = 209, dir_correction = {elevation = math.rad(-1)}},--"MAIN_SPOT_PTR_02","RESERV_SPOT_PTR"
						{typename = "spotlight", connector = "MAIN_SPOT_PTR", argument = 208, dir_correction = {elevation = math.rad(3)}},--"MAIN_SPOT_PTR_01","RESERV_SPOT_PTR","MAIN_SPOT_PTL",
							}
			},
    [3]	= {	typename = "collection", -- nav_lights_default
					lights = {
						{typename  = "omnilight",connector =  "BANO_1"  ,argument  =  190,color = {0.99, 0.11, 0.3}},-- Left Position(red)
						{typename  = "omnilight",connector =  "BANO_2"  ,argument  =  191,color = {0, 0.894, 0.6}},-- Right Position(green)
						{typename  = "omnilight",connector =  "BANO_0"  ,argument  =  192,color = {1, 1, 1}},-- Tail Position white)
							}
			},
	[4] = { typename = "collection", -- formation_lights_default
					lights = {
						{typename  = "argumentlight" ,argument  = 200,},--formation_lights_tail_1 = 200;
						{typename  = "argumentlight" ,argument  = 201,},--formation_lights_tail_2 = 201;
						{typename  = "argumentlight" ,argument  = 202,},--formation_lights_left   = 202;
						{typename  = "argumentlight" ,argument  = 203,},--formation_lights_right  = 203;
						{typename  = "argumentlight" ,argument  =  88,},--old aircraft arg 
							}
			},
--[[			
	[5] = { typename = "collection", -- strobe_lights_default
					lights = {
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument = 193, color = {0.8,0,0}},-- Arg 193, 83,
						{typename  = "strobelight",connector =  "RED_BEACON_2",argument = 194, color = {0.8,0,0}},-- (-1"RESERV_RED_BEACON")
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument =  83, color = {0.8,0,0}},-- Arg 193, 83,
							}
			},
--]]			
	}},
}

add_aircraft(C2A_Greyhound)
