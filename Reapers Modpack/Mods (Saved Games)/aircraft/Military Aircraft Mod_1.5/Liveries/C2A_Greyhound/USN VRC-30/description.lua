livery = {
    {"MAM_C2A", 0, "MAM_C2A",true},
	{"MAM_C2A_Wings", 0, "MAM_C2A_Wings",true},

	{"MAM_C2A", 1, "MAM_C2A_B",true},
	{"MAM_C2A_Wings", 1, "MAM_C2A_Wings_B",true},

	{"MAM_C2A", ROUGHNESS_METALLIC, "MAM_C2A_RoughMet",true},
	{"MAM_C2A_Wings", ROUGHNESS_METALLIC, "MAM_C2A_Wings_RoughMet",true},
	
	{"pilot_F15_patch", 0 ,"pilot_c2_patch_VRC30",false},
 
}

name = "USN - VRC-30 DET-1"

