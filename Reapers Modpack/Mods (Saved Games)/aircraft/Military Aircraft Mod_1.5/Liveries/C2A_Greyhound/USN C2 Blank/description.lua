livery = {
    {"MAM_C2A", 0, "MAM_C2Ablank",false},
	{"MAM_C2A_Wings", 0, "MAM_C2A_Wings",true},

	{"MAM_C2A", 1, "MAM_C2A_B",true},
	{"MAM_C2A_Wings", 1, "MAM_C2A_Wings_B",true},

	{"MAM_C2A", ROUGHNESS_METALLIC, "MAM_C2A_RoughMet",true},
	{"MAM_C2A_Wings", ROUGHNESS_METALLIC, "MAM_C2A_Wings_RoughMet",true},
 
}

name = "Blank"

