livery = {
    {"MAM_C5_Galaxy", 0, "../MAC/MAM_C5_Galaxy_MAC",false},
	{"MAM_C5_Decals", 0, "../MAC/MAM_C5_Galaxy_MAC",false},

    {"MAM_C5_Decals", DECAL, "../MAC/MAM_C5_Decals_MAC",false},																

	{"MAM_Pilot", 0, "MAM_Pilot",true},

	{"MAM_C5_Galaxy", 1, "MAM_C5_Galaxy_B",true},
	{"MAM_Pilot", 1, "MAM_Pilot_B",true},

	{"MAM_C5_Galaxy", ROUGHNESS_METALLIC, "MAM_C5_Galaxy_RoughMet",true},
	{"MAM_Pilot", ROUGHNESS_METALLIC, "MAM_Pilot_RoughMet",true},
 
}

custom_args =
{
   [70] = 1.0,
} 

name = "USAF - MAC - open Doors"

