livery = {
    {"MAM_C5_Galaxy", 0, "MAM_C5_Galaxy_MC",false},
	{"MAM_C5_Decals", 0, "MAM_C5_Galaxy_MC",false},
	
    {"MAM_C5_Decals", DECAL, "MAM_C5_Decals_MC",false},

	{"MAM_Pilot", 0, "MAM_Pilot",true},

	{"MAM_C5_Galaxy", 1, "MAM_C5_Galaxy_B",true},
	{"MAM_Pilot", 1, "MAM_Pilot_B",true},

	{"MAM_C5_Galaxy", ROUGHNESS_METALLIC, "MAM_C5_Galaxy_RoughMet",true},
	{"MAM_Pilot", ROUGHNESS_METALLIC, "MAM_Pilot_RoughMet",true},
 
}

custom_args =
{
   [70] = 0.0,
} 

name = "USAF - MEMPHIS ANG Camo"

