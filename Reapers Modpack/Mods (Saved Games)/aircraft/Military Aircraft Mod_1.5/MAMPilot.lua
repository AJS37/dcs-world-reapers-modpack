--Pilot Mod - By SUNTSAG; Modified Warlord --

	
GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot";
GT.DisplayName = _("MAM USAF Pilot");
GT.Rate = 1;

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)	

GT_t.CH_t.HUMAN_T = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -19.875,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN_T);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot_TARAWA";
GT.DisplayName = _("MAM USAF Pilot_TARAWA");
GT.Rate = 1;

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)	

GT_t.CH_t.HUMAN_S = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -19.06,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN_S);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot_STENNIS";
GT.DisplayName = _("MAM USAF Pilot_STENNIS");
GT.Rate = 1;

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)

GT_t.CH_t.HUMAN_B = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -19.6,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN_B);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot_BUSH";
GT.DisplayName = _("MAM USAF Pilot_BUSH");
GT.Rate = 1;


GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)
	
GT_t.CH_t.HUMAN_K = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -16.257,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN_K);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot_KUZNETZOV";
GT.DisplayName = _("MAM USAF KUZNETZOV");
GT.Rate = 1;

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)		

GT_t.CH_t.HUMAN_H = {
    life = 0.08,
    mass = 90,
    length = 1,
    width = 1,
    max_road_velocity = 4,
    max_slope = 0.87,
	canSwim = true,
	canWade = true,
	waterline_level = -11.812,
    engine_power = 0.5,
	fordingDepth = 1.0,
    max_vert_obstacle = 1,
    max_acceleration = 3.0,
    min_turn_radius = 0.1,
    X_gear_1 = 0.3,
    Y_gear_1 = 0,
    Z_gear_1 = 0.0,
    X_gear_2 = 0.0,
    Y_gear_2 = 0,
    Z_gear_2 = 0.0,
	gear_type = GT_t.GEAR_TYPES.HUMAN,
    r_max = 0.53,
    armour_thickness = 0,
	human_figure = true,
}

GT = {};
GT.animation = {};

GT_t.ws = 0;
set_recursive_metatable(GT, GT_t.generic_human);
set_recursive_metatable(GT.chassis, GT_t.CH_t.HUMAN_H);
set_recursive_metatable(GT.animation, GT_t.CH_t.HUMAN_ANIMATION);

GT.visual.shape = "MAM_usaf_pilot";
GT.visual.shape_dstr = "soldier_ge_00_d";
GT.CustomAimPoint = {0,1.0,0};

GT.AddPropVehicle = {
			{ id = "TentedRoof" , control = 'checkbox', label = _('Pilot Face'), defValue = true, arg=70} 
		}

GT.mobile = true;

-- weapon systems

GT.WS = {};
GT.WS.maxTargetDetectionRange = 7500;
GT.WS.fire_on_march = false;

local ws = GT_t.inc_ws();
GT.WS[ws] = {};
set_recursive_metatable(GT.WS[ws], GT_t.WS_t.igla_manpad);
GT.driverViewConnectorName = {"camera", offset = {0.0, 0.0, 0.0}}
GT.WS[ws].pointer = "camera";

GT.Name = "MAMPilot_HERMES";
GT.DisplayName = _("*MAM USAF HERMES");
GT.Rate = 1;

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000201";
GT.attribute = {wsType_Ground,wsType_Miss,IglaGRG_1,wsType_Gun,wsType_GenericInfantry,
				"MANPADS",
				"New infantry",
				};
				
GT.category = "Infantry";

GT.Transportable = {
	size = 100
}

add_surface_unit(GT)
