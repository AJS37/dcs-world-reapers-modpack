
P3C_Orion =  {
      
		Name 			= 'P3C_Orion',--AG
		DisplayName		= _('P-3C Orion'),--AG
        Picture 		= "P3C_Orion.png",
        Rate 			= "40",
        Shape			= "P3C_Orion",--AG	
		WorldID			=  WSTYPE_PLACEHOLDER, 
		--WorldID 		= 31,		
		singleInFlight 	= true,
        
	shape_table_data 	= 
	{
		{
			file  	 	= 'P3C_Orion';--AG
			life  	 	= 20; -- lifebar
			vis   	 	= 3; -- visibility gain.
			desrt    	= 'P3C_Orion-oblomok'; -- Name of destroyed object file name
			fire  	 	= { 300, 2}; 			-- Fire on the ground after destoyed: 300sec 2m
			username	= 'P3C_Orion';--AG
			index       =  WSTYPE_PLACEHOLDER;
			--index       =  P3C_Orion;
			classname   = "lLandPlane";
			positioning = "BYNORMAL";
		},
		{
			name  		= "P3C_Orion-oblomok";
			file  		= "P3C_Orion-oblomok";
			fire  		= { 240, 2};
		},
	},
	
	    mapclasskey 		= "P0091000029",
	    attribute  			= {wsType_Air, wsType_Airplane, wsType_Cruiser, WSTYPE_PLACEHOLDER ,
        "Transports", "Refuelable",},
		
	    Categories = {
        },
				
		M_empty	=	35000,--c-130
		M_nominal	=	61400,
		M_max	=	64410,
		M_fuel_max	=	34800,
		H_max	=	8625,
		average_fuel_consumption	=	0.06,
		CAS_min	=	54,
		V_opt	=	174,
		V_take_off	=	58,
		V_land	=	61,
		has_afteburner	=	false,
		has_speedbrake	=	false,
		has_differential_stabilizer = false,
		nose_gear_pos = 	{7.98,	-3.05,	0},
		main_gear_pos = 	{-0.80,	-3.10,	4.62},
		radar_can_see_ground	=	false,
		AOA_take_off	=	0.17,
		stores_number	=	0,
		bank_angle_max	=	45,
		Ny_min	=	0.5,
		Ny_max	=	2.5,
		tand_gear_max	=	0.577,
		V_max_sea_level	=	328,
		V_max_h	=	411,
		tanker_type	=	0,
		wing_area	=	120.77,
		wing_span	=	30.37,--B-52 60.0,
		thrust_sum_max	=	44400,
		thrust_sum_ab	=	44400,
		Vy_max	=	9.1,
		length	=	35.61,--tu-160 67.0,
		height	=	10.27,--c-17 19.60,
		flaps_maneuver	=	0.5,
		Mach_max	=	0.63,
		range	=	8260,
		RCS	=	80,
		Ny_max_e	=	2,
		detection_range_max	=	0,
		IR_emission_coeff	=	1,
		IR_emission_coeff_ab	=	0,
		engines_count	=	4,
		wing_tip_pos = 	{-0.753,	0.617,	18.04},
		nose_gear_wheel_diameter	=	0.83,
		main_gear_wheel_diameter	=	1.09,
		engines_nozzles = 
		{
			[1] = 
			{
				pos = 	{-0.8,	0.9,	9.5},--
				elevation	=	0,
				diameter	=	4.6,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.2, 
			}, -- end of [1]
			[2] = 
			{
				pos = 	{-0.8,	0.9,	-9.5},--
				elevation	=	0,
				diameter	=	4.6,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.2, 
			}, -- end of [2] 
			[3] = 
			{
				pos = 	{5.3,	0.15,	15.5},
				elevation	=	0,
				diameter	=	4.6,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.2, 
			}, -- end of [3]
			[4] = 
			{
				pos = 	{5.3,	0.15,	15.5},
				elevation	=	0,
				diameter	=	4.6,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
				smokiness_level     = 	0.2, 
			}, -- end of [4]
		}, -- end of engines_nozzles
		crew_members = 
		{
			[1] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{7.916,	0.986,	0},
			}, -- end of [1]
			[2] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
			}, -- end of [2]
			[3] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
			}, -- end of [3]
		}, -- end of crew_members
		brakeshute_name	=	0,
		is_tanker	=	false,
--		air_refuel_receptacle_pos = 	{6.731,	0.825,	0.492},
		fires_pos = 
		{
			[1] = 	{-0.138,	-0.79,	0},
			[2] = 	{-0.138,	-0.79,	5.741},
			[3] = 	{-0.138,	-0.79,	-5.741},
			[4] = 	{-0.82,	0.265,	2.774},
			[5] = 	{-0.82,	0.265,	-2.774},
			[6] = 	{-0.82,	0.255,	4.274},
			[7] = 	{-0.82,	0.255,	-4.274},
			[8] = 	{-0.347,	-1.875,	8.138},
			[9] = 	{-0.347,	-1.875,	-8.138},
			[10] = 	{-5.024,	-1.353,	13.986},
			[11] = 	{-5.024,	-1.353,	-13.986},
		}, -- end of fires_pos

        CanopyGeometry = {
            azimuth = {-110.0, 110.0},
            elevation = {-40.0, 70.0}
        },

	Failures = {
					{ id = 'asc', 			label = _('ASC'), 			enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
					{ id = 'autopilot', label = _('AUTOPILOT'), enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
					{ id = 'hydro',  		label = _('HYDRO'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
					{ id = 'l_engine',  label = _('L-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
					{ id = 'r_engine',	label = _('R-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
	},
	HumanRadio = {
		frequency = 127.5,  -- Radio Freq
		editable = true,
		minFrequency = 100.000,
		maxFrequency = 156.000,
		modulation = MODULATION_AM
	},

	Pylons =     {

        pylon(1, 0, -2.15, 0.395, -6.405,
            {
				use_full_connector_position = true,
				arg 	  	  = 308,
				arg_increment = 1,
            },
            {
				{ CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E741}" ,arg_increment = 0.0}, --Smokewinder - red
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E742}" ,arg_increment = 0.0}, --Smokewinder - green
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E743}" ,arg_increment = 0.0}, --Smokewinder - blue
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E744}" ,arg_increment = 0.0}, --Smokewinder - white
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E745}" ,arg_increment = 0.0}, --Smokewinder - yellow
				{ CLSID = "{AIS_ASQ_T50}" ,arg_increment = 0.0, attach_point_position = {0.30,  0.0,  0.0}},-- ACMI pod
				{ CLSID = "LAU-115_LAU-127_AIM-9L",arg_increment = 0.0},				  -- AIM-9L
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
            }
        ),
        pylon(2, 0, -1.87, 0.075, -4.27,
            {
				use_full_connector_position = true,
				arg 	  	  = 309,
				arg_increment = 1,
            },
            {
				{ CLSID = "LAU-115_LAU-127_AIM-9L",arg_increment = 0.0},	-- AIM-9L
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID = "{0B9ABA77-93B8-45FC-9C63-82AFB2CB50A4}" ,arg_increment = 0.0}, -- 2 Mk-20 Rockeye
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
				{ CLSID = "{AF42E6DF-9A60-46D8-A9A0-1708B241AADB}" ,arg_increment = 0.0}, -- AGM-84E
				{ CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EC}" ,arg_increment = 0.0}, -- AGM-65E
				{ CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}" ,arg_increment = 0.0}, -- LAU-10 - 4 ZUNI MK 71
            }
        ),
        pylon(3, 0, -1.02, -0.074, -3.325,
            {
				use_full_connector_position = true,
				arg 	  	  = 310,
				arg_increment = 1,
            },
            {
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{60CC734F-0AFA-4E2E-82B8-93B941AB11CF}" ,arg_increment = 0.0}, -- 3 Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID = "{0B9ABA77-93B8-45FC-9C63-82AFB2CB50A4}" ,arg_increment = 0.0}, -- 2 Mk-20 Rockeye
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
				{ CLSID = "{AF42E6DF-9A60-46D8-A9A0-1708B241AADB}" ,arg_increment = 0.0}, -- AGM-84E
				{ CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EC}" ,arg_increment = 0.0}, -- AGM-65E
				{ CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}" ,arg_increment = 0.0}, -- LAU-10 - 4 ZUNI MK 71
            }
        ),
        pylon(4, 0, -0.77, -0.066, -2.297,
            {
				use_full_connector_position = true,
				arg 	  	  = 311,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
				
            }
        ),
        pylon(5, 1, -2.47, -0.45, -1.165,--2.47, -0.5, -1.215
            {
				use_full_connector_position = true,
				arg 	  	  = 312,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
            }
        ),
        pylon(6, 1, 0.93, -0.466, 0,
            {
				use_full_connector_position = true,
				arg 	  	  = 313,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
				{ CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}" }, -- Mk-83
            }
        ),
        pylon(7, 1, -2.47, -0.45, 1.165,--2.47, -0.5, 1.215
            {
				use_full_connector_position = true,
				arg 	  	  = 314,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
				{ CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}" }, -- Mk-83
            }
        ),
        pylon(8, 0, -0.77, -0.066, 2.297,
			{
				use_full_connector_position = true,
				arg 	  	  = 315,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
				{ CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}" }, -- Mk-83
            }
        ),
        pylon(9, 0, -1.02, -0.074, 3.325,
            {
				use_full_connector_position = true,
				arg 	  	  = 316,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
				{ CLSID = "{7A44FF09-527C-4B7E-B42B-3F111CFE50FB}" }, -- Mk-83
            }
        ),
        pylon(10, 0, -1.87, 0.075, 4.27,            
			{
				use_full_connector_position = true,
				arg 	  	  = 317,
				arg_increment = 1,
            },
            {
                { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
            }
        ),
        pylon(11, 0, -2.15, 0.395, 6.405,--2.15, 0.475, 6.455
            {
				use_full_connector_position = true,
				arg 	  	  = 318,
				arg_increment = 1,
            },
            {
		        { CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" }, -- Mk-20
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" }, -- Mk-82
            }
        ),
		pylon(12, 0, -2.15, 0.395, 6.405,--2.15, 0.475, 6.455
            {
				use_full_connector_position = true,
				arg 	  	  = 319,
				arg_increment = 1,
            },
            {
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{60CC734F-0AFA-4E2E-82B8-93B941AB11CF}" ,arg_increment = 0.0}, -- 3 Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID = "{0B9ABA77-93B8-45FC-9C63-82AFB2CB50A4}" ,arg_increment = 0.0}, -- 2 Mk-20 Rockeye
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
				{ CLSID = "{AF42E6DF-9A60-46D8-A9A0-1708B241AADB}" ,arg_increment = 0.0}, -- AGM-84E
				{ CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EC}" ,arg_increment = 0.0}, -- AGM-65E
				{ CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}" ,arg_increment = 0.0}, -- LAU-10 - 4 ZUNI MK 71
            }
        ),
		pylon(13, 0, -2.15, 0.395, 6.405,--2.15, 0.475, 6.455
            {
				use_full_connector_position = true,
				arg 	  	  = 320,
				arg_increment = 1,
            },
            {
		        { CLSID = "LAU-115_LAU-127_AIM-9L",arg_increment = 0.0},	-- AIM-9L
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID = "{0B9ABA77-93B8-45FC-9C63-82AFB2CB50A4}" ,arg_increment = 0.0}, -- 2 Mk-20 Rockeye
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
				{ CLSID = "{AF42E6DF-9A60-46D8-A9A0-1708B241AADB}" ,arg_increment = 0.0}, -- AGM-84E
				{ CLSID = "{F16A4DE0-116C-4A71-97F0-2CF85B0313EC}" ,arg_increment = 0.0}, -- AGM-65E
				{ CLSID = "{F3EFE0AB-E91A-42D8-9CA2-B63C91ED570A}" ,arg_increment = 0.0}, -- LAU-10 - 4 ZUNI MK 71
            }
        ),
		pylon(14, 0, -2.15, 0.395, 6.405,--2.15, 0.475, 6.455
            {
				use_full_connector_position = true,
				arg 	  	  = 321,
				arg_increment = 1,
            },
            {
		        { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E741}" ,arg_increment = 0.0}, --Smokewinder - red
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E742}" ,arg_increment = 0.0}, --Smokewinder - green
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E743}" ,arg_increment = 0.0}, --Smokewinder - blue
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E744}" ,arg_increment = 0.0}, --Smokewinder - white
			    { CLSID	= "{A4BCC903-06C8-47bb-9937-A30FEDB4E745}" ,arg_increment = 0.0}, --Smokewinder - yellow
				{ CLSID = "{AIS_ASQ_T50}" ,arg_increment = 0.0, attach_point_position = {0.30,  0.0,  0.0}},-- ACMI pod
				{ CLSID = "LAU-115_LAU-127_AIM-9L",arg_increment = 0.0},				  -- AIM-9L
				{ CLSID = "{BCE4E030-38E9-423E-98ED-24BE3DA87C32}" ,arg_increment = 0.0}, -- Mk-82
				{ CLSID = "{ADD3FAE1-EBF6-4EF9-8EFC-B36B5DDF1E6B}" ,arg_increment = 0.0}, -- Mk-20
				{ CLSID	= "{AGM_84D}", Type = 1 ,arg_increment = 0.0}, 					  -- AGM-84D Harpoon 	
            }
        ),
},
	
	Tasks = {
		aircraft_task(Transport),
        --aircraft_task(CAP),
     	--aircraft_task(Escort),
      	--aircraft_task(FighterSweep),
		--aircraft_task(Intercept),
		--aircraft_task(Reconnaissance),
    	aircraft_task(GroundAttack),
     	aircraft_task(CAS),
        aircraft_task(AFAC),
	    aircraft_task(RunwayAttack),
    	aircraft_task(AntishipStrike),
    },	
	DefaultTask = aircraft_task(AntishipStrike),
	
	
	

	SFM_Data = {     --c-130
	aerodynamics = 
		{
			Cy0	=	0,
			Mzalfa	=	6.6,
			Mzalfadt	=	1,
			kjx	=	2.85,
			kjz	=	0.00125,
			Czbe	=	-0.012,
			cx_gear	=	0.015,
			cx_flap	=	0.08,
			cy_flap	=	1,
			cx_brk	=	0.06,
			table_data = 
			{
				[1] = 	{0,		0.024,	0.1,	0.0384,	1e-006,	0.5,	20,	1.2},
				[2] = 	{0.2,	0.024,	0.1,	0.0384,	1e-006,	1.5,	20,	1.2},
				[3] = 	{0.4,	0.024,	0.1,	0.0384,	1e-006,	2.5,	20,	1.2},
				[4] = 	{0.5,	0.024,	0.1,	0.0384,	1e-006,	2.5,	20,	1.2},
				[5] = 	{0.6,	0.027,	0.1,	0.0,	0.3,	3.5,	20,	1.2},
				[6] = 	{0.7,	0.031,	0.1,	0.045,	0.9,	3.5,	20,	1},
				[7] = 	{0.8,	0.036,	0.1,	0.107,	1,		3.5,	20,	0.8},
				[8] = 	{0.9,	0.045,	0.1,	0.148,	0.058,	3.5,	20,	0.6},
				[9] = 	{1,		0.054,	0.1,	0.199,	0.1,	3.5,	20,	0.53333333333333},
				[10] = 	{1.5,	0.054,	0.1,	0.199,	0.1,	3.5,	20,	0.2},
			}, -- end of table_data
		}, -- end of aerodynamics
		engine = 
		{
			Nmg	=	67.5,
			MinRUD	=	0,
			MaxRUD	=	1,
			MaksRUD	=	1,
			ForsRUD	=	1,
			--type	=	"TurboJet", 
			type	=	"TurboProp", 	--MQ-9 Reaper
			hMaxEng	=	19.5,
			dcx_eng	=	0.0085,
			cemax	=	0.37,
			cefor	=	0.37,
			dpdh_m	=	4820,
			dpdh_f	=	4820,
			table_data = 
			{
				[1] = 	{0,		150791.9,	150791.9},
				[2] = 	{0.1,	148287.6,	148287.6},
				[3] = 	{0.2,	123531.3,	123531.3},
				[4] = 	{0.3,	103801.6,	103801.6},
				[5] = 	{0.4,	87546.7,	87546.7},
				[6] = 	{0.5,	71708.3,	71708.3},
				[7] = 	{0.6,	58458.4,	58458.4},
				[8] = 	{0.7,	48624.7,	48624.7},
				[9] = 	{0.8,	41438.6,	41438.6},
				[10] = 	{0.9,	33000,		33000},
			}, -- end of table_data
		}, -- end of engine
	},


	--damage , index meaning see in  Scripts\Aircrafts\_Common\Damage.lua
	Damage = {
	[0]  = {critical_damage = 5,  args = {146}},--NOSE_CENTER
	[1]  = {critical_damage = 3,  args = {296}},--NOSE_LEFT_SIDE
	[2]  = {critical_damage = 3,  args = {297}},--NOSE_RIGHT_SIDE
	[3]  = {critical_damage = 8, args = {65}},--CABINA / COCKPIT
	[4]  = {critical_damage = 2,  args = {298}},--CABIN_LEFT_SIDE
	[5]  = {critical_damage = 2,  args = {301}},--CABIN_RIGHT_SIDE
	[7]  = {critical_damage = 2,  args = {249}},--GUN
	[8]  = {critical_damage = 3,  args = {265}},--FRONT_GEAR_BOX
	[9]  = {critical_damage = 3,  args = {154}},--FUSELAGE_LEFT_SIDE
	[10] = {critical_damage = 3,  args = {153}},--MAIN / FUSELAGE_RIGHT_SIDE
	[11] = {critical_damage = 1,  args = {167}},--ENGINE_L
	[12] = {critical_damage = 1,  args = {161}},--ENGINE_R
	[13] = {critical_damage = 2,  args = {169}},--MTG_L_BOTTOM
	[14] = {critical_damage = 2,  args = {163}},--MTG_R_BOTTOM
	[15] = {critical_damage = 2,  args = {267}},--LEFT_GEAR_BOX
	[16] = {critical_damage = 2,  args = {266}},--RIGHT_GEAR_BOX
	[17] = {critical_damage = 2,  args = {168}},--ENGINE_L_OUT
	[18] = {critical_damage = 2,  args = {162}},--ENGINE_R_OUT
	[20] = {critical_damage = 2,  args = {183}},--AIR_BRAKE_R
	[23] = {critical_damage = 5, args = {223}},--WING_L_OUT
	[24] = {critical_damage = 5, args = {213}},--WING_R_OUT
	[25] = {critical_damage = 2,  args = {226}},--ELERON_L
	[26] = {critical_damage = 2,  args = {216}},--ELERON_R
	[29] = {critical_damage = 5, args = {224}, deps_cells = {23, 25}},--WING_L_CENTER
	[30] = {critical_damage = 5, args = {214}, deps_cells = {24, 26}},--WING_R_CENTER
	[35] = {critical_damage = 6, args = {225}, deps_cells = {23, 29, 25, 37}},--WING_L_IN
	[36] = {critical_damage = 6, args = {215}, deps_cells = {24, 30, 26, 38}},--WING_R_IN
	[37] = {critical_damage = 2,  args = {228}},--FLAP_L_IN
	[38] = {critical_damage = 2,  args = {218}},--FLAP_R_IN
	[39] = {critical_damage = 2,  args = {244}, deps_cells = {53}},--FIN_L_TOP
	[40] = {critical_damage = 2,  args = {241}, deps_cells = {54}},--FIN_R_TOP 
	[43] = {critical_damage = 2,  args = {243}, deps_cells = {39, 53}},--FIN_L_BOTTOM
	[44] = {critical_damage = 2,  args = {242}, deps_cells = {40, 54}},--FIN_R_BOTTOM 
	[51] = {critical_damage = 2,  args = {240}},--ELEVATOR_L_IN
	[52] = {critical_damage = 2,  args = {238}},--ELEVATOR_R_IN
	[53] = {critical_damage = 2,  args = {248}},--RUDDER_L
	[54] = {critical_damage = 2,  args = {247}},--RUDDER_R
	[56] = {critical_damage = 2,  args = {158}},--TAIL_LEFT_SIDE
	[57] = {critical_damage = 2,  args = {157}},--TAIL_RIGHT_SIDE
	[59] = {critical_damage = 3,  args = {148}},--NOSE_BOTTOM
	[61] = {critical_damage = 2,  args = {147}},--FUEL_TANK_F
	[82] = {critical_damage = 2,  args = {152}},--FUSELAGE_BOTTOM
	[105] = {critical_damage = 2,  args = {603}},--ENGINE_3
	[106] = {critical_damage = 2,  args = {604}},--ENGINE_4
	},
	
	DamageParts = 
	{  
		[1] = "P3C_Orion-OBLOMOK-WING-R", -- wing R
		[2] = "P3C_Orion-OBLOMOK-WING-L", -- wing L
--		[3] = "kc-135-oblomok-noise", -- nose
--		[4] = "kc-135-oblomok-tail-r", -- tail
--		[5] = "kc-135-oblomok-tail-l", -- tail
	},
	
-- VSN DCS World\Scripts\Aircrafts\_Common\Lights.lua

	lights_data = { typename = "collection", lights = {
	
    [1] = { typename = "collection", -- WOLALIGHT_STROBES
					lights = {	
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV_BANO_1"},--R
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV1_BANO_1"},--L
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV2_BANO_1"},--H
						--{typename  = "natostrobelight",	argument_1  = 195, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON L"},--195
						--{typename  = "natostrobelight",	argument_1  = 196, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON R"},--196
						--{typename  = "natostrobelight",	argument_1  = 192, period = 1.2, color = {0.8,0,0}, connector = "BANO_0_BACK"},
						--{typename  = "natostrobelight",	argument_1  = 195, period = 1.2, color = {0.8,0,0}, connector = "RED_BEACON L"},
						--{typename  = "natostrobelight",	argument_1  = 196, period = 1.2, color = {0.8,0,0}, connector = "RED_BEACON R"},
							}
			},
	[2] = { typename = "collection",
					lights = {-- 1=Landing light -- 2=Landing/Taxi light
						{typename = "spotlight", connector = "MAIN_SPOT_PTR", argument = 209, dir_correction = {elevation = math.rad(-1)}},--"MAIN_SPOT_PTR_02","RESERV_SPOT_PTR"
						{typename = "spotlight", connector = "MAIN_SPOT_PTR", argument = 208, dir_correction = {elevation = math.rad(3)}},--"MAIN_SPOT_PTR_01","RESERV_SPOT_PTR","MAIN_SPOT_PTL",
							}
			},
    [3]	= {	typename = "collection", -- nav_lights_default
					lights = {
						{typename  = "omnilight",connector =  "BANO_1"  ,argument  =  190,color = {0.99, 0.11, 0.3}},-- Left Position(red)
						{typename  = "omnilight",connector =  "BANO_2"  ,argument  =  191,color = {0, 0.894, 0.6}},-- Right Position(green)
						{typename  = "omnilight",connector =  "BANO_0"  ,argument  =  192,color = {1, 1, 1}},-- Tail Position white)
							}
			},
	[4] = { typename = "collection", -- formation_lights_default
					lights = {
						{typename  = "argumentlight" ,argument  = 200,},--formation_lights_tail_1 = 200;
						{typename  = "argumentlight" ,argument  = 201,},--formation_lights_tail_2 = 201;
						{typename  = "argumentlight" ,argument  = 202,},--formation_lights_left   = 202;
						{typename  = "argumentlight" ,argument  = 203,},--formation_lights_right  = 203;
						{typename  = "argumentlight" ,argument  =  88,},--old aircraft arg 
							}
			},
--[[			
	[5] = { typename = "collection", -- strobe_lights_default
					lights = {
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument = 193, color = {0.8,0,0}},-- Arg 193, 83,
						{typename  = "strobelight",connector =  "RED_BEACON_2",argument = 194, color = {0.8,0,0}},-- (-1"RESERV_RED_BEACON")
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument =  83, color = {0.8,0,0}},-- Arg 193, 83,
							}
			},
--]]			
	}},
}

add_aircraft(P3C_Orion)
