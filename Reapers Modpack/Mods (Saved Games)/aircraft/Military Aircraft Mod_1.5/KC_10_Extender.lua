	
KC_10_Extender =  {
      
		Name 			= 'KC_10_Extender',--AG
		DisplayName		= _('KC-10 Extender'),--AG
        Picture 		= "KC-10A.png",
        Rate 			= "100",
        Shape			= "KC_10_Extender",--AG	
        WorldID		=  WSTYPE_PLACEHOLDER, 
        
	shape_table_data 	= 
	{
		{
			file  	 	= 'KC_10_Extender';
			life  	 	= 50; -- lifebar
			vis   	 	= 3; -- visibility gain.
			desrt    	= 'KC_10_Extender-oblomok'; -- Name of destroyed object file name
			fire  	 	= { 300, 2}; 			-- Fire on the ground after destoyed: 300sec 2m
			username	= 'KC_10_Extender';
			index       =  WSTYPE_PLACEHOLDER;
			classname   = "lLandPlane";
			positioning = "BYNORMAL";
		},
		{
			name  		= "KC_10_Extender-oblomok";
			file  		= "KC_10_Extender-oblomok";
			fire  		= { 240, 2};
		},
	},
	
	
	mapclasskey 		= "P0091000064",
	attribute  			= {wsType_Air, wsType_Airplane, wsType_Cruiser, WSTYPE_PLACEHOLDER, "Tankers",},
	Categories= {"{8A302789-A55D-4897-B647-66493FA6826F}", "",},
	
		TACAN = true,
		singleInFlight = true,
		
		M_empty	=	110664,
		M_nominal	=	150000,--Wartime 251000lbs = 114000kg ish for Tanker only missions
		M_max	=	267000,-- Peacetime 590000lbs = 267000
		M_fuel_max	=	80000,--Wartime 340000lbs = 154000kg
		H_max	=	12000,
		average_fuel_consumption	=	0.127,
		CAS_min	=	54,
		V_opt	=	208,
		V_take_off	=	58,
		V_land	=	61,
		has_afteburner	=	false,
		has_speedbrake	=	false,
		has_differential_stabilizer = false,
		main_gear_pos = 	{-1.817,	-5.038,	5.525},
		radar_can_see_ground	=	false,
		nose_gear_pos = 	{21.317,	-5.152,	0},
		AOA_take_off	=	0.17,
		stores_number	=	0,
		bank_angle_max	=	45,
		Ny_min	=	0.5,
		Ny_max	=	2.5,
		tand_gear_max	=	0.577,
		V_max_sea_level	=	223.61,
		V_max_h	=	223.61,
		tanker_type	=	1,--***
		wing_area	=	367.7,
		wing_span	=	50.4,
		thrust_sum_max	=	71442,
		thrust_sum_ab	=	71442,
		Vy_max	=	10,
		length	=	55.35,
		height	=	17.7,
		flaps_maneuver	=	0.5,
		Mach_max	=	0.77,
		range	=	7800,
		crew_size	=	4,--***
		RCS	=	80,
		Ny_max_e	=	2,
		detection_range_max	=	0,
		IR_emission_coeff	=	4,
		IR_emission_coeff_ab	=	0,
		engines_count	=	3,
		wing_tip_pos = 	{-6.627,	-0.265,	25.2},
		nose_gear_wheel_diameter	=	0.754,
		main_gear_wheel_diameter	=	0.972,
		engines_nozzles = 
		{
			{
				pos = 	{1.013,	-1.899,	-8.043},
				elevation	=	0,
				diameter	=	1.523,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
			},
			{
				pos = 	{-27.25,	4.797,	0},
				elevation	=	0,
				diameter	=	1.523,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
			},
			{
				pos = 	{1.013,	-1.899,	8.043},
				elevation	=	0,
				diameter	=	1.523,
				exhaust_length_ab	=	11.794,
				exhaust_length_ab_K	=	0.76,
			},
		}, -- end of engines_nozzles
		crew_members = 
		{
			[1] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{7.916,	0.986,	0},
			}, -- end of [1]
			[2] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
			}, -- end of [2]
			[3] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
			}, -- end of [3]
			[4] = 
			{
				ejection_seat_name	=	0,
				drop_canopy_name	=	0,
				pos = 	{3.949,	1.01,	0},
			}, -- end of [4]
		}, -- end of crew_members
		brakeshute_name	=	0,
		
		air_refuel_receptacle_pos = 	{ 26.024,   2.847,   0.00},

		is_tanker				=	1, -- BOOM_AND_RECEPTACLE
		--is_tanker				=	true,
		refueling_points_count	=	1,
		
		refueling_points = 
		{
			--          Front/Rear, Up/Down, Left/Right
			--            +   -      +  -      -    +
			[1] =  { pos = {-34.096,	-11.485,	 0}, clientType = 3 },
			--[2] =  { pos = {-34.096,	-11.485, 0.996}, clientType = 3 },
		}, -- end of refueling_points
		
		fires_pos = 
		{
			[1] = 	{7.166,	-1.843,	0},
			[2] = 	{3.863,	-0.629,	2.578},
			[3] = 	{3.863,	-0.629,	-2.578},
			[4] = 	{-0.82,	0.265,	2.774},
			[5] = 	{-0.82,	0.265,	-2.774},
			[6] = 	{-0.82,	0.255,	4.274},
			[7] = 	{-0.82,	0.255,	-4.274},
			[8] = 	{5.354,	-1.868,	8.017},
			[9] = 	{5.354,	-1.868,	-8.017},
			[10] = 	{-23.974,	4.877,	0},
			[11] = 	{-23.974,	4.877,	0},
		}, -- end of fires_pos
		
		chaff_flare_dispenser = 
		{
			[1] = 
			{
				dir = 	{0,	-1,	0},
				pos = 	{1.158,	-1.77,	-0.967},
			}, -- end of [1]
			[2] = 
			{
				dir = 	{0,	-1,	0},
				pos = 	{1.158,	-1.77,	0.967},
			}, -- end of [2]
		}, -- end of chaff_flare_dispenser


		-- Countermeasures, 
		passivCounterm = {
			CMDS_Edit = true,
			SingleChargeTotal = 240,
			chaff = {default = 120, increment = 30, chargeSz = 1},
			flare = {default = 60, increment = 15, chargeSz = 2}
        },
	
        CanopyGeometry = {
            azimuth = {-145.0, 145.0},
            elevation = {-50.0, 90.0}
        },

Sensors = {
RADAR = "AN/APG-63",--f15
RWR = "Abstract RWR"
},
Countermeasures = {
ECM = "AN/ALQ-135"--f15
},
	Failures = {
			{ id = 'asc', 		label = _('ASC'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'autopilot', label = _('AUTOPILOT'), enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hydro',  	label = _('HYDRO'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'l_engine',  label = _('L-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'r_engine',  label = _('R-ENGINE'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'radar',  	label = _('RADAR'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
--			{ id = 'eos',  		label = _('EOS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
--			{ id = 'helmet',  	label = _('HELMET'), 	enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mlws',  	label = _('MLWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'rws',  		label = _('RWS'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'ecm',   	label = _('ECM'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'hud',  		label = _('HUD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },
			{ id = 'mfd',  		label = _('MFD'), 		enable = false, hh = 0, mm = 0, mmint = 1, prob = 100 },		
	},
	
	HumanRadio = {
		frequency = 127.5,  -- Radio Freq
		editable = true,
		minFrequency = 100.000,
		maxFrequency = 156.000,
		modulation = MODULATION_AM
	},

Guns = {gun_mount("M_61", { count = 0 },{muzzle_pos = {5.00000, 0.250000, 0.000000}})--
},


	Pylons =     {

		pylon(1, 0, 0, 0, 0,
            {
                FiZ = 0,--Winkel
				FiX = 0,--Drehung
            },
            {
                --
            }
        ),
},
	
	Tasks = {
		aircraft_task(Transport),
		aircraft_task(Refueling),
    },	
	DefaultTask = aircraft_task(Refueling),

	SFM_Data = {     --kc-135
	aerodynamics = 
		{
			Cy0	=	0,
			Mzalfa	=	6.6,
			Mzalfadt	=	1,
			kjx	=	2.85,
			kjz	=	0.00125,
			Czbe	=	-0.012,
			cx_gear	=	0.015,
			cx_flap	=	0.08,
			cy_flap	=	1.6,
			cx_brk	=	0.06,
			table_data = 
			{
				[1] = 	{0,	0.023,	0.1,	0.064,	0,	0.5,	20,	1.4},
				[2] = 	{0.2,	0.023,	0.1,	0.064,	0,	1.5,	20,	1.4},
				[3] = 	{0.4,	0.023,	0.1,	0.064,	0,	2.5,	20,	1.4},
				[4] = 	{0.6,	0.025,	0.1,	0.064,	0.022,	3.5,	20,	1.4},
				[5] = 	{0.7,	0.03,	0.1,	0.083,	0.031,	3.5,	20,	1.2},
				[6] = 	{0.8,	0.032,	0.1,	0.107,	0.04,	3.5,	20,	1},
				[7] = 	{0.9,	0.045,	0.1,	0.148,	0.058,	3.5,	20,	0.8},
				[8] = 	{1,	0.054,	0.1,	0.199,	0.1,	3.5,	20,	0.7},
				[9] = 	{1.5,	0.054,	0.1,	0.199,	0.1,	3.5,	20,	0.2},
			}, -- end of table_data
		}, -- end of aerodynamics
		engine = 
		{
			Nmg	=	60.00001,--67.5,
			MinRUD	=	0,
			MaxRUD	=	1,
			MaksRUD	=	1,
			ForsRUD	=	1,
			type	=	"TurboJet",
			hMaxEng	=	19.5,
			dcx_eng	=	0.0085,
			cemax	=	0.37,
			cefor	=	0.37,
			dpdh_m	=	6200,
			dpdh_f	=	6200,
			table_data = 
			{
				[1] = 	{0,	480000,	480000},
				[2] = 	{0.2,	400000,	400000},
				[3] = 	{0.4,	343000,	343000},
				[4] = 	{0.6,	300000,	300000},
				[5] = 	{0.7,	285000,	285000},
				[6] = 	{0.8,	264000,	264000},
				[7] = 	{0.9,	254000,	254000},
				[8] = 	{1,	233000,	233000},
				[9] = 	{1.1,	217000,	217000},
			}, -- end of table_data
		}, -- end of engine
	},


	--damage , index meaning see in  Scripts\Aircrafts\_Common\Damage.lua
	Damage = {
	[0]  = {critical_damage = 5,  args = {146}},--NOSE_CENTER
	[1]  = {critical_damage = 3,  args = {296}},--NOSE_LEFT_SIDE
	[2]  = {critical_damage = 3,  args = {297}},--NOSE_RIGHT_SIDE
	[3]  = {critical_damage = 8, args = {65}},--CABINA / COCKPIT
	[4]  = {critical_damage = 2,  args = {298}},--CABIN_LEFT_SIDE
	[5]  = {critical_damage = 2,  args = {301}},--CABIN_RIGHT_SIDE
	[7]  = {critical_damage = 2,  args = {249}},--GUN
	[8]  = {critical_damage = 3,  args = {265}},--FRONT_GEAR_BOX
	[9]  = {critical_damage = 3,  args = {154}},--FUSELAGE_LEFT_SIDE
	[10] = {critical_damage = 3,  args = {153}},--MAIN / FUSELAGE_RIGHT_SIDE
	[11] = {critical_damage = 1,  args = {167}},--ENGINE_L
	[12] = {critical_damage = 1,  args = {161}},--ENGINE_R
	[13] = {critical_damage = 2,  args = {169}},--MTG_L_BOTTOM
	[14] = {critical_damage = 2,  args = {163}},--MTG_R_BOTTOM
	[15] = {critical_damage = 2,  args = {267}},--LEFT_GEAR_BOX
	[16] = {critical_damage = 2,  args = {266}},--RIGHT_GEAR_BOX
	[17] = {critical_damage = 2,  args = {168}},--ENGINE_L_OUT
	[18] = {critical_damage = 2,  args = {162}},--ENGINE_R_OUT
	[20] = {critical_damage = 2,  args = {183}},--AIR_BRAKE_R
	[23] = {critical_damage = 5, args = {223}},--WING_L_OUT
	[24] = {critical_damage = 5, args = {213}},--WING_R_OUT
	[25] = {critical_damage = 2,  args = {226}},--ELERON_L
	[26] = {critical_damage = 2,  args = {216}},--ELERON_R
	[29] = {critical_damage = 5, args = {224}, deps_cells = {23, 25}},--WING_L_CENTER
	[30] = {critical_damage = 5, args = {214}, deps_cells = {24, 26}},--WING_R_CENTER
	[35] = {critical_damage = 6, args = {225}, deps_cells = {23, 29, 25, 37}},--WING_L_IN
	[36] = {critical_damage = 6, args = {215}, deps_cells = {24, 30, 26, 38}},--WING_R_IN
	[37] = {critical_damage = 2,  args = {228}},--FLAP_L_IN
	[38] = {critical_damage = 2,  args = {218}},--FLAP_R_IN
	[39] = {critical_damage = 2,  args = {244}, deps_cells = {53}},--FIN_L_TOP
	[40] = {critical_damage = 2,  args = {241}, deps_cells = {54}},--FIN_R_TOP 
	[43] = {critical_damage = 2,  args = {243}, deps_cells = {39, 53}},--FIN_L_BOTTOM
	[44] = {critical_damage = 2,  args = {242}, deps_cells = {40, 54}},--FIN_R_BOTTOM 
	[51] = {critical_damage = 2,  args = {240}},--ELEVATOR_L_IN
	[52] = {critical_damage = 2,  args = {238}},--ELEVATOR_R_IN
	[53] = {critical_damage = 2,  args = {248}},--RUDDER_L
	[54] = {critical_damage = 2,  args = {247}},--RUDDER_R
	[56] = {critical_damage = 2,  args = {158}},--TAIL_LEFT_SIDE
	[57] = {critical_damage = 2,  args = {157}},--TAIL_RIGHT_SIDE
	[59] = {critical_damage = 3,  args = {148}},--NOSE_BOTTOM
	[61] = {critical_damage = 2,  args = {147}},--FUEL_TANK_F
	[82] = {critical_damage = 2,  args = {152}},--FUSELAGE_BOTTOM
	[105] = {critical_damage = 2,  args = {603}},--ENGINE_3
	[106] = {critical_damage = 2,  args = {604}},--ENGINE_4
	},
	
	DamageParts = 
	{  
		[1] = "kc_10_extender-oblomok-wing-r", -- wing R
		[2] = "kc_10_extender-oblomok-wing-l", -- wing L
--		[3] = "kc-135-oblomok-nose", -- nose
--		[4] = "kc-135-oblomok-tail", -- tail
--		[5] = "kc-135-oblomok-tail-l", -- tail
	},
	
	lights_data = { typename = "collection", lights = {
	
    [1] = { typename = "collection", -- WOLALIGHT_STROBES
					lights = {	
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV_BANO_1"},--R
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV1_BANO_1"},--L
						--{typename  = "natostrobelight",	argument_1  = 199, period = 1.2, color = {0.8,0,0}, connector = "RESERV2_BANO_1"},--H
						{typename  = "natostrobelight",	argument_1  = 196, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON R"},--195
						{typename  = "natostrobelight",	argument_1  = 195, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON L"},--196
						--{typename  = "natostrobelight",	argument_1  = 192, period = 1.2, color = {0.8,0,0}, connector = "BANO_0_BACK"},
						{typename  = "natostrobelight",	argument_1  = 204, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON R"},
						{typename  = "natostrobelight",	argument_1  = 204, period = 1.2, color = {0.8,0,0}, connector = "WHITE_BEACON L"},
							}
			},
	[2] = { typename = "collection",
					lights = {-- Landing light
										{typename = "spotlight",	connector = "MAIN_SPOT_PTM",argument = 208},
										{typename = "spotlight",	connector = "MAIN_SPOT_PTL",argument = 208},
										{typename = "spotlight",	connector = "MAIN_SPOT_PTR",argument = 208},
										{typename = "spotlight",	connector = "RESERV_SPOT_PTL",	argument = 209},
										{typename = "spotlight",	connector = "RESERV_SPOT_PTR",	argument = 209}
									 }
						},
    [3]	= {	typename = "collection", -- nav_lights_default
					lights = {
						{typename  = "omnilight",connector =  "BANO_1"  ,argument  =  190,color = {0.99, 0.11, 0.3}},-- Left Position(red)
						{typename  = "omnilight",connector =  "BANO_2"  ,argument  =  191,color = {0, 0.894, 0.6}},-- Right Position(green)
						{typename  = "omnilight",connector =  "BANO_0"  ,argument  =  192,color = {1, 1, 1}},-- Tail Position white)
							}
			},
	[4] = { typename = "collection", -- formation_lights_default
					lights = {
						--{typename  = "argumentlight" ,argument  = 200,},--formation_lights_tail_1 = 200;
						--{typename  = "argumentlight" ,argument  = 201,},--formation_lights_tail_2 = 201;
						--{typename  = "argumentlight" ,argument  = 202,},--formation_lights_left   = 202;
						--{typename  = "argumentlight" ,argument  = 203,},--formation_lights_right  = 203;
						{typename  = "argumentlight" ,argument  =  88,},--old aircraft arg
							}
			},
--[[			
	[5] = { typename = "collection", -- strobe_lights_default
					lights = {
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument = 193, color = {0.8,0,0}},-- Arg 193, 83,
						{typename  = "strobelight",connector =  "RED_BEACON_2",argument = 194, color = {0.8,0,0}},-- (-1"RESERV_RED_BEACON")
						{typename  = "strobelight",connector =  "RED_BEACON"  ,argument =  83, color = {0.8,0,0}},-- Arg 193, 83,
							}
			},
--]]			
	}},
}

add_aircraft(KC_10_Extender)
