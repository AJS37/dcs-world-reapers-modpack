01.11.2019


DCS 2.5.5 FREEWARE RELEASE


Installation Anleitung
**********************
https://lockonforum.de/community/thread/7521-mods-richtig-installieren/
https://lockonforum.de/community/thread/6667-flyable-fc3-flieger-mit-3d-f-15c-cockpit/

===========================================================================================
RECHTLICHER HINWEIS P3C_Orion

Diese P3C_Orion ist FREEWARE.
Das gesamte Originalmaterial ist urheberrechtlich geschützt und darf nicht ohne Genehmigung verwendet werden.
Das Umpacken oder Modifizieren dieses Pakets und seines Inhalts ist ohne Genehmigung nicht gestattet.
Die Verbreitung dieser Datei im Internet oder auf anderen Medien ist erlaubt, sofern sie kostenlos und ohne Veränderung der Originaldatei erfolgt.
Jede Art von Verteilung, die den Tausch von Geld beinhaltet, ist ohne Genehmigung nicht erlaubt.

Diese Software wird ohne jegliche ausdrückliche oder stillschweigende Garantie vertrieben. 
Diese Software sollte und wird keine Schäden an Ihrem System verursachen, dennoch ist der Autor nicht verantwortlich, für alle Schäden, die durch diese Software verursacht wird.

Das in diesem Modell dargestellte geistige Eigentum, einschliesslich der Marke "Lockheed", ist nicht mit den ursprünglichen Rechteinhabern verbunden oder wird von diesen unterstützt.

Dieser Mod enthält lizenzfreie Texte aus Wikipedia.

CREDITS
*******
Modellierung, Animationen 		- cdpkobra
RoughMet Dateien 				- BlackLibrary
Icons 							- BlackLibrary
Liveries							- crazyeddie

Das 3D-Modell des Lockheed P-3 Orion basiert auf 3D-Daten von EGPJET3D, die über Turbosquid erworben und als Referenz in der Modellierung verwendet wurde.
===========================================================================================
RECHTLICHER HINWEIS  C5_Galaxy

Diese C5_Galaxy ist FREEWARE.
Das gesamte Originalmaterial ist urheberrechtlich geschützt und darf nicht ohne Genehmigung verwendet werden.
Das Umpacken oder Modifizieren dieses Pakets und seines Inhalts ist ohne Genehmigung nicht gestattet.
Die Verbreitung dieser Datei im Internet oder auf anderen Medien ist erlaubt, sofern sie kostenlos und ohne Veränderung der Originaldatei erfolgt.
Jede Art von Verteilung, die den Tausch von Geld beinhaltet, ist ohne Genehmigung nicht erlaubt.

Diese Software wird ohne jegliche ausdrückliche oder stillschweigende Garantie vertrieben. 
Diese Software sollte und wird keine Schäden an Ihrem System verursachen, dennoch ist der Autor nicht verantwortlich, für alle Schäden, die durch diese Software verursacht wird.

Das in diesem Modell dargestellte geistige Eigentum, einschliesslich der Marke "Lockheed", ist nicht mit den ursprünglichen Rechteinhabern verbunden oder wird von diesen unterstützt.

Dieser Mod enthält lizenzfreie Texte aus Wikipedia.

CREDITS
*******
Modellierung, Animationen 		- cdpkobra
RoughMet Dateien 				- BlackLibrary, Urbi
Icons 							- BlackLibrary
Liveries							- crazyeddie

Das 3D-Modell des Lockheed C-5 Galaxy basiert auf 3D-Daten von EGPJET3D, die über Turbosquid erworben und als Referenz in der Modellierung verwendet wurde.
===========================================================================================
RECHTLICHER HINWEIS  C-2A_Greyhound

Diese C-2A_Greyhound ist FREEWARE.
Das gesamte Originalmaterial ist urheberrechtlich geschützt und darf nicht ohne Genehmigung verwendet werden.
Das Umpacken oder Modifizieren dieses Pakets und seines Inhalts ist ohne Genehmigung nicht gestattet.
Die Verbreitung dieser Datei im Internet oder auf anderen Medien ist erlaubt, sofern sie kostenlos und ohne Veränderung der Originaldatei erfolgt.
Jede Art von Verteilung, die den Tausch von Geld beinhaltet, ist ohne Genehmigung nicht erlaubt.

Diese Software wird ohne jegliche ausdrückliche oder stillschweigende Garantie vertrieben. 
Diese Software sollte und wird keine Schäden an Ihrem System verursachen, dennoch ist der Autor nicht verantwortlich, für alle Schäden, die durch diese Software verursacht wird.

Das in diesem Modell dargestellte geistige Eigentum, einschliesslich der Marke "Grumman", ist nicht mit den ursprünglichen Rechteinhabern verbunden oder wird von diesen unterstützt.

Dieser Mod enthält lizenzfreie Texte aus Wikipedia.

CREDITS
*******
Modellierung, Animationen 		- cdpkobra
RoughMet Dateien 				- BlackLibrary
Icons 							- BlackLibrary
Liveries							- crazyeddie

Das 3D-Modell des Lockheed C-2A Greyhound basiert auf 3D-Daten von damaggio, die über Turbosquid erworben und als Referenz in der Modellierung verwendet wurde.
===========================================================================================
RECHTLICHER HINWEIS  KC-10 Extender

Diese KC-10 Extender ist FREEWARE.
Das gesamte Originalmaterial ist urheberrechtlich geschützt und darf nicht ohne Genehmigung verwendet werden.
Das Umpacken oder Modifizieren dieses Pakets und seines Inhalts ist ohne Genehmigung nicht gestattet.
Die Verbreitung dieser Datei im Internet oder auf anderen Medien ist erlaubt, sofern sie kostenlos und ohne Veränderung der Originaldatei erfolgt.
Jede Art von Verteilung, die den Tausch von Geld beinhaltet, ist ohne Genehmigung nicht erlaubt.

Diese Software wird ohne jegliche ausdrückliche oder stillschweigende Garantie vertrieben. 
Diese Software sollte und wird keine Schäden an Ihrem System verursachen, dennoch ist der Autor nicht verantwortlich, für alle Schäden, die durch diese Software verursacht wird.

Das in diesem Modell dargestellte geistige Eigentum, einschliesslich der Marke "McDonnell Douglas", ist nicht mit den ursprünglichen Rechteinhabern verbunden oder wird von diesen unterstützt.

Dieser Mod enthält lizenzfreie Texte aus Wikipedia.

CREDITS
*******
Modellierung, Animationen 		- cdpkobra
RoughMet Dateien 				- Urbi
Icons 							- BlackLibrary
Liveries							- crazyeddie

Das Modell wurde von Sierra99 gesponsert 

Das 3D-Modell des KC-10 Extender basiert auf 3D-Daten von EGPJET3D, die über Turbosquid erworben und als Referenz in der Modellierung verwendet wurde.
===========================================================================================
RECHTLICHER HINWEIS  USAF Pilot

CREDITS
*******
Modellierung, Animationen 		- cdpkobra
Texturen					- BlackLibrary
RoughMet Dateien 					- BlackLibrary
Lua basiertend auf dem Pilot Mod by SUNTSAG 


Das 3D-Modell des USAF Piloten basiert auf 3D-Daten von AviaKinetic, die über Turbosquid erworben und als Referenz in der Modellierung verwendet wurde.
===========================================================================================


KONTAKTDATEN
************

cdpkobra
https://lockonforum.de/user/711-cdpkobra/#sitemap_cms

cdpkobra
https://forums.eagle.ru/member.php?u=88910

BlackLibrary
https://forums.eagle.ru/member.php?u=127667

