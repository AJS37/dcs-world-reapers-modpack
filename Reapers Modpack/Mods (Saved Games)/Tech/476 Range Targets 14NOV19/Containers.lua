GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x1_Blu"
GT.visual.shape_dstr = "Container_40_1x1_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x1_Blu"
GT.DisplayName = _("*SeaLand 1x1 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x1_Bro"
GT.visual.shape_dstr = "Container_40_1x1_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x1_Bro"
GT.DisplayName = _("*SeaLand 1x1 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x1_Grn"
GT.visual.shape_dstr = "Container_40_1x1_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x1_Grn"
GT.DisplayName = _("*SeaLand 1x1 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x1_Tan"
GT.visual.shape_dstr = "Container_40_1x1_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x1_Tan"
GT.DisplayName = _("*SeaLand 1x1 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x1_Wht"
GT.visual.shape_dstr = "Container_40_1x1_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x1_Wht"
GT.DisplayName = _("*SeaLand 1x1 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x2_Blu"
GT.visual.shape_dstr = "Container_40_1x2_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x2_Blu"
GT.DisplayName = _("*SeaLand 1x2 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x2_Bro"
GT.visual.shape_dstr = "Container_40_1x2_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x2_Bro"
GT.DisplayName = _("*SeaLand 1x2 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x2_Grn"
GT.visual.shape_dstr = "Container_40_1x2_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x2_Grn"
GT.DisplayName = _("*SeaLand 1x2 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x2_Tan"
GT.visual.shape_dstr = "Container_40_1x2_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x2_Tan"
GT.DisplayName = _("*SeaLand 1x2 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x2_Wht"
GT.visual.shape_dstr = "Container_40_1x2_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x2_Wht"
GT.DisplayName = _("*SeaLand 1x2 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x3_Blu"
GT.visual.shape_dstr = "Container_40_1x3_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x3_Blu"
GT.DisplayName = _("*SeaLand 1x3 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x3_Bro"
GT.visual.shape_dstr = "Container_40_1x3_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x3_Bro"
GT.DisplayName = _("*SeaLand 1x3 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x3_Grn"
GT.visual.shape_dstr = "Container_40_1x3_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x3_Grn"
GT.DisplayName = _("*SeaLand 1x3 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x3_Tan"
GT.visual.shape_dstr = "Container_40_1x3_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x3_Tan"
GT.DisplayName = _("*SeaLand 1x3 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_1x3_Wht"
GT.visual.shape_dstr = "Container_40_1x3_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_1x3_Wht"
GT.DisplayName = _("*SeaLand 1x3 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x1_Blu"
GT.visual.shape_dstr = "Container_40_2x1_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x1_Blu"
GT.DisplayName = _("*SeaLand 2x1 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x1_Bro"
GT.visual.shape_dstr = "Container_40_2x1_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x1_Bro"
GT.DisplayName = _("*SeaLand 2x1 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x1_Grn"
GT.visual.shape_dstr = "Container_40_2x1_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x1_Grn"
GT.DisplayName = _("*SeaLand 2x1 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x1_Tan"
GT.visual.shape_dstr = "Container_40_2x1_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x1_Tan"
GT.DisplayName = _("*SeaLand 2x1 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x1_Wht"
GT.visual.shape_dstr = "Container_40_2x1_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x1_Wht"
GT.DisplayName = _("*SeaLand 2x1 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x2_Blu"
GT.visual.shape_dstr = "Container_40_2x2_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x2_Blu"
GT.DisplayName = _("*SeaLand 2x2 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x2_Bro"
GT.visual.shape_dstr = "Container_40_2x2_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x2_Bro"
GT.DisplayName = _("*SeaLand 2x2 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x2_Grn"
GT.visual.shape_dstr = "Container_40_2x2_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x2_Grn"
GT.DisplayName = _("*SeaLand 2x2 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x2_Tan"
GT.visual.shape_dstr = "Container_40_2x2_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x2_Tan"
GT.DisplayName = _("*SeaLand 2x2 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x2_Wht"
GT.visual.shape_dstr = "Container_40_2x2_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x2_Wht"
GT.DisplayName = _("*SeaLand 2x2 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x3_Blu"
GT.visual.shape_dstr = "Container_40_2x3_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x3_Blu"
GT.DisplayName = _("*SeaLand 2x3 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x3_Bro"
GT.visual.shape_dstr = "Container_40_2x3_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x3_Bro"
GT.DisplayName = _("*SeaLand 2x3 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x3_Grn"
GT.visual.shape_dstr = "Container_40_2x3_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x3_Grn"
GT.DisplayName = _("*SeaLand 2x3 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x3_Tan"
GT.visual.shape_dstr = "Container_40_2x3_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x3_Tan"
GT.DisplayName = _("*SeaLand 2x3 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_2x3_Wht"
GT.visual.shape_dstr = "Container_40_2x3_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_2x3_Wht"
GT.DisplayName = _("*SeaLand 2x3 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x1_Blu"
GT.visual.shape_dstr = "Container_40_3x1_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x1_Blu"
GT.DisplayName = _("*SeaLand 3x1 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x1_Bro"
GT.visual.shape_dstr = "Container_40_3x1_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x1_Bro"
GT.DisplayName = _("*SeaLand 3x1 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x1_Grn"
GT.visual.shape_dstr = "Container_40_3x1_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x1_Grn"
GT.DisplayName = _("*SeaLand 3x1 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x1_Tan"
GT.visual.shape_dstr = "Container_40_3x1_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x1_Tan"
GT.DisplayName = _("*SeaLand 3x1 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x1_Wht"
GT.visual.shape_dstr = "Container_40_3x1_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x1_Wht"
GT.DisplayName = _("*SeaLand 3x1 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x2_Blu"
GT.visual.shape_dstr = "Container_40_3x2_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x2_Blu"
GT.DisplayName = _("*SeaLand 3x2 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x2_Bro"
GT.visual.shape_dstr = "Container_40_3x2_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x2_Bro"
GT.DisplayName = _("*SeaLand 3x2 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x2_Grn"
GT.visual.shape_dstr = "Container_40_3x2_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x2_Grn"
GT.DisplayName = _("*SeaLand 3x2 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x2_Tan"
GT.visual.shape_dstr = "Container_40_3x2_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x2_Tan"
GT.DisplayName = _("*SeaLand 3x2 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x2_Wht"
GT.visual.shape_dstr = "Container_40_3x2_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x2_Wht"
GT.DisplayName = _("*SeaLand 3x2 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x3_Blu"
GT.visual.shape_dstr = "Container_40_3x3_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x3_Blu"
GT.DisplayName = _("*SeaLand 3x3 (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x3_Bro"
GT.visual.shape_dstr = "Container_40_3x3_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x3_Bro"
GT.DisplayName = _("*SeaLand 3x3 (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x3_Grn"
GT.visual.shape_dstr = "Container_40_3x3_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x3_Grn"
GT.DisplayName = _("*SeaLand 3x3 (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x3_Tan"
GT.visual.shape_dstr = "Container_40_3x3_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x3_Tan"
GT.DisplayName = _("*SeaLand 3x3 (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_3x3_Wht"
GT.visual.shape_dstr = "Container_40_3x3_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_3x3_Wht"
GT.DisplayName = _("*SeaLand 3x3 (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_9x1_gray"
GT.visual.shape_dstr = "Container_40_9x1_gray"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_9x1_gray"
GT.DisplayName = _("*SeaLand 9x1 (Gray)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_Tower_Blu"
GT.visual.shape_dstr = "Container_40_Tower_Blu"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_Tower_Blu"
GT.DisplayName = _("*SeaLand Tower (Blue)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_Tower_Bro"
GT.visual.shape_dstr = "Container_40_Tower_Bro"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_Tower_Bro"
GT.DisplayName = _("*SeaLand Tower (Brown)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_Tower_Grn"
GT.visual.shape_dstr = "Container_40_Tower_Grn"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_Tower_Grn"
GT.DisplayName = _("*SeaLand Tower (Green)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_Tower_Tan"
GT.visual.shape_dstr = "Container_40_Tower_Tan"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_Tower_Tan"
GT.DisplayName = _("*SeaLand Tower (Tan)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "Container_40_Tower_Wht"
GT.visual.shape_dstr = "Container_40_Tower_Wht"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "Container_40_Tower_Wht"
GT.DisplayName = _("*SeaLand Tower (White)")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
