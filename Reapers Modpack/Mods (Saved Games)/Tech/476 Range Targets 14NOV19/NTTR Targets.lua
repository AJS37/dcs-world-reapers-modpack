
GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-11 DMPI 01"
GT.visual.shape_dstr = "NTTR Target 62-11 DMPI 01"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-11 DMPI 01"
GT.DisplayName = _("*NTTR Target 62-11 DMPI 01")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-12 DMPI 203"
GT.visual.shape_dstr = "NTTR Target 62-12 DMPI 203"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-12 DMPI 203"
GT.DisplayName = _("*NTTR Target 62-12 DMPI 203")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-13 DMPI 310"
GT.visual.shape_dstr = "NTTR Target 62-13 DMPI 310"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-13 DMPI 310"
GT.DisplayName = _("*NTTR Target 62-13 DMPI 310")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-13 DMPI 313"
GT.visual.shape_dstr = "NTTR Target 62-13 DMPI 313"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-13 DMPI 313"
GT.DisplayName = _("*NTTR Target 62-13 DMPI 313")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-13 DMPI 321"
GT.visual.shape_dstr = "NTTR Target 62-13 DMPI 321"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-13 DMPI 321"
GT.DisplayName = _("*NTTR Target 62-13 DMPI 321")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-13 DMPI 334"
GT.visual.shape_dstr = "NTTR Target 62-13 DMPI 334"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-13 DMPI 334"
GT.DisplayName = _("*NTTR Target 62-13 DMPI 334")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-13 DMPI 344"
GT.visual.shape_dstr = "NTTR Target 62-13 DMPI 344"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-13 DMPI 344"
GT.DisplayName = _("*NTTR Target 62-13 DMPI 344")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 102"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 102"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 102"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 102")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 107"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 107"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 107"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 107")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 109"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 109"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 109"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 109")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 111"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 111"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 111"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 111")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 113"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 113"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 113"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 113")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 116"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 116"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 116"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 116")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 119"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 119"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 119"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 119")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 124"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 124"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 124"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 124")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 126"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 126"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 126"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 126")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 128"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 128"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 128"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 128")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 130"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 130"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 130"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 130")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 131"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 131"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 131"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 131")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 134"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 134"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 134"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 134")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 135"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 135"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 135"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 135")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 140"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 140"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 140"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 140")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 142"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 142"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 142"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 142")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-21 DMPI 147"
GT.visual.shape_dstr = "NTTR Target 62-21 DMPI 147"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-21 DMPI 147"
GT.DisplayName = _("*NTTR Target 62-21 DMPI 147")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 205"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 205"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 205"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 205")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 206"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 206"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 206"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 206")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 208"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 208"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 208"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 208")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 210"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 210"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 210"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 210")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 214"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 214"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 214"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 214")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-22 DMPI 217"
GT.visual.shape_dstr = "NTTR Target 62-22 DMPI 217"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-22 DMPI 217"
GT.DisplayName = _("*NTTR Target 62-22 DMPI 217")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-32 DMPI 205"
GT.visual.shape_dstr = "NTTR Target 62-32 DMPI 205"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-32 DMPI 205"
GT.DisplayName = _("*NTTR Target 62-32 DMPI 205")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-32 DMPI 216"
GT.visual.shape_dstr = "NTTR Target 62-32 DMPI 216"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-32 DMPI 216"
GT.DisplayName = _("*NTTR Target 62-32 DMPI 216")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 103"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 103"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 103"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 103")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 110"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 110"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 110"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 110")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 121"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 121"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 121"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 121")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 122"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 122"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 122"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 122")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 123"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 123"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 123"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 123")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-41 DMPI 136"
GT.visual.shape_dstr = "NTTR Target 62-41 DMPI 136"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-41 DMPI 136"
GT.DisplayName = _("*NTTR Target 62-41 DMPI 136")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-51 DMPI 103"
GT.visual.shape_dstr = "NTTR Target 62-51 DMPI 103"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-51 DMPI 103"
GT.DisplayName = _("*NTTR Target 62-51 DMPI 103")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-52 DMPI 203"
GT.visual.shape_dstr = "NTTR Target 62-52 DMPI 203"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-52 DMPI 203"
GT.DisplayName = _("*NTTR Target 62-52 DMPI 203")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-53 DMPI 305"
GT.visual.shape_dstr = "NTTR Target 62-53 DMPI 305"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-53 DMPI 305"
GT.DisplayName = _("*NTTR Target 62-53 DMPI 305")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-53 DMPI 306"
GT.visual.shape_dstr = "NTTR Target 62-53 DMPI 306"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-53 DMPI 306"
GT.DisplayName = _("*NTTR Target 62-53 DMPI 306")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-53 DMPI 309"
GT.visual.shape_dstr = "NTTR Target 62-53 DMPI 309"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-53 DMPI 309"
GT.DisplayName = _("*NTTR Target 62-53 DMPI 309")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-53 DMPI 310"
GT.visual.shape_dstr = "NTTR Target 62-53 DMPI 310"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-53 DMPI 310"
GT.DisplayName = _("*NTTR Target 62-53 DMPI 310")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-54 DMPI 407"
GT.visual.shape_dstr = "NTTR Target 62-54 DMPI 407"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-54 DMPI 407"
GT.DisplayName = _("*NTTR Target 62-54 DMPI 407")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-54 DMPI 413"
GT.visual.shape_dstr = "NTTR Target 62-54 DMPI 413"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-54 DMPI 413"
GT.DisplayName = _("*NTTR Target 62-54 DMPI 413")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-54 DMPI 419"
GT.visual.shape_dstr = "NTTR Target 62-54 DMPI 419"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-54 DMPI 419"
GT.DisplayName = _("*NTTR Target 62-54 DMPI 419")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-54 DMPI 421"
GT.visual.shape_dstr = "NTTR Target 62-54 DMPI 421"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-54 DMPI 421"
GT.DisplayName = _("*NTTR Target 62-54 DMPI 421")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-55 DMPI 501"
GT.visual.shape_dstr = "NTTR Target 62-55 DMPI 501"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-55 DMPI 501"
GT.DisplayName = _("*NTTR Target 62-55 DMPI 501")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-55 DMPI 510"
GT.visual.shape_dstr = "NTTR Target 62-55 DMPI 510"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-55 DMPI 510"
GT.DisplayName = _("*NTTR Target 62-55 DMPI 510")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-61 DMPI 102"
GT.visual.shape_dstr = "NTTR Target 62-61 DMPI 102"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-61 DMPI 102"
GT.DisplayName = _("*NTTR Target 62-61 DMPI 102")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-62 DMPI 201"
GT.visual.shape_dstr = "NTTR Target 62-62 DMPI 201"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-62 DMPI 201"
GT.DisplayName = _("*NTTR Target 62-62 DMPI 201")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-62 DMPI 207"
GT.visual.shape_dstr = "NTTR Target 62-62 DMPI 207"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-62 DMPI 207"
GT.DisplayName = _("*NTTR Target 62-62 DMPI 207")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-62 DMPI 209"
GT.visual.shape_dstr = "NTTR Target 62-62 DMPI 209"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-62 DMPI 209"
GT.DisplayName = _("*NTTR Target 62-62 DMPI 209")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-62 DMPI 213"
GT.visual.shape_dstr = "NTTR Target 62-62 DMPI 213"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-62 DMPI 213"
GT.DisplayName = _("*NTTR Target 62-62 DMPI 213")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-63 DMPI 301"
GT.visual.shape_dstr = "NTTR Target 62-63 DMPI 301"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-63 DMPI 301"
GT.DisplayName = _("*NTTR Target 62-63 DMPI 301")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-63 DMPI 303"
GT.visual.shape_dstr = "NTTR Target 62-63 DMPI 303"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-63 DMPI 303"
GT.DisplayName = _("*NTTR Target 62-63 DMPI 303")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-71 DMPI 101"
GT.visual.shape_dstr = "NTTR Target 62-71 DMPI 101"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-71 DMPI 101"
GT.DisplayName = _("*NTTR Target 62-71 DMPI 101")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-71 DMPI 113"
GT.visual.shape_dstr = "NTTR Target 62-71 DMPI 113"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-71 DMPI 113"
GT.DisplayName = _("*NTTR Target 62-71 DMPI 113")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-71 DMPI 119"
GT.visual.shape_dstr = "NTTR Target 62-71 DMPI 119"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-71 DMPI 119"
GT.DisplayName = _("*NTTR Target 62-71 DMPI 119")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-72 DMPI 201"
GT.visual.shape_dstr = "NTTR Target 62-72 DMPI 201"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-72 DMPI 201"
GT.DisplayName = _("*NTTR Target 62-72 DMPI 201")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-72 DMPI 204"
GT.visual.shape_dstr = "NTTR Target 62-72 DMPI 204"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-72 DMPI 204"
GT.DisplayName = _("*NTTR Target 62-72 DMPI 204")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-73 DMPI 301"
GT.visual.shape_dstr = "NTTR Target 62-73 DMPI 301"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-73 DMPI 301"
GT.DisplayName = _("*NTTR Target 62-73 DMPI 301")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-74 DMPI 401"
GT.visual.shape_dstr = "NTTR Target 62-74 DMPI 401"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-74 DMPI 401"
GT.DisplayName = _("*NTTR Target 62-74 DMPI 401")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-74 DMPI 411"
GT.visual.shape_dstr = "NTTR Target 62-74 DMPI 411"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-74 DMPI 411"
GT.DisplayName = _("*NTTR Target 62-74 DMPI 411")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-75 DMPI 501"
GT.visual.shape_dstr = "NTTR Target 62-75 DMPI 501"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-75 DMPI 501"
GT.DisplayName = _("*NTTR Target 62-75 DMPI 501")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-75 DMPI 514"
GT.visual.shape_dstr = "NTTR Target 62-75 DMPI 514"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-75 DMPI 514"
GT.DisplayName = _("*NTTR Target 62-75 DMPI 514")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-75 DMPI 524"
GT.visual.shape_dstr = "NTTR Target 62-75 DMPI 524"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-75 DMPI 524"
GT.DisplayName = _("*NTTR Target 62-75 DMPI 524")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-76 DMPI 601"
GT.visual.shape_dstr = "NTTR Target 62-76 DMPI 601"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-76 DMPI 601"
GT.DisplayName = _("*NTTR Target 62-76 DMPI 601")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-77 DMPI 701"
GT.visual.shape_dstr = "NTTR Target 62-77 DMPI 701"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-77 DMPI 701"
GT.DisplayName = _("*NTTR Target 62-77 DMPI 701")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-77 DMPI 712"
GT.visual.shape_dstr = "NTTR Target 62-77 DMPI 712"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-77 DMPI 712"
GT.DisplayName = _("*NTTR Target 62-77 DMPI 712")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-78 DMPI 802"
GT.visual.shape_dstr = "NTTR Target 62-78 DMPI 802"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-78 DMPI 802"
GT.DisplayName = _("*NTTR Target 62-78 DMPI 802")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-92 DMPI 201"
GT.visual.shape_dstr = "NTTR Target 62-92 DMPI 201"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-92 DMPI 201"
GT.DisplayName = _("*NTTR Target 62-92 DMPI 201")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 62-93 DMPI 301"
GT.visual.shape_dstr = "NTTR Target 62-93 DMPI 301"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 62-93 DMPI 301"
GT.DisplayName = _("*NTTR Target 62-93 DMPI 301")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-02 DMPI 01"
GT.visual.shape_dstr = "NTTR Target 66-02 DMPI 01"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-02 DMPI 01"
GT.DisplayName = _("*NTTR Target 66-02 DMPI 01")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-02 DMPI 02"
GT.visual.shape_dstr = "NTTR Target 66-02 DMPI 02"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-02 DMPI 02"
GT.DisplayName = _("*NTTR Target 66-02 DMPI 02")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-02 DMPI 40"
GT.visual.shape_dstr = "NTTR Target 66-02 DMPI 40"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-02 DMPI 40"
GT.DisplayName = _("*NTTR Target 66-02 DMPI 40")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 02"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 02"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 02"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 02")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 03"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 03"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 03"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 03")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 04"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 04"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 04"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 04")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 05"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 05"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 05"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 05")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 06"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 06"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 06"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 06")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 07"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 07"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 07"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 07")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 09"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 09"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 09"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 09")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 13"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 13"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 13"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 13")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 18"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 18"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 18"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 18")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 66-07 DMPI 31"
GT.visual.shape_dstr = "NTTR Target 66-07 DMPI 31"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 66-07 DMPI 31"
GT.DisplayName = _("*NTTR Target 66-07 DMPI 31")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 71-12 DMPI 01"
GT.visual.shape_dstr = "NTTR Target 71-12 DMPI 01"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 71-12 DMPI 01"
GT.DisplayName = _("*NTTR Target 71-12 DMPI 01")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-01 DMPI 01"
GT.visual.shape_dstr = "NTTR Target 76-01 DMPI 01"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-01 DMPI 01"
GT.DisplayName = _("*NTTR Target 76-01 DMPI 01")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-01 DMPI 13"
GT.visual.shape_dstr = "NTTR Target 76-01 DMPI 13"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-01 DMPI 13"
GT.DisplayName = _("*NTTR Target 76-01 DMPI 13")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-01 DMPI 14"
GT.visual.shape_dstr = "NTTR Target 76-01 DMPI 14"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-01 DMPI 14"
GT.DisplayName = _("*NTTR Target 76-01 DMPI 14")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-01 DMPI 306"
GT.visual.shape_dstr = "NTTR Target 76-01 DMPI 306"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-01 DMPI 306"
GT.DisplayName = _("*NTTR Target 76-01 DMPI 306")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 02"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 02"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 02"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 02")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 04"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 04"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 04"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 04")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 06"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 06"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 06"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 06")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 210"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 210"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 210"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 210")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 23"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 23"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 23"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 23")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 24"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 24"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 24"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 24")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 27"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 27"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 27"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 27")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-11 DMPI 33"
GT.visual.shape_dstr = "NTTR Target 76-11 DMPI 33"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-11 DMPI 33"
GT.DisplayName = _("*NTTR Target 76-11 DMPI 33")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-20 DMPI 01"
GT.visual.shape_dstr = "NTTR Target 76-20 DMPI 01"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-20 DMPI 01"
GT.DisplayName = _("*NTTR Target 76-20 DMPI 01")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)

GT = {};
set_recursive_metatable(GT, GT_t.generic_stationary)
set_recursive_metatable(GT.chassis, GT_t.CH_t.STATIC);
GT.chassis.life = 1000

GT.visual.shape = "NTTR Target 76-30 DMPI 14"
GT.visual.shape_dstr = "NTTR Target 76-30 DMPI 14"
GT.CustomAimPoint = {1,1,0}
GT.visual.fire_size = 0
GT.visual.fire_pos = {-2,0,0};
GT.visual.fire_time = 0
GT.time_agony = 180;

GT.Name = "NTTR Target 76-30 DMPI 14"
GT.DisplayName = _("*NTTR Target 76-30 DMPI 14")
GT.Rate = 1

GT.DetectionRange  = 0;
GT.ThreatRange = 0;
GT.mapclasskey = "P0091000076";
GT.attribute = {wsType_Ground,wsType_Tank,wsType_NoWeapon,wsType_GenericFort,
                "Fortifications",
                };
GT.category = "Fortification";
                
add_surface_unit(GT)
